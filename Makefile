# Unit tests that we run without the `testing` feature, to test certain
# functionality in production.
run-standard = \
	test_build_critical_smoke \
	test_build_var_multiple \
	test_build_var_smoke \
	test_build_var_lazy_smoke \
	should_encode_block_headers \
	should_encode_block \
	block_size_should_not_change \
	test_total_headers_size

# Amount of time to run each fuzz target.
fuzz-time = 60

all: build

audit:
	# Ignore warning for unmaintained dependency of criterion.
	cargo audit -D warnings --ignore RUSTSEC-2021-0127

dependencies:
	curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
	rustup component add rustfmt
	rustup component add clippy
	rustup target add wasm32-unknown-unknown
	cargo install cargo-audit

check:
	cargo check
	cargo check --target wasm32-unknown-unknown
	cargo check                                 --features dev
	cargo check --target wasm32-unknown-unknown --features dev
	cargo check                                                --features testing
	cargo check --target wasm32-unknown-unknown                --features testing
	cargo check                                 --features dev --features testing
	cargo check --target wasm32-unknown-unknown --features dev --features testing

fmt:
	cargo fmt

lint: fmt
	cargo clippy --all-targets --all-features -- -D warnings
	cargo fmt --all -- --check

build:
	wasm-pack -v build

# Run all tests.
test-all: test-unit test-browser

# Run browser tests for all browsers.
test-browser: test-browser-chrome test-browser-firefox test-browser-safari

# NOTE: Drivers should be installed automatically by wasm-pack.
test-browser-chrome:
	wasm-pack -v test --chrome --headless -- --features testing

# NOTE: Drivers should be installed automatically by wasm-pack.
test-browser-firefox:
	wasm-pack -v test --firefox --headless -- --features testing

# You must enable the 'Allow Remote Automation' option in Safari's Develop menu
# to control Safari via WebDriver.
#
# NOTE: Drivers are NOT installed automatically by wasm-pack.
test-browser-safari:
	wasm-pack -v test --safari --headless -- --features testing

# Run unit tests.
test-unit:
	cargo test --verbose --features testing -- --nocapture $(run)

# Run certain unit tests without the `testing` feature.
# Run in dev mode so we still get a backtrace.
test-unit-standard:
	cargo test --verbose --features dev -- --nocapture $(run-standard)

# Coverage command for GitLab CI. Linux-only.
coverage-ci:
	cargo tarpaulin --ignore-tests --out Xml --features testing

# Command for checking coverage locally.
#
# See https://github.com/xd009642/tarpaulin#docker.
coverage-docker:
	docker run --security-opt seccomp=unconfined -v "${PWD}:/volume" xd009642/tarpaulin sh -c "cargo tarpaulin --features testing"

# Run the fuzzer on all fuzz targets.
fuzzer:
	# -len_control=20 will quickly ramp up the input length. Otherwise we don't
	# get enough for a full block header. Default is 100.
	cargo fuzz run decode_encode_block_header -- -max_total_time=$(fuzz-time) -len_control=20
	cargo fuzz run decode_solve_block_header -- -max_total_time=$(fuzz-time) -len_control=20
	cargo fuzz run verify_difficulty_state -- -max_total_time=$(fuzz-time) -len_control=20

bench:
	cargo bench --features testing
