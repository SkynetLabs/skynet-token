/// Macro that prints a stacktrace but doesn't panic in production. Requires at
/// least a format string argument.
#[macro_export]
macro_rules! build_critical {
    ($($arg:tt)*) => ({
        let s = format!($($arg)*);

        // Print backtrace in production and dev modes.
        #[cfg(any(not(feature = "testing"), feature = "dev"))]
        {
            let bt = backtrace::Backtrace::new();

            // Use console.error in browsers.
            #[cfg(target = "wasm32-unknown-unknown")]
            {
                $crate::utils::error(&format!("{:?}", bt));
                $crate::utils::error(&s);
            }
            // Use stderr in cli.
            #[cfg(not(target = "wasm32-unknown-unknown"))]
            {
                eprintln!("{:?}", bt);
                eprintln!("{}", s);
            }
        }

        // Panic if not in production.
        #[cfg(any(feature = "testing", feature = "dev"))]
        panic!("{}", s);
    })
}

/// Macro to declare consts and statics which have different values depending on
/// whether they are declared within a test or production build.
#[macro_export]
macro_rules! build_var {
    ($(#[$attr:meta])* $v:vis $prefix:ident $name:ident: $type:ty = {
        standard = $standard_value:expr,
        testing = $testing_value:expr,
    }; $($t:tt)*) => {
        #[cfg(not(feature = "testing"))]
        $(#[$attr])* $v $prefix $name: $type = $standard_value;

        #[cfg(feature = "testing")]
        $(#[$attr])* $v $prefix $name: $type = $testing_value;

        // Invoke recursively with the tail (remaining tokens).
        build_var!($($t)*);
    };
    // Allow invoking this macro recursively with the tail, which may be empty.
    () => ();
}

/// Macro to declare consts and statics which have different values depending on
/// whether they are declared within a test or production build.
///
/// This is the `lazy_static!` version of `build_var!`. Use this for variables
/// that can't be defined in a const context.
#[macro_export]
macro_rules! build_var_lazy {
    ($(#[$attr:meta])* pub static ref $name:ident: $type:ty = {
        standard = $standard_value:expr,
        testing = $testing_value:expr,
    }; $($t:tt)*) => {
        #[cfg(not(feature = "testing"))]
        lazy_static::lazy_static! {
            $(#[$attr])* pub static ref $name: $type = $standard_value;
        }
        #[cfg(feature = "testing")]
        lazy_static::lazy_static! {
            $(#[$attr])* pub static ref $name: $type = $testing_value;
        }

        // Invoke recursively with the tail (remaining tokens).
        build_var_lazy!($($t)*);
    };
    // Allow invoking this macro recursively with the tail, which may be empty.
    () => ();
}

#[cfg(test)]
mod tests {
    #[test]
    #[should_panic(expected = "panicking with value 123")]
    fn test_build_critical_smoke() {
        build_critical!("panicking with value {}", 123);
    }

    #[test]
    /// Simple testing for the build_var macro.
    fn test_build_var_smoke() {
        // Start by creating a simple build var with numbers.
        build_var!(
            // NUMBER is ...
            const NUMBER: u64 = {
                standard = 1337,
                testing = 42,
            };
        );
        #[cfg(not(feature = "testing"))]
        assert_eq!(NUMBER, 1337);
        #[cfg(feature = "testing")]
        assert_eq!(NUMBER, 42);

        // Next wrap the numbers in a struct and add the pub keyword.
        #[derive(Debug, PartialEq, Eq)]
        pub struct Foo(u64);

        build_var!(
            pub const STRUCT: Foo = {
                standard = Foo(1337),
                testing = Foo(42),
            };
        );
        #[cfg(not(feature = "testing"))]
        assert_eq!(STRUCT, Foo(1337));
        #[cfg(feature = "testing")]
        assert_eq!(STRUCT, Foo(42));
    }

    #[test]
    /// Simple testing for the build_var_lazy macro.
    fn test_build_var_lazy_smoke() {
        use num_bigint::BigInt;

        build_var_lazy!(
            // BIG is ...
            pub static ref BIG: BigInt = {
                standard = 1337.into(),
                testing = 42.into(),
            };
        );
        #[cfg(not(feature = "testing"))]
        assert_eq!(*BIG, 1337.into());
        #[cfg(feature = "testing")]
        assert_eq!(*BIG, 42.into());
    }

    #[test]
    fn test_build_var_multiple() {
        build_var!(
            /// NUMBER_A is ...
            const NUMBER_A: u64 = {
                standard = 1337,
                testing = 42,
            };

            /// NUMBER_B is ...
            const NUMBER_B: u64 = {
                standard = 42,
                testing = 1337,
            };

            /// NUMBER_C is ...
            const NUMBER_C: i32 = {
                standard = 0,
                testing = -1,
            };
        );

        #[cfg(not(feature = "testing"))]
        {
            assert_eq!(NUMBER_A, 1337);
            assert_eq!(NUMBER_B, 42);
            assert_eq!(NUMBER_C, 0);
        }

        #[cfg(feature = "testing")]
        {
            assert_eq!(NUMBER_A, 42);
            assert_eq!(NUMBER_B, 1337);
            assert_eq!(NUMBER_C, -1);
        }
    }
}
