use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    // Bind javascript's console.error.
    #[wasm_bindgen(js_namespace = console)]
    pub fn error(s: &str);

    // Bind javascript's console.log.
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);
}

/// Helper function to get log2 of a number, because for some reason this is a
/// nightly-only feature.
///
/// Source: https://users.rust-lang.org/t/logarithm-of-integers/8506/5.
pub fn log2(x: u32) -> u32 {
    const fn num_bits<T>() -> usize {
        std::mem::size_of::<T>() * 8
    }
    assert!(x > 0);
    num_bits::<u32>() as u32 - x.leading_zeros() - 1
}

pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_log2() {
        let cases = [(1, 0), (2, 1), (3, 1), (4, 2), (5, 2)];

        for (n, expected) in cases {
            assert_eq!(log2(n), expected);
        }
    }
}
