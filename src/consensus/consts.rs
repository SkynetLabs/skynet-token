//! Consensus constants.

use crate::build_var;

build_var!(
    /// Time between blocks in seconds.
    pub const BLOCK_FREQUENCY: u64 = {
        standard = 600,
        testing = 1,
    };
);
