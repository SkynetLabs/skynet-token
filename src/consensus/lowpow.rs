//! lowPoW is a new proof of work algorithm designed specifically for blockchain
//! mining. Unlike typical proof of work algorithms, this algorithm is
//! engineered to consume nearly zero electricity.
//!
//! As of writing, a $10,000 Bitcoin miner consumes about 3400 watts. By
//! comparison, a lowPoW miner costing $10,000 would be expected to consume
//! between 10 and 100 watts.
//!
//! The major premise of lowPoW is that SRAM is very expensive to manufacture
//! and consumes very little electricity when constrained to performing a series
//! of sequential random lookups. Our goal is to create a cryptographic hash
//! function that uses an enormous amount of SRAM that must be accessed
//! sequentially.
//!
//! Other memory hard proof of work algorithms fail to achieve low energy
//! consumption in practice because the operations are easy to parallelize. We
//! have been careful with the design of lowPoW to ensure that there is no
//! possible parallelism, and also that the cryptographic elements of the hash
//! function consume as little electricity as possible.
//!
//! Each hashing core of lowPoW requires 512 KiB of SRAM. Completing a single
//! lowPoW hash requires performing 1,290,240 sequential lookups to SRAM.
//! Between each lookup, the data found by the lookup is randomly mixed with
//! data from previous lookups, which is what enforces the sequential nature of
//! lowPoW.
//!
//! The mixing operations use a combination of addition and XOR on 16 bit
//! fields. Doing addition on 16 bits is meaningfully cheaper in hardware than
//! doing addition on larger fields. On average, lowPoW performs less than four
//! 16 bit additions per sequential memory access, which keeps overall power
//! consumption very low. The XOR operations are slightly more numerous, but in
//! hardware, XOR is nearly free.
//!
//! The total memory size of 512 KiB has been chosen as a tradeoff between DRAM
//! and SRAM. DRAM is significantly cheaper, but is also slower and consumes
//! significantly more power. Because we want to minimize total power usage, we
//! do not want hardware designers to choose designs that include DRAM. A single
//! DRAM chip is rather expensive, and can produce at most performane equivalent
//! to a single SRAM hashing core. By making the total memory requirement 512
//! KiB, we ensure that SRAM hashing cores are cheaper than DRAM hashing cores
//! when providing a similar amount of performance. At larger memory sizes, it
//! may start to make sense to use DRAM instead of SRAM.

use k12::digest::{ExtendableOutput, Update};
use k12::KangarooTwelve as K12;
use std::fmt;
use std::num::Wrapping;

/// A 32-byte hash.
#[derive(Debug, Default, PartialEq, Eq)]
pub struct Hash(pub [u8; 32]);

/// Customization string used for K12.
const LOW_POW_SALT: &[u8] = b"lowPoW-v1";

// low_pow is a new proof of work algorithm designed specifically for blockchain
// mining. Unlike typical proof of work algorithms, this algorithm is engineered
// to consume nearly zero electricity.
#[allow(dead_code)]
pub fn low_pow(seed: &[u8]) -> Hash {
    // Initialize the 512-byte entropy field which is used as the core mixing
    // element of lowPoW. The initialization uses a full cryptographic hash
    // function (Kangaroo12), which consumes a large amount of power. For that
    // reason, we want the entropy field to be as small as possible so that we
    // minimize the total amount of power required to initialize lowPoW.
    //
    // We depend on the randomness of the entropy field to get security in the
    // rest of our hash function. By assuming a uniform random entropy field, we
    // can take a lot of shortcuts in the rest of the mixing, which allows us to
    // reduce the total power consumption.
    let mut seed_entropy = vec![0; 512];
    K12::new_with_customization(LOW_POW_SALT)
        .chain(seed)
        .finalize_xof_into(&mut seed_entropy);

    // We convert the seed entropy to an array of uint16s, since the rest of the
    // algorithm uses all 16 bit operations on the data.
    //
    // The cast is safe because `Wrapping<T>` is guaranteed to have the same
    // layout and ABI as `T`. See
    // https://doc.rust-lang.org/1.60.0/std/num/struct.Wrapping.html#layout.
    let entropy_pool: &mut [Wrapping<u16>] = unsafe {
        std::slice::from_raw_parts_mut(
            seed_entropy.as_mut_ptr() as *mut Wrapping<u16>,
            seed_entropy.len() / 2,
        )
    };

    // Create 512 KiB of memory, split into 4 rows of 128 KiB each. Each row
    // gets 6 extra bytes (3 extra uint16s) so that we don't need to perform an
    // out of bounds check in the core loop. A hardware implementation will not
    // need these trailing bytes, as they don't influence the result of the
    // algorithm.
    let mut board = [[Wrapping(0u16); (1 << 16) + 3]; 4];

    // The core loop performs 1024 rounds of mixing, where each round iterates
    // over the entire entropy pool two bytes at a time - 252 iterations total,
    // for a total of 258,048 acccesses to the entropy pool. Between each access
    // to the entropy pool, there is a sequential access to each of the four
    // memory banks, bringing the total number of sequential memory accesses to
    // 1,290,240.
    //
    // When performing cryptanalysis on this function, it's important to
    // remember that the large SRAM memory banks start off entirely zeroed out.
    // The memory banks achieve 50% saturation somewhere around round 62, and
    // reach 86% saturation by round 128. (meaning 86% of the memory bank is now
    // random data, the remaining data is still zeroes).
    for _ in 0..1024 {
        // Iterate over the 256 elements of the entropy. The mixing function
        // looks ahead 4 steps, so we only actually do 252 total iterations to
        // avoid an out of bounds. The remaining 4 fields do still influence the
        // mixing, so they cannot be omitted.
        for i in 0..entropy_pool.len() - 4 {
            // Define abcde, which are the next 5 elements in the entropy pool.
            // These will be used for mixing.
            let a = entropy_pool[i];
            let b = entropy_pool[i + 1];
            let c = entropy_pool[i + 2];
            let d = entropy_pool[i + 3];
            let e = entropy_pool[i + 4];

            // Get the lookup modifiers. These determine which cells in the SRAM
            // are going to be accessed by each lookup. Because we know the
            // entropy pool has a uniform random distribution, we know that the
            // four memory banks we select to access will also have a uniform
            // random distribution.
            //
            // We use addition here instead of XOR because XOR is the main
            // operation used to mix up our entropy pool between iterations over
            // the entropy pool, and we want non-linearity between them.
            let i0 = a;
            let mut i1 = a + b;
            let mut i2 = a + c;
            let mut i3 = a + d;

            // Get the update modifiers. These decide what the memory banks will
            // be set to after they are accessed. We use addition here for
            // similar reasons to above, and we use a different combination of
            // additions to ensure that all 8 fields follow a uniform random
            // distrubtion between each other.
            let u0 = a + b + c;
            let u1 = a + b + d;
            let u2 = a + c + d;
            let u3 = a + b + c + d;

            // Perform 4 sequential lookups to the 4 SRAM memory banks. After
            // each lookup, we update 8 consecutive bytes (even though the
            // lookup is only 2 bytes) of the memory bank. Updating 8 bytes at
            // once helps us achieve quicker randomness saturation of the memory
            // banks.
            //
            // We XOR the result of each lookup with the lookup modifier for the
            // next lookup, which is what enforces the sequential nature of the
            // lookups. When the memory banks are largely zero, there is
            // potential opportunity to cheat, but this cheating is only
            // materially effective for the first 200 rounds or so, after which
            // the memory banks are sufficiently random that you cannot expect
            // to predict what the next lookup will be. This cheating is the
            // main reason that we need to perform 1024 rounds total, as we want
            // to minimize the overall speedup that can be achieved by cheating.
            //
            // In the first memory bank, we XOR the existing field with the
            // update modifiers directly. In the following memory banks, we also
            // XOR in fields from the entropy pool, to provide additional
            // mixing. The final updates between the 4 memory banks do not
            // follow a uniform random distrubtion, however they do introduce
            // enough entropy to be effective.
            let r0 = board[0][i0.0 as usize];
            board[0][i0.0 as usize] ^= u0;
            board[0][(i0 + Wrapping(1)).0 as usize] ^= u1;
            board[0][(i0 + Wrapping(2)).0 as usize] ^= u2;
            board[0][(i0 + Wrapping(3)).0 as usize] ^= u3;
            i1 ^= r0;
            let r1 = board[1][i1.0 as usize];
            board[1][i1.0 as usize] ^= u0 ^ b;
            board[1][(i1 + Wrapping(1)).0 as usize] ^= u1 ^ b;
            board[1][(i1 + Wrapping(2)).0 as usize] ^= u2 ^ b;
            board[1][(i1 + Wrapping(3)).0 as usize] ^= u3 ^ b;
            i2 ^= r1;
            let r2 = board[2][i2.0 as usize];
            board[2][i2.0 as usize] ^= u0 ^ c;
            board[2][(i2 + Wrapping(1)).0 as usize] ^= u1 ^ c;
            board[2][(i2 + Wrapping(2)).0 as usize] ^= u2 ^ c;
            board[2][(i2 + Wrapping(3)).0 as usize] ^= u3 ^ c;
            i3 ^= r2;
            let r3 = board[3][i3.0 as usize];
            board[3][i3.0 as usize] ^= u0 ^ d;
            board[3][(i3 + Wrapping(1)).0 as usize] ^= u1 ^ d;
            board[3][(i3 + Wrapping(2)).0 as usize] ^= u2 ^ d;
            board[3][(i3 + Wrapping(3)).0 as usize] ^= u3 ^ d;

            // Update the entropy pool to get new board lookup locations. For
            // the first several rounds of the algorithm, r0 through r3 are
            // going to be zero, which means they are not contributing any
            // entropy. That's why we XOR each field with the next field in the
            // entropy pool, this ensures that we get a large number of randomly
            // distributed values in the early rounds. In the later rounds, the
            // rX values will no longer be zero, which provides meaningful
            // mixing of the entropy pool.
            //
            // To make this operation hardware friendly, we do not want to wrap
            // around when doing the XORs, as moving data over long distances is
            // not hardware friendly. This constraint does limit the total
            // amount of mixing that we get while the rX values are zero,
            // especially the later values in the entropy pool will start to
            // repeat after just a few rounds. The earlier half of the values
            // won't start to repeat for several hundreds, which is more than
            // enough time for the rX values to start providing meaningful new
            // entropy.
            entropy_pool[i] = (a ^ b) + r0;
            entropy_pool[i + 1] = (b ^ c) + r1;
            entropy_pool[i + 2] = (c ^ d) + r2;
            entropy_pool[i + 3] = (d ^ e) + r3;
        }
    }

    // To produce the final output, we hash the entropy pool. This ensures that
    // all of the entropy provided by the mixing stage of lowPoW is captured in
    // the result.
    let mut hash = Hash::default();
    K12::new_with_customization(LOW_POW_SALT)
        .chain(seed_entropy)
        .finalize_xof_into(&mut hash.0);
    hash
}

// To use the `{}` marker, the trait `fmt::Display` must be implemented
// manually for the type.
impl fmt::Display for Hash {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{}", hex::encode(self.0))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn low_pow_smoke() {
        // compare output against the Go implementation, which can be found at
        // https://go.dev/play/p/I0YKYdq3qH3
        assert_eq!(
            low_pow(b"example input (typically a block header)").to_string(),
            "62e6b21521e7592d4c77e30f649db34b5ac8920bd9be0c233cec01498dabcbdf"
        );
    }
}
