//! Test miner.

#![allow(dead_code)]

use crate::build_critical;
use crate::consensus::{
    block::{self, consts, BlockHeader, SolvedBlockHeader},
    emergency_mode,
    target::Target,
    time::current_timestamp,
};
use crate::encoding::{decode, encode};
use crate::test_utils::Random;
use anyhow;
use lazy_static::lazy_static;
use std::{sync, thread, time};

lazy_static! {
    pub static ref TEST_MINER: sync::RwLock<TestMiner> = sync::RwLock::new(TestMiner::new());
}

pub struct TestMiner {
    pub blocks: Vec<SolvedBlockHeader>,
    target: Target,
}

impl TestMiner {
    /// Initializes the TestMiner with a test genesis block.
    pub fn new() -> Self {
        Self {
            target: *consts::GENESIS_CHILD_TARGET,
            blocks: vec![SolvedBlockHeader {
                header: consts::GENESIS_BLOCK.clone(),
                id: consts::GENESIS_ID.clone(),
            }],
        }
    }

    /// Adds a block to the consensus set.
    ///
    /// Modeled after siad::modules::miner::testminer::AddBlock.
    pub fn mine_block(&mut self) -> &SolvedBlockHeader {
        let block = self.find_block();
        self.target = *block.header.child_target();
        self.blocks.push(block);
        self.last_block()
    }

    /// Mines up to n blocks, if that many haven't been mined yet.
    pub fn mine_up_until(&mut self, n: usize) {
        const SLEEP_DURATION: u64 = 200;

        while self.blocks.len() < n {
            let start = time::Instant::now();
            _ = self.mine_block();
            let end = time::Instant::now();

            if end - start < time::Duration::from_millis(SLEEP_DURATION) {
                // Sleep to avoid invalid blocks due to "timestamp is not
                // greater than the median timestamp of the previous 11 blocks"
                // error.
                thread::sleep(time::Duration::from_millis(
                    SLEEP_DURATION - ((end - start).as_millis() as u64),
                ));
            }
        }
    }

    pub fn last_block(&self) -> &SolvedBlockHeader {
        self.blocks.last().expect("There should never be no blocks")
    }

    /// Finds at most one block that extends the current blockchain.
    ///
    /// Modeled after siad::modules::miner::testminer::FindBlock.
    fn find_block(&self) -> SolvedBlockHeader {
        let block = self.block_for_work().unwrap_or_else(|err| {
            build_critical!("Could not find block for work: {}", err);
        });
        let target = self.target;
        block.solve(target).unwrap_or_else(|err| {
            build_critical!("Could not solve block: {}", err);
        })
    }

    /// Returns a block that is ready for nonce grinding.
    fn block_for_work(&self) -> anyhow::Result<BlockHeader> {
        let prev_block = self.last_block();

        // For now, use the current block height as the unique skylink.
        let mut skylink_bytes = encode(&(self.blocks.len() as u32));
        skylink_bytes.extend(&[0; 30]);
        let block = decode(&skylink_bytes)?;

        // Use a random pow_mask.
        let pow_mask = block::Hash::random();

        // TODO: Is this right? What happens if the emergency mode changes while
        // solving the block?
        let emergency_mode = emergency_mode::is_emergency_mode(&prev_block.header);

        Ok(prev_block.derive_child(block, pow_mask, emergency_mode, current_timestamp())?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::consensus::block::verify;

    // Test that mined blocks pass verification.
    #[test]
    fn test_verify_mined_blocks() -> anyhow::Result<()> {
        const NUM_BLOCKS: usize = 15;

        // Make sure we have at least 15 blocks.
        TEST_MINER.write().unwrap().mine_up_until(NUM_BLOCKS);

        let genesis_block = &TEST_MINER.read().unwrap().blocks[0];
        let mut header_cache = verify::HeaderCache::initialize(&genesis_block.header)?;

        for (i, (prev_block, current_block)) in TEST_MINER.read().unwrap().blocks[..NUM_BLOCKS]
            .windows(2)
            .map(|pair| (pair[0].header.clone(), pair[1].header.clone()))
            .enumerate()
        {
            let download = verify::Reader::from_block(current_block.block());

            assert_eq!(prev_block.height(), i as u64 + 1);
            verify::verify_block(&current_block, &prev_block, download, &mut header_cache)?;
        }

        Ok(())
    }
}
