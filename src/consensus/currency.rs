use auto_ops::impl_op_ex;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct Currency {
    pub n: u64,
}

impl Currency {
    #[allow(dead_code)]
    pub const MAX: Currency = Currency { n: u64::MAX };

    pub fn new(n: u64) -> Currency {
        Currency { n }
    }

    #[allow(dead_code)]
    pub fn zero() -> Currency {
        Currency::new(0)
    }
}

impl_op_ex!(+ |a: &Currency, b: u64| -> Currency {
    let sum: Option<u64> = a.n.checked_add(b);
    Currency::new(sum.expect("overflow detected"))
});

impl_op_ex!(+ |a: &Currency, b: &Currency| -> Currency {
    let sum: Option<u64> = a.n.checked_add(b.n);
    Currency::new(sum.expect("overflow detected"))
});

impl_op_ex!(+= |a: &mut Currency, b: Currency| {
    a.n = a.n.checked_add(b.n).expect("overflow detected");
});

impl_op_ex!(-|a: &Currency, b: u64| -> Currency {
    let sub: Option<u64> = a.n.checked_sub(b);
    Currency::new(sub.expect("underflow detected"))
});

impl_op_ex!(-|a: &Currency, b: &Currency| -> Currency {
    let sub: Option<u64> = a.n.checked_sub(b.n);
    Currency::new(sub.expect("underflow detected"))
});

impl_op_ex!(-= |a: &mut Currency, b: Currency| {
    a.n = a.n.checked_sub(b.n).expect("underflow detected");
});

impl_op_ex!(*|a: &Currency, b: u64| -> Currency {
    let mul: Option<u64> = a.n.checked_mul(b);
    Currency::new(mul.expect("overflow detected"))
});

impl_op_ex!(*|a: &Currency, b: &Currency| -> Currency {
    let mul: Option<u64> = a.n.checked_mul(b.n);
    Currency::new(mul.expect("overflow detected"))
});

impl_op_ex!(*= |a: &mut Currency, b: Currency| {
    a.n = a.n.checked_mul(b.n).expect("overflow detected");
});

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_max() {
        assert_eq!(Currency::MAX, Currency::new(u64::MAX));
    }

    #[test]
    fn test_zero() {
        assert_eq!(Currency::zero(), Currency::new(0));
    }

    #[test]
    #[allow(clippy::op_ref)]
    fn test_add() {
        let u1: u64 = 1;
        let c1 = Currency::new(1);
        let c2 = Currency::new(2);
        let c3 = Currency::new(3);
        let c_one_off_max = Currency::new(u64::MAX - 1);

        assert_eq!(&c1 + u1, c2);
        assert_eq!(c1 + c2, c3);
        assert_eq!(c_one_off_max + c1, Currency::MAX);
    }

    #[test]
    fn test_add_eq() {
        let mut c = Currency::new(1);
        c += c;
        assert_eq!(c, Currency::new(2));
    }

    #[test]
    #[should_panic(expected = "overflow detected")]
    fn test_add_overflow() {
        _ = Currency::MAX + Currency::new(1);
    }

    #[test]
    #[should_panic(expected = "overflow detected")]
    #[allow(clippy::op_ref)]
    fn test_add_overflow_borrowed() {
        _ = Currency::MAX + &Currency::new(1);
    }

    #[test]
    #[should_panic(expected = "overflow detected")]
    fn test_add_overflow_u64() {
        _ = Currency::MAX + 1;
    }

    #[test]
    #[allow(clippy::op_ref)]
    fn test_sub() {
        let u1: u64 = 1;
        let c1 = Currency::new(1);
        let c2 = Currency::new(2);
        let c3 = Currency::new(3);
        assert_eq!(&c1 - u1, Currency::zero());
        assert_eq!(c3 - c2, c1);
        assert_eq!(c1 - c1, Currency::zero());
    }

    #[test]
    fn test_sub_eq() {
        let mut c = Currency::new(1);
        c -= c;
        assert_eq!(c, Currency::zero());
    }

    #[test]
    #[should_panic(expected = "underflow detected")]
    fn test_sub_underflow() {
        _ = Currency::zero() - Currency::new(1);
    }

    #[test]
    #[should_panic(expected = "underflow detected")]
    #[allow(clippy::op_ref)]
    fn test_sub_underflow_borrowed() {
        _ = Currency::zero() - &Currency::new(1);
    }

    #[test]
    #[should_panic(expected = "underflow detected")]
    fn test_sub_underflow_u64() {
        _ = Currency::zero() - 1;
    }

    #[test]
    #[allow(clippy::op_ref)]
    fn test_mul() {
        let u2: u64 = 2;
        let c2 = Currency::new(2);
        let c3 = Currency::new(3);
        let c6 = Currency::new(6);

        assert_eq!(&c3 * u2, c6);
        assert_eq!(c2 * c3, c6);
    }

    #[test]
    fn test_mul_eq() {
        let mut c = Currency::new(2);
        c *= c;
        assert_eq!(c, Currency::new(4));
    }

    #[test]
    #[should_panic(expected = "overflow detected")]
    fn test_mul_overflow() {
        _ = Currency::MAX * Currency::new(2);
    }

    #[test]
    #[should_panic(expected = "overflow detected")]
    #[allow(clippy::op_ref)]
    fn test_mul_overflow_borrowed() {
        _ = Currency::MAX * &Currency::new(2);
    }

    #[test]
    #[should_panic(expected = "overflow detected")]
    fn test_mul_overflow_u64() {
        _ = Currency::MAX * 2;
    }
}
