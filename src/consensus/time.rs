//! Time-related utilities.

use std::time;
use std::time::SystemTime;

/// A unix timestamp marking the creation time of a block.
pub type BlockTimestamp = u64;

/// The number of seconds in an hour.
pub const SECS_IN_HOUR: u64 = 60 * 60;

/// Returns the BlockTimestamp for the current time.
pub fn current_timestamp() -> BlockTimestamp {
    SystemTime::now()
        .duration_since(time::UNIX_EPOCH)
        .expect("failed to compute time since UNIX_EPOCH - this should never happen")
        .as_secs()
}
