use crate::build_var;
use crate::consensus::{block::BlockHeader, time::current_timestamp};
use lazy_static::lazy_static;
use num::BigRational;
use num_bigint::BigInt;

build_var! {
    pub const EMERGENCY_MODE_ACTIVATION_THRESHOLD: u64 = {
        standard = crate::consensus::time::SECS_IN_HOUR * 16,
        testing = 5,
    };
}

/// The emergency mode is enabled when the time since the last block has been >
/// [`EmergencyModeActivationThreshold`]. In that case we replace some of the
/// difficulty algorithm vars.
pub fn is_emergency_mode(last_block_header: &BlockHeader) -> bool {
    let current_timestamp = current_timestamp();
    let last_block_timestamp = last_block_header.lite_header.timestamp;

    if current_timestamp < last_block_timestamp {
        return false;
    }

    current_timestamp - last_block_timestamp > EMERGENCY_MODE_ACTIVATION_THRESHOLD
}

// These types can't be constructed in a const context.
// See https://github.com/rust-num/num-bigint/issues/181.
lazy_static! {
    pub static ref EMERGENCY_OAK_DECAY_NUMERATOR: BigInt = 800.into();
    pub static ref EMERGENCY_OAK_DECAY_DENOMINATOR: BigInt = 1_000.into();
    pub static ref EMERGENCY_OAK_MAX_RISE: BigRational =
        BigRational::new(1004.into(), 1_000.into());
    pub static ref EMERGENCY_OAK_MAX_DROP: BigRational =
        BigRational::new(1_000.into(), 3_000.into());
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::Random;

    #[test]
    fn test_is_emergency_mode() {
        // Test when emergency mode should be on.
        let mut last_block_header = BlockHeader::random();
        // Set the timestamp to be right below the activation threshold.
        last_block_header.lite_header.timestamp =
            current_timestamp() - EMERGENCY_MODE_ACTIVATION_THRESHOLD - 1;
        assert!(is_emergency_mode(&last_block_header));

        // Test when emergency mode should be off.
        last_block_header.lite_header.timestamp =
            current_timestamp() - EMERGENCY_MODE_ACTIVATION_THRESHOLD / 2;
        assert!(!is_emergency_mode(&last_block_header));

        // Test when the timestamp is right above the activation threshold.
        last_block_header.lite_header.timestamp =
            current_timestamp() - EMERGENCY_MODE_ACTIVATION_THRESHOLD + 1;
        assert!(!is_emergency_mode(&last_block_header));

        // Test when the current timestamp is less than the last block timestamp.
        last_block_header.lite_header.timestamp = current_timestamp() + 10;
        assert!(!is_emergency_mode(&last_block_header));
    }
}
