use crate::encoding::{Decode, Encode, Result};
#[cfg(test)]
use crate::test_utils::Random;
use sha3::digest::Output;
use sha3::{Digest, Sha3_256};
use std::clone::Clone;

/// The prefix to prepend when hashing a leaf of the merkle tree.
const LEAF_SUM_PREFIX: [u8; 1] = [0x00];

/// The prefix to prepend when hashing a node of the merkle tree.
const NODE_SUM_PREFIX: [u8; 1] = [0x01];

/// The merkle hash.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Hash(pub [u8; 32]);

impl From<OutputHash> for Hash {
    fn from(output_hash: OutputHash) -> Self {
        Self(
            output_hash
                .try_into()
                .expect("The output hash should fit in the Hash type"),
        )
    }
}

impl Encode for Hash {
    const SIZE: usize = 32;

    fn encode(&self, bytes: &mut [u8]) {
        self.0.encode(bytes);
    }
}
impl Decode for Hash {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(Self(bytes.try_into()?))
    }
}

#[cfg(test)]
impl Random for Hash {
    fn random() -> Self {
        Self(<[u8; 32]>::random())
    }
}

/// Convenience type. Used internally instead of `Hash` to minimize
/// back-and-forth conversions between `Hash` and `GenericArray`.
type OutputHash = Output<Sha3_256>;

/// Computes the hash of a leaf.
/// # Arguments
///
/// * `data` - some binary data which is then hashed into a leaf
///
fn leaf_sum(data: impl AsRef<[u8]>) -> OutputHash {
    let mut hasher = Sha3_256::new();
    hasher.update(LEAF_SUM_PREFIX);
    hasher.update(data);
    hasher.finalize()
}

/// Proof contains all information related to a merkle proof for a single leaf.
struct Proof<'a> {
    // proof_base is the data of the leaf for which to prove membership within
    // the tree.
    #[allow(dead_code)]
    proof_base: &'a [u8],

    // proof_set contains the intermediary hashes of the proof.
    #[allow(dead_code)]
    proof_set: Vec<OutputHash>,

    // proof_index is the index of the leaf described by proof_base.
    #[allow(dead_code)]
    proof_index: u64,
}

/// Tree describes a merkle tree.
pub struct Tree {
    /// The stack of the tree. Whenever 2 subtrees of the same height are next
    /// to each other, they get joined into a SubTree with an incremented
    /// height.
    stack: Vec<SubTree>,

    // The current leaf index. This is incremented every time another leaf is
    // pushed.
    current_index: u64,

    // proof_base is the data of the leaf for which to create a proof. it's heap
    // allocated to avoid a lifetime restriction on the tree where all leaves'
    // data has to outlive the tree. That way we can stream leaves.
    proof_base: Option<Box<[u8]>>,

    // The leaf index which is contained within the proof.
    proof_index: u64,

    // The hashes that are required to create the proof.
    proof_set: Vec<OutputHash>,

    // Indicator for whether this tree is a tree for creating a proof or just a
    // tree to compute a merkle root.
    proof_tree: bool,
}

impl Tree {
    /// Pushes some data to the tree as a new leaf.
    /// # Arguments
    ///
    /// * `data` - the data segment to hash and add as a leaf.
    ///
    #[allow(dead_code)]
    pub fn push_leaf(&mut self, data: &[u8]) {
        // Hash the leaf.
        let leaf_sum = leaf_sum(data);

        // The first element of a proof is the data at the proof index. If this
        // data is being inserted at the proof index, it is added to the proof
        // set.
        if self.proof_tree && self.current_index == self.proof_index {
            self.proof_base = Some(Box::from(data));
            self.proof_set.push(leaf_sum);
        }

        // Create a subtree of height 0. The sum of the new node is going to be
        // the data for cached trees, and is going to be the result of calling
        // leaf_sum() on the data for standard trees. Doing a check here prevents
        // needing to duplicate the entire 'Push' function for the trees.
        self.stack.push(SubTree {
            height: 0,
            sum: leaf_sum,
        });

        // Join subTrees if possible.
        self.join_subtrees();

        // Update the index.
        self.current_index += 1;
    }

    #[allow(dead_code)]
    fn join_subtrees(&mut self) {
        // Grab stack for convenience.
        let s = &mut self.stack;

        // Loop over the stack as long as the last two elements have the same
        // height.
        while s.len() > 1 && s[s.len() - 1].height == s[s.len() - 2].height {
            let i = s.len() - 1;
            let j = s.len() - 2;

            // Before combining subtrees, check whether one of the subtree
            // hashes needs to be added to the proof set. This is going to be
            // true IFF the subtrees being combined are one height higher than
            // the previous subtree added to the proof set. The height of the
            // previous subtree added to the proof set is equal to
            // t.proof_set.len() - 1.
            if self.proof_tree
                && !self.proof_set.is_empty()
                && s[i].height as usize == self.proof_set.len() - 1
            {
                // One of the subtrees needs to be added to the proof set. The
                // subtree that needs to be added is the subtree that does not
                // contain the proofIndex. Because the subtrees being compared
                // are the smallest and rightmost trees in the Tree, this can be
                // determined by rounding the currentIndex down to the number of
                // nodes in the subtree and comparing that index to the
                // proofIndex.
                let leaves = 1 << s[i].height;
                let mid = (self.current_index / leaves) * leaves;
                if self.proof_index < mid {
                    self.proof_set.push(s[i].sum);
                } else {
                    self.proof_set.push(s[j].sum);
                }

                // Sanity check - the proofIndex should never be less than the
                // midpoint minus the number of leaves in each subtree.
                assert!(self.proof_index >= mid - leaves);
            }

            // Join the two subTrees into one subTree with a greater height.
            let last = s.pop().unwrap();
            let second_to_last = s.pop().unwrap();
            s.push(second_to_last.join(&last));
        }
    }

    /// prove creates a Merkle Proof using a tree. To create a proof, set_index
    /// must be called on the tree after creating it. Otherwise it may only be
    /// used for computing the merkle root of the tree.
    #[allow(dead_code)]
    fn prove(&self) -> Option<Proof> {
        assert!(self.proof_tree);

        // Return None if the Tree is empty, or if the proof_index hasn't yet been
        // reached.
        if self.stack.is_empty() || self.proof_set.is_empty() || self.proof_base.is_none() {
            return None;
        }
        let mut proof_set = self.proof_set.clone();

        // The set of subtrees must now be collapsed into a single root. The proof
        // set already contains all of the elements that are members of a complete
        // subtree. Of what remains, there will be at most 1 element provided from
        // a sibling on the right, and all of the other proofs will be provided
        // from a sibling on the left. This results from the way orphans are
        // treated. All subtrees smaller than the subtree containing the proofIndex
        // will be combined into a single subtree that gets combined with the
        // proofIndex subtree as a single right sibling. All subtrees larger than
        // the subtree containing the proofIndex will be combined with the subtree
        // containing the proof index as left siblings.

        // Start at the smallest subtree and combine it with larger subtrees until
        // it would be combining with the subtree that contains the proof index. We
        // can recognize the subtree containing the proof index because the height
        // of that subtree will be one less than the current length of the proof
        // set.
        let mut iter = self.stack.iter().rev().peekable();
        let mut current = iter.next().unwrap().sum;
        while let Some(next) =
            iter.next_if(|next| -> bool { usize::from(next.height) < proof_set.len() - 1 })
        {
            node_sum_into_right(&next.sum, &mut current);
        }

        // If the current subtree is not the subtree containing the proof index,
        // then it must be an aggregate subtree that is to the right of the subtree
        // containing the proof index, and the next subtree is the subtree
        // containing the proof index.
        if iter
            .next_if(|next| usize::from(next.height) == proof_set.len() - 1)
            .is_some()
        {
            proof_set.push(current);
        }

        // The current subtree must be the subtree containing the proof index. This
        // subtree does not need an entry, as the entry was created during the
        // construction of the Tree. Instead, skip to the next subtree.
        //
        // All remaining subtrees will be added to the proof set as a left sibling,
        // completing the proof set.
        for next in iter {
            proof_set.push(next.sum);
        }

        // Drop the first element of the proof_set. It's the hash of the
        // proof_base and can be derived on demand from the proof_base which
        // must be part of the proof anyway.
        proof_set.remove(0);

        Some(Proof {
            proof_base: self.proof_base.as_ref().unwrap(),
            proof_set,
            proof_index: self.proof_index,
        })
    }

    /// root computes the root hash of the merkle tree by hashing together all
    /// subTrees of the tree.
    #[allow(dead_code)]
    pub fn root(&self) -> OutputHash {
        // The root is formed by hashing together subTrees in order from least
        // in height to greatest in height. The taller subtree is the first
        // subtree in the join. If the tree is empty, we return the default
        // Output.
        let mut iter = self.stack.iter().rev().map(|tree| &tree.sum);

        // Grab the first element of the iterator.
        let mut current: OutputHash = if let Some(first) = iter.next() {
            *first
        } else {
            // If there is no first element, return the default.
            return OutputHash::default();
        };

        // Hash the rest of the stack together, reusing the memory of
        // current.
        for next in iter {
            node_sum_into_right(next, &mut current);
        }
        current
    }

    #[allow(dead_code)]
    fn set_index(&mut self, i: u64) {
        assert_eq!(self.stack.len(), 0);
        self.proof_tree = true;
        self.proof_index = i;
    }
}

impl Default for Tree {
    /// Creates an empty tree with a reasonable capacity.
    fn default() -> Self {
        Tree {
            // preallocate a stack large enough for most trees
            stack: Vec::with_capacity(32),

            current_index: 0,
            proof_base: None,
            proof_index: 0,
            proof_set: Vec::new(),
            proof_tree: false,
        }
    }
}

/// A SubTree describes a partial merkle tree and consists of an intermediary
/// hash as well as its height. Two neighboring SubTrees with the same height
/// can be joined into a new SubTree with a greater height.
#[derive(Clone)]
struct SubTree {
    /// Describes the height of the SubTree. For a leaf the height is 0.
    height: u16,
    /// The intermediary hash of the subtree.
    sum: OutputHash,
}

impl SubTree {
    /// Joins two SubTrees into a new one, reusing the memory of the existing
    /// SubTree.
    #[allow(dead_code)]
    fn join(&self, other: &Self) -> Self {
        assert_eq!(self.height, other.height);

        SubTree {
            height: self.height + 1,
            sum: node_sum(&self.sum, &other.sum),
        }
    }
}

// verify_proof verifies a merkle proof.
/// # Arguments
///
/// * `root` - the root of the tree as observed by the verifier.
/// * `proof_base` - the data of the leaf for which to prove membership.
/// * `proof_set` - the intermediary hashes required for the proof.
/// * `proof_index` - the index of the leaf for which to prove membership within
/// the tree.
/// * `num_leaves` - the total number of leaves of the tree.
///
#[allow(dead_code)]
fn verify_proof<D: Digest>(
    root: &OutputHash,
    proof_base: &[u8],
    proof_set: &[OutputHash],
    proof_index: u64,
    num_leaves: u64,
) -> bool {
    // Return false for nonsense input.
    if proof_index >= num_leaves {
        return false;
    }

    // In a Merkle tree, every node except the root node has a sibling.
    // Combining the two siblings in the correct order will create the parent
    // node. Each of the remaining hashes in the proof set is a sibling to a
    // node that can be built from all of the previous elements of the proof
    // set. The next node is built by taking:
    //
    //		H(NODE_SUM_PREFIX || sibling A || sibling B)
    //
    // The difficulty of the algorithm lies in determining whether the supplied
    // hash is sibling A or sibling B. This information can be determined by
    // using the proof index and the total number of leaves in the tree.
    //
    // A pair of two siblings forms a subtree. The subtree is complete if it
    // has 1 << height total leaves. When the subtree is complete, the position
    // of the proof index within the subtree can be determined by looking at
    // the bounds of the subtree and determining if the proof index is in the
    // first or second half of the subtree.
    //
    // When the subtree is not complete, either 1 or 0 of the remaining hashes
    // will be sibling B. All remaining hashes after that will be sibling A.
    // This is true because of the way that orphans are merged into the Merkle
    // tree - an orphan at height n is elevated to height n + 1, and only
    // hashed when it is no longer an orphan. Each subtree will therefore merge
    // with at most 1 orphan to the right before becoming an orphan itself.
    // Orphan nodes are always merged with larger subtrees to the left.
    //
    // One vulnerability with the proof verification is that the proof_set may
    // not be long enough. Before looking at an element of proof_set, a check
    // needs to be made that the element exists.

    // The proof_base is the leaf for which to prove membership in the merkle
    // tree.
    let mut sum = leaf_sum(proof_base);

    // Add the hash (the one we dropped at the end of creating the proof) of the
    // proof_base to the proof_set.
    let mut proof_set = Vec::from(proof_set);
    proof_set.insert(0, sum);

    // While the current subtree (of height 'height') is complete, determine the
    // position of the next sibling using the complete subtree algorithm.
    // 'stableEnd' tells us the ending index of the last full subtree. It gets
    // initialized to 'proof_index' because the first full subtree was the
    // subtree of height 1, created above (and had an ending index of
    // 'proof_index').
    let mut height = 1;
    let mut stable_end = proof_index;
    loop {
        // Determine if the subtree is complete. This is accomplished by
        // rounding down the proofIndex to the nearest 1 << 'height', adding 1
        // << 'height', and comparing the result to the number of leaves in the
        // Merkle tree.
        let st_start_index = (proof_index / (1 << height)) * (1 << height); // round down to the nearest 1 << height
        let st_end_index = st_start_index + (1 << height) - 1; // subtract 1 because the start index is inclusive
        if st_end_index >= num_leaves {
            // If the Merkle tree does not have a leaf at index
            // 'subTreeEndIndex', then the subtree of the current height is not
            // a complete subtree.
            break;
        }
        stable_end = st_end_index;

        // Determine if the `proof_index` is in the first or the second half of
        // the subtree.
        if proof_set.len() <= height {
            return false;
        }
        if proof_index - st_start_index < 1 << (height - 1) {
            node_sum_into_left(&mut sum, &proof_set[height]);
        } else {
            node_sum_into_right(&proof_set[height], &mut sum);
        }
        height += 1;
    }

    // Determine if the next hash belongs to an orphan that was elevated. This
    // is the case IFF 'stable_end' (the last index of the largest full subtree)
    // is equal to the number of leaves in the Merkle tree.
    if stable_end != num_leaves - 1 {
        if proof_set.len() <= height {
            return false;
        }
        node_sum_into_left(&mut sum, &proof_set[height]);
        height += 1;
    }

    // All remaining elements in the proof set will belong to a left sibling.
    while height < proof_set.len() {
        node_sum_into_right(&proof_set[height], &mut sum);
        height += 1;
    }

    // Compare our calculated Merkle root to the desired Merkle root.
    &sum == root
}

/// Computes the hash of a node.
/// # Arguments
///
/// * `left` - the hash of a node or a leaf to combine with 'right'.
/// * `right` - the hash of a node or a leaf to combine with 'left'.
///
fn node_sum(left: &OutputHash, right: &OutputHash) -> OutputHash {
    let mut dst = *right;
    node_sum_into_right(left, &mut dst);
    dst
}

/// Like [node_sum] but reuses the memory of 'right'.
/// # Arguments
///
/// * `left` - the hash of a node or a leaf to combine with 'right'.
/// * `right` - the hash of a node or a leaf to combine with 'left'.
///
fn node_sum_into_right(left: &OutputHash, right: &mut OutputHash) {
    let hasher = node_sum_hasher(left, right);
    hasher.finalize_into(right);
}

/// Like [node_sum] but reuses the memory of 'left'.
/// # Arguments
///
/// * `left` - the hash of a node or a leaf to combine with 'right'.
/// * `right` - the hash of a node or a leaf to combine with 'left'.
///
#[allow(dead_code)]
fn node_sum_into_left(left: &mut OutputHash, right: &OutputHash) {
    let hasher = node_sum_hasher(left, right);
    hasher.finalize_into(left);
}

/// Creates a hasher which hashes two sums into a new node sum when finalised.
#[allow(dead_code)]
fn node_sum_hasher(left: &OutputHash, right: &OutputHash) -> Sha3_256 {
    let mut hasher = Sha3_256::new();
    hasher.update(NODE_SUM_PREFIX);
    hasher.update(left);
    hasher.update(&right);
    hasher
}

// ******************** Unit tests ********************
#[cfg(test)]
mod tests {
    use super::*;
    use sha3::Sha3_256;

    #[test]
    fn leaf_sum_smoke() {
        let data = [1, 2, 3];
        let out = leaf_sum(&data);
        assert_eq!(
            "33bad5430899ed6f8beaf3e732b2a2cad1d40b7c9de0cfcdc7e0bc0756803a10",
            hex::encode(out)
        )
    }

    #[test]
    fn node_sum_smoke() {
        let leaf_hash =
            hex::decode("33bad5430899ed6f8beaf3e732b2a2cad1d40b7c9de0cfcdc7e0bc0756803a10")
                .unwrap();
        let data = OutputHash::from_slice(&leaf_hash);
        let out = node_sum(data, data);
        assert_eq!(
            "02c63fd3e90288158186057d99b866db5799dcdc106dca8f01e1d48de550a268",
            hex::encode(&out)
        );
    }

    #[test]
    fn tree_root() {
        let mut tree = Tree::default();

        let data1 = &[1, 2, 3];
        let data2 = &[3, 2, 1];
        let data3 = &[2, 2, 2];

        let sum1 = leaf_sum(data1);
        let sum2 = leaf_sum(data2);
        let sum3 = leaf_sum(data3);

        let sum12 = node_sum(&sum1, &sum2);
        let sum123 = node_sum(&sum12, &sum3);

        // Test empty tree.
        assert_eq!(tree.root(), OutputHash::default());

        // Tree with 1 leaf.
        tree.push_leaf(data1);
        assert_eq!(tree.root(), sum1);

        // Tree with 2 leaves combined into 1 node.
        tree.push_leaf(data2);
        assert_eq!(tree.root(), sum12);

        // Tree with 3 leaves.
        tree.push_leaf(data3);
        assert_eq!(tree.root(), sum123);
    }

    // Makes sure prove can't be called on a tree that doesn't have the proof
    // index set.
    #[test]
    #[should_panic]
    fn tree_prove_panic() {
        let tree = Tree::default();
        tree.prove();
    }

    // Tests some edge cases on empty trees.
    #[test]
    fn tree_prove_empty() {
        let mut tree = Tree::default();
        tree.set_index(1);

        // Test calling prove on empty tree.
        assert!(tree.prove().is_none());

        // Test calling root on empty tree.
        assert_eq!(tree.root(), OutputHash::default());
    }

    // Tests some edge cases on empty trees.
    #[test]
    fn proof_solve_index_out_of_bounds() {
        // Build proof for tree with 1 leaf.
        let mut tree = Tree::default();
        tree.set_index(0);
        tree.push_leaf(&[1, 2, 3]);
        let proof = tree.prove().unwrap();

        // Check proof_index equal to num_leaves.
        let result =
            verify_proof::<Sha3_256>(&tree.root(), proof.proof_base, &proof.proof_set, 1, 1);
        assert!(!result);

        // Check proof_index bigger than num_leaves.
        let result =
            verify_proof::<Sha3_256>(&tree.root(), proof.proof_base, &proof.proof_set, 2, 1);
        assert!(!result);
    }

    // Unit test for verify_proof that tries some invalid inputs.
    #[test]
    fn tree_verify_proof() {
        let mut tree = Tree::default();
        let proof_index = 1;
        tree.set_index(proof_index);

        // Build proof for tree with 3 leaves.
        tree.push_leaf(&[1, 1, 1]);
        tree.push_leaf(&[2, 2, 2]);
        tree.push_leaf(&[3, 3, 3]);

        // Test updating the base to an invalid one.
        let mut proof = tree.prove().unwrap();
        proof.proof_base = &[0, 0, 0];
        let result = verify_proof::<Sha3_256>(
            &tree.root(),
            proof.proof_base,
            &proof.proof_set,
            proof_index,
            3,
        );
        assert!(!result);

        // Empty proof_set.
        let mut proof = tree.prove().unwrap();
        proof.proof_set = Vec::new();
        let result = verify_proof::<Sha3_256>(
            &tree.root(),
            proof.proof_base,
            &proof.proof_set,
            proof_index,
            3,
        );
        assert!(!result);

        // Remove the first element of the set.
        let mut proof = tree.prove().unwrap();
        proof.proof_set.remove(0);
        let result = verify_proof::<Sha3_256>(
            &tree.root(),
            proof.proof_base,
            &proof.proof_set,
            proof_index,
            3,
        );
        assert!(!result);

        // Remove the last element of the set.
        let mut proof = tree.prove().unwrap();
        proof.proof_set.remove(proof.proof_set.len() - 1);
        let result = verify_proof::<Sha3_256>(
            &tree.root(),
            proof.proof_base,
            &proof.proof_set,
            proof_index,
            3,
        );
        assert!(!result);
    }

    // Builds a proof for every leaf in a large tree and verifies it.
    #[test]
    fn tree_prove_exhaustive() {
        let num_leaves = 100;
        for proof_index in 0..num_leaves {
            let mut tree = Tree::default();
            tree.set_index(proof_index);

            for leaf_index in 0..num_leaves {
                let data = leaf_index as u8;
                let tmp = vec![data];
                tree.push_leaf(&tmp)
            }

            // Make sure the proof only verifies correctly for the right proof
            // index.
            let proof = tree.prove().unwrap();
            for verify_index in 0..num_leaves {
                let result = verify_proof::<Sha3_256>(
                    &tree.root(),
                    proof.proof_base,
                    &proof.proof_set,
                    verify_index,
                    num_leaves,
                );
                assert_eq!(result, verify_index == proof.proof_index);
            }
        }
    }

    // Tests some happy cases for using the tree.
    #[test]
    fn tree_prove() {
        let data1 = &[1, 2, 3];
        let data2 = &[3, 2, 1];
        let data3 = &[2, 2, 2];

        let sum1 = leaf_sum(data1);
        let sum2 = leaf_sum(data2);
        let sum3 = leaf_sum(data3);

        let sum12 = node_sum(&sum1, &sum2);
        let sum123 = node_sum(&sum12, &sum3);

        // Declare the test. This tests a proof for a given leaf index and
        // asserts that the proof has the right fields.
        let test = |index: u64, expected_base: &[u8], expected_proof_set: Vec<OutputHash>| {
            let mut tree = Tree::default();
            tree.set_index(index);
            tree.push_leaf(data1);
            tree.push_leaf(data2);
            tree.push_leaf(data3);

            let proof = tree.prove().unwrap();

            assert_eq!(proof.proof_base, expected_base);
            assert_eq!(proof.proof_index, index);
            assert_eq!(proof.proof_set, expected_proof_set);

            let result = verify_proof::<Sha3_256>(
                &sum123,
                proof.proof_base,
                &proof.proof_set,
                proof.proof_index,
                3,
            );
            assert!(result);
        };

        // Run test for every leaf index.
        test(0, data1, vec![sum2, sum3]);
        test(1, data2, vec![sum1, sum3]);
        test(2, data3, vec![sum12]);
    }

    mod hash {
        use super::*;
        use crate::encoding::{decode, encode};

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_hash() {
            const INNER_BYTES: [u8; 32] = [
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                24, 25, 26, 27, 28, 29, 30, 31, 32,
            ];
            const TEST_HASH: Hash = Hash(INNER_BYTES);

            let encoded = encode(&TEST_HASH);
            assert_eq!(encoded, INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_hash() -> Result<()> {
            let hash = Hash::default();
            assert_eq!(decode::<Hash>(&encode(&hash))?, hash);

            let hash = Hash::random();
            assert_eq!(decode::<Hash>(&encode(&hash))?, hash);

            Ok(())
        }

        #[test]
        fn should_generate_random_hash() {
            assert_ne!(Hash::random(), Hash::random());
        }

        #[test]
        fn should_convert_output_hash_to_hash() {
            // Test the default value.
            let output_hash_bytes = [0; 32];
            let output_hash: OutputHash = output_hash_bytes.into();
            let hash: Hash = output_hash.into();
            assert_eq!(hash.0, output_hash_bytes);

            // Test a random value.
            let output_hash_bytes = <[u8; 32]>::random();
            let output_hash: OutputHash = output_hash_bytes.into();
            let hash: Hash = output_hash.into();
            assert_eq!(hash.0, output_hash_bytes);
        }
    }
}
