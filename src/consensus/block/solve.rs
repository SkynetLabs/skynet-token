//! Solve blocks.

// TODO: Should this function go somewhere else?

use super::{BlockHeader, Nonce};
use crate::consensus::{
    block,
    lowpow::{self, low_pow},
    target::Target,
};
use crate::encoding::{encode, Encode};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("failed to solve block")]
    FailedToSolve,
}

pub type Result<T> = std::result::Result<T, Error>;

impl BlockHeader {
    /// Takes a block header and a target and tries to solve the block for the
    /// target. A result is returned indicating whether the block was
    /// successfully solved.
    ///
    /// Based on siad::modules::miner::testminer::solveBlock.
    pub fn solve(mut self, target: Target) -> Result<block::SolvedBlockHeader> {
        // Assemble the header.
        let mut encoded_header = self.encode_for_pow();

        for nonce in 0..u64::MAX {
            let encoded_nonce = encode(&nonce);
            // Update nonce in encoding.
            encoded_header.0[0..Nonce::SIZE].copy_from_slice(&encoded_nonce);

            let mut hash = low_pow(&encoded_header.0);
            self.apply_pow_mask(&mut hash);

            if hash.0 <= target.0 {
                self.lite_header.nonce = Nonce(
                    encoded_nonce
                        .as_slice()
                        .try_into()
                        .expect("conversion from u64 to [u8; 8] expected to succeed"),
                );
                return Ok(block::SolvedBlockHeader {
                    header: self,
                    id: block::BlockId(hash.0),
                });
            }
        }

        Err(Error::FailedToSolve)
    }

    /// XOR the PoW hash with arbitrary bytes.
    pub fn apply_pow_mask(&self, hash: &mut lowpow::Hash) {
        for i in 0..32 {
            hash.0[i] ^= self.lite_header.pow_mask.0[i];
        }
    }

    /// Returns the ID of the block which is the PoW hash. This method is slow,
    /// hence the verbose name.
    pub fn compute_id(&self) -> block::BlockId {
        let encoded_header = self.encode_for_pow();
        let mut hash = low_pow(&encoded_header.0);
        self.apply_pow_mask(&mut hash);
        block::BlockId(hash.0)
    }
}

#[cfg(test)]
mod tests {
    use super::super::test_consts::*;
    use super::*;
    use crate::test_utils::Random;

    #[test]
    fn test_solve_block() -> Result<()> {
        // Use a hard-coded target that's not too hard so that this finishes in
        // reasonable time.
        let target = Target([20; 32]);

        // Test a hard-coded block header.
        {
            let block = BlockHeader::default();

            let solved_block = block.solve(target)?;

            // Test against a hard-coded nonce.
            assert_eq!(
                solved_block.header.lite_header.nonce,
                Nonce([8, 0, 0, 0, 0, 0, 0, 0])
            );

            // Confirm that the nonce works.
            let encoded_header = solved_block.header.encode_for_pow();
            let mut hash = low_pow(&encoded_header.0);
            solved_block.header.apply_pow_mask(&mut hash);
            // The hash should be below the target.
            assert!(hash.0 < target.0);
        }

        // Test a random block header.
        {
            let block = BlockHeader::random();

            let solved_block = block.solve(target)?;

            // Confirm that the nonce works.
            let encoded_header = solved_block.header.encode_for_pow();
            let mut hash = low_pow(&encoded_header.0);
            solved_block.header.apply_pow_mask(&mut hash);
            // The hash should be below the target.
            assert!(hash.0 < target.0);
        }

        Ok(())
    }

    // Hard-code the expected ID.
    #[test]
    fn should_get_the_expected_block_id() {
        const EXPECTED_ID_BYTES: &[u8] = &[
            218, 170, 49, 220, 25, 87, 87, 169, 91, 35, 163, 121, 42, 167, 187, 180, 43, 16, 2, 98,
            99, 3, 10, 15, 23, 180, 154, 145, 96, 240, 110, 192,
        ];

        let id = TEST_BLOCK_HEADER.compute_id();
        assert_eq!(id.0, EXPECTED_ID_BYTES);
    }
}
