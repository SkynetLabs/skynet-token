use super::{
    Block, BlockHeader, BlockHeight, BlockId, BlockTimestamp, Hash, LiteHeader, Nonce,
    PrevBlockHeaders, PrevTimestamps, Skylink, Target, NUM_PREV_BLOCK_HEADERS, RAW_SKYLINK_SIZE,
};
use crate::consensus::merkletree;
#[cfg(test)]
use crate::test_utils::Random;
#[cfg(test)]
use rand::Rng;

impl Random for BlockId {
    fn random() -> Self {
        Self(<[u8; 32]>::random())
    }
}

impl Random for Hash {
    fn random() -> Self {
        Self(<[u8; 32]>::random())
    }
}

impl Random for Nonce {
    fn random() -> Self {
        Self(<[u8; 8]>::random())
    }
}

impl Random for Skylink {
    fn random() -> Self {
        Self([0; RAW_SKYLINK_SIZE].map(|_| rand::thread_rng().gen_range(0..u8::MAX)))
    }
}

impl Random for PrevTimestamps {
    fn random() -> Self {
        let mut result = Self::default();
        result.0 = result.0.map(|_| BlockTimestamp::random());
        result
    }
}

impl Random for LiteHeader {
    fn random() -> Self {
        Self {
            parent_id: BlockId::random(),
            timestamp: BlockTimestamp::random(),
            pow_mask: Hash::random(),
            nonce: Nonce::random(),
            block: Skylink::random(),
            state_root: merkletree::Hash::random(),
            num_leaves: u64::random(),
            num_account_updates: u16::random(),
            num_new_accounts: u16::random(),
        }
    }
}

impl Random for BlockHeader {
    fn random() -> Self {
        Self {
            lite_header: LiteHeader::random(),
            base_fee_step: u16::random(),
            decayed_total_target: Target::random(),
            target: Target::random(),
            child_target: Target::random(),
            prev_timestamps: PrevTimestamps::random(),
            parent_decayed_total_target: Target::random(),
            parent_decayed_total_time: u64::random(),
            height: BlockHeight::random(),
        }
    }
}

impl Random for Block {
    fn random() -> Self {
        Self {
            prev_header: BlockHeader::random(),
            block_headers: PrevBlockHeaders::random(),
        }
    }
}

impl Random for PrevBlockHeaders {
    fn random() -> Self {
        let mut result = Self::default();
        let mut last_none_index: usize = 0;

        // 50% of the time, fill in some `None`s.
        if bool::random() {
            // Fill in a random, non-zero amount of `None`s.
            last_none_index = rand::thread_rng().gen_range(1..NUM_PREV_BLOCK_HEADERS);
        }

        // Fill in the rest with `Some`s.
        for block_header in &mut result.0[last_none_index..] {
            *block_header = Some(LiteHeader::random());
        }

        result
    }
}
