use super::{BlockHeight, BlockId, BlockTimestamp, Target};
use crate::encoding;
use num::bigint::BigInt;
use num_bigint::TryFromBigIntError;
use std::io;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("error while decoding")]
    Decode(#[from] encoding::Error),
    #[error("error while performing io")]
    Io(#[from] io::Error),
    #[error("error while converting BigInt to u64")]
    TryFromBigInt(#[from] TryFromBigIntError<BigInt>),

    #[error("block's base_fee_step did not match expected base_fee_step. (block's base_fee_step {step:?}, expected base_fee_step {expected_step:?})")]
    BaseFeeStepDidNotMatch { step: u16, expected_step: u16 },
    #[error("block's height did not match parent's height + 1. (block's height {height:?}, parent's height {parent_height:?})")]
    HeightDidNotMatch {
        height: BlockHeight,
        parent_height: BlockHeight,
    },
    #[error("block's parent height is already at the maximum height. (block's parent height {parent_height:?})")]
    MaximumParentHeight { parent_height: BlockHeight },
    #[error("computed decayed_total_target did not match. (new decayed_total_target {new_target:?}, block's decayed_total_target {expected_target:?})")]
    NewDecayedTotalTargetDidNotMatch {
        new_target: Target,
        expected_target: Target,
    },
    #[error("computed decayed_total_time did not match. (new parent_decayed_total_time {new_time:?}, parent's decayed_total_time {expected_time:?})")]
    NewDecayedTotalTimeDidNotMatch { new_time: u64, expected_time: u64 },
    #[error("parent block did not match block in history (height {height:?})")]
    ParentBlockDidNotMatchHistory { height: BlockHeight },
    #[error("block history did not match parent block history. (mismatch index {index:?}, parent mismatch index {parent_index:?}, height {height:?})")]
    ParentBlockHistoryDidNotMatch {
        index: usize,
        parent_index: usize,
        height: Option<BlockHeight>,
    },
    #[error("parent's decayed_total_target did not match the target in the parent header. (parent's decayed_total_target {parent_target:?}, parent header's decayed_total_target {expected_target:?})")]
    ParentDecayedTotalTargetDidNotMatch {
        parent_target: Target,
        expected_target: Target,
    },
    #[error("the parent's child_target did not match the target of this block. (parent's child_target {parent_child_target:?}, block's target {expected_target:?})")]
    ParentChildTargetDidNotMatch {
        parent_child_target: Target,
        expected_target: Target,
    },
    #[error("the header cache was called with a non-sequential parent header height. (parent header height {parent_header_height:?}, cache height {cache_height:?})")]
    ParentHeaderHeightDidNotMatchHeaderCache {
        parent_header_height: BlockHeight,
        cache_height: BlockHeight,
    },
    #[error("parent's id did not match parent header's id. (parent's id {parent_id:?}, parent header's id {expected_id:?})")]
    ParentIdDidNotMatch {
        parent_id: BlockId,
        expected_id: BlockId,
    },
    #[error("the pow hash for the block did not beat the target. (target {target:?})")]
    PowDidNotBeatTarget { target: Target },
    #[error("previous timestamps did not match")]
    PrevTimestampsDidNotMatch,
    #[error("timestamp is more than 3 hours into the future but less than 6 hours into the future (timestamp {0})")]
    TimestampMoreThanThreeHoursAway(BlockTimestamp),
    #[error("timestamp is more than 6 hours into the future (timestamp {0})")]
    TimestampMoreThanSixHoursAway(BlockTimestamp),
    #[error("timestamp is not greater than the median timestamp of the previous 11 blocks (timestamp {block_timestamp:?}, median timestamp {median_timestamp:?})")]
    TimestampNotGreaterThanMedian {
        block_timestamp: BlockTimestamp,
        median_timestamp: BlockTimestamp,
    },
}

pub type Result<T> = std::result::Result<T, Error>;
