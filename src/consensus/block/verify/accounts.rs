use super::super::BlockHeader;
use super::{error::Result, AccountUpdatesMap};
use std::collections::HashMap;

// Dummy struct for now.
pub struct AccountUpdateLeaf();

// Dummy struct for now.
pub struct AccountUpdateVerifier();

// Dummy struct for now.
pub struct NewAccountVerifier();

pub fn verify_account_shard_offsets(_bytes: &[u8]) -> Result<()> {
    unimplemented!();
}

pub fn verify_active_accounts(_bytes: &[u8]) -> Result<()> {
    unimplemented!();
}

pub fn verify_account_updates(_bytes: Vec<u8>) -> Result<AccountUpdatesMap> {
    unimplemented!();
}

pub fn verify_new_account_shard_offsets(_bytes: &[u8]) -> Result<()> {
    unimplemented!();
}

pub fn verify_new_accounts(
    _header: &BlockHeader,
    _bytes: Vec<u8>,
) -> Result<HashMap<NewAccountVerifier, bool>> {
    unimplemented!();
}

pub fn verify_account_updates_map(
    _account_updates_map: HashMap<AccountUpdateLeaf, AccountUpdateVerifier>,
) -> Result<()> {
    unimplemented!();
}

pub fn verify_new_accounts_map(_new_accounts_map: HashMap<NewAccountVerifier, bool>) -> Result<()> {
    unimplemented!();
}
