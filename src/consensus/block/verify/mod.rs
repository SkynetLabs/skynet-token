//! Full logic for block verification.

mod accounts;
mod block_header;
mod error;
mod header_cache;
mod leaves;
#[cfg(not(test))]
mod reader;
#[cfg(test)]
mod test_reader;
mod transactions;

pub use header_cache::HeaderCache;
#[cfg(not(test))]
pub use reader::Reader;
#[cfg(test)]
pub use test_reader::Reader;

use super::{BlockHeader, BlockHeight, BlockId, BlockTimestamp, Encode, PrevBlockHeaders, Target};
use crate::consensus::{emergency_mode::is_emergency_mode, merkletree};
use crate::encoding::decode;
use crate::utils::log2;
use std::collections::{HashMap, VecDeque};

use accounts::{AccountUpdateLeaf, AccountUpdateVerifier, NewAccountVerifier};
use error::Result;
use leaves::{LeafId, LeafTransformationVerifier};

/// Size of the first 1,580,234 bytes of the block.
const TOTAL_HEADERS_SIZE: usize = BlockHeader::SIZE + PrevBlockHeaders::SIZE;

type AccountUpdatesMap = HashMap<AccountUpdateLeaf, AccountUpdateVerifier>;
type NewAccountsMap = HashMap<NewAccountVerifier, bool>;
type LeafTransformationsMap = HashMap<LeafId, LeafTransformationVerifier>;

/// Full block validation for empty blocks as described in the "Full Block
/// Validation" section of the spec.
pub fn verify_block(
    header: &BlockHeader,
    parent_header: &BlockHeader,
    mut download: Reader,
    header_cache: &mut HeaderCache,
) -> Result<()> {
    let emergency_mode = is_emergency_mode(parent_header);

    // 1. Verify that the parentid in the BlockHeader matches the id of the header we
    //    are using for the parent block.
    header.verify_parent_id(parent_header)?;

    // 2. Verify that the header meets the work requirements, which can be determined
    //    by looking at the difficultyState of the parent block.
    header.verify_work(parent_header)?;

    // 3. Verify that the timestamp in the BlockHeader is less than 3 hours into the
    //    future. If it is more than 3 hours into the future but less than 6 hours
    //    into the future, the block should be kept and re-validated once it is no
    //    longer more than 3 hours into the future. If the block is more than 6 hours
    //    into the future, it should be discarded entirely.
    header.verify_timestamp_future()?;

    // 4. Verify that the timestamp is greater than the median timestamp of the
    //    previous 11 blocks. The timestamps of the previous 11 blocks can be found in
    //    the difficultyState.
    header.verify_timestamp_median()?;

    // 5. Verify that the new difficulty state in the BlockHeader is correct.
    //    The new difficulty state can be computed by using the difficulty state
    //    of the parent block. Make sure to cover all fields, including height,
    //    decayedTotalTarget, and the timestamps.
    header.verify_difficulty_state(parent_header)?;

    // 6. Verify that the baseFeeStep was updated correctly. This can be verified by
    //    looking at the baseFeeStep in the parent header and also the numAccountUpdates
    //    in the parent header.
    parent_header.verify_base_fee_step(parent_header, emergency_mode)?;

    // 7. Download the first 1,580,234 bytes of the block. This corresponds to the
    //    10,000 headers listed in the block.
    let headers_bytes = download.read_all_headers()?;

    // 8. Verify the 10,000 headers that are listed in the block. Unfortunately, this
    //    requires downloading the 10,000 headers listed in the parent block. This
    //    strays from the ideal that a full node only needs the current block and the
    //    header of the parent, but the usefulness of having 10,000 headers in each
    //    block is enough to justify the consequences. The prevHeader field of the
    //    current block can be verified by computing it from the prevHeader of the
    //    previous block and the first element of the blockHeaders array. The next
    //    9998 headers can be verified by ensuring they match byte-for-byte the
    //    corresponding headers listed in the parent block. The final header (the newest)
    //    should match the parent header exactly.
    let parent_headers_bytes = header_cache.read(parent_header)?;
    block_header::verify_parent_headers(&headers_bytes, &parent_headers_bytes, parent_header)?;

    // 9. Discard the currently downloaded bytes, we no longer need them. We will be
    //    discarding key pieces of the block as we go to limit the total memory
    //    consumption.
    drop(headers_bytes);

    // Verification succeeded, update the header cache.
    header_cache.add_parent_header(parent_header)?;

    Ok(())
}

// Unimplemented part of `verify_block`. Splitting this out allows us to call
// the implemented part of `verify_block`.
fn verify_block_unimplemented(
    header: &BlockHeader,
    parent_header: &BlockHeader,
    mut download: Reader,
) -> Result<()> {
    // 10. Download the accountUpdates section of the block. The size of this section
    //     can be computed from the numLeaves and numAccountUpdates. This section
    //     includes the accountShardOffsets field, the activeAccounts field, and the
    //     accountUpdates field. This will always be less than 18 MiB total, and
    //     typically less than 9 MiB. Note that this implicitly checks the correctness
    //     of numLeaves and numAccountUpdates, because if those values are wrong we
    //     will not parse the block correctly.
    let account_updates_bytes = download.read(header.account_updates_size())?;

    // 11. Scan through the accountShardOffsets and ensure that the
    //     accountShardOffsets are monotonically increasing. Multiple consecutive
    //     elements with the same value is allowed.
    accounts::verify_account_shard_offsets(&account_updates_bytes)?;

    // 12. Scan through the activeAccounts and ensure that the activeAccounts are
    //     monotonically increasing. Multiple consecutive elements with the same value
    //     is allowed. At the same time, ensure that the elements in accountShardOffsets
    //     correspond to the first element in each shard.
    accounts::verify_active_accounts(&account_updates_bytes)?;

    // 13. Scan through the accountUpdates. Ensure that every set of updates which is
    //     supposedly clustered under one account actually shares the same accountID.
    //
    // 14. While scanning through the accountUpdates, create a hashmap. For each
    //     accountUpdate, create an accountUpdateLeaf object which will be used as the
    //     key. Create an accountUpdateVerifier object that contains the initialState
    //     and finalState reported by the accountUpdate, and set the Verified field to
    //     false. Check whether the accountUpdateLeaf key already exists in the
    //     hashmap. If it does, the accountUpdateVerifier inside of the hashmap should
    //     exactly match the accountUpdateVerifier we just created. If the match is not
    //     exact, the block is invalid. If the key does not already exist, insert the new
    //     accountUpdateVerifier object using the key.
    //
    // 15. Keep the hashmap in memory, but free the rest of the accountUpdates section
    //     of the block. When we process the full set of transactions, we will update
    //     the hashmap and check that every change was properly represented.
    let mut account_updates_map = accounts::verify_account_updates(account_updates_bytes)?;

    // 16. Download the newAccounts section of the block. This should not exceed more
    //     than 2056 KiB of data. This section includes the newAccountShardOffsets and
    //     newAccounts fields.
    let new_accounts_bytes = download.read(header.new_accounts_size())?;

    // 17. Verify that the shard offsets of the newAccountShardOffsets field are
    //     monotonically increasing. There should be no repeats.
    accounts::verify_new_account_shard_offsets(&new_accounts_bytes)?;

    // 18. Verify that all of the accounts in the newAccounts objects are
    //     monotonically increasing with no repeats, and that the first element in
    //     each shard of newAccounts matches the corresponding element in the
    //     newAccountShardOffsets array.
    //
    // 19. Create a hashmap for the newAccounts that maps from a newAccountVerifier
    //     object to a bool. For each new account, create the newAccountVerifier
    //     object and map it to the value 'false'. The impliedIndex of the account is
    //     the numLeaves of the parent block plus the index of the account in the
    //     newAccounts object. We will check later that these new accounts were all
    //     represented correctly.
    //
    // 20. Keep the new hashmap but toss the newAccounts section of the block.
    let mut new_accounts_map = accounts::verify_new_accounts(header, new_accounts_bytes)?;

    // 21. Begin streaming the leafTransformations field of the block. This field is
    //     the largest part of the block, potentially as large as 100 MB, so it cannot
    //     be downloaded all at once. Each element will be at most 2000 bytes, so it
    //     can be productively streamed in small segments (suggested: 8 MB at a time).

    let num_leaf_transformations = decode::<u32>(&download.read(u32::SIZE)?)?;
    // We need to remember log(n) total leaves. This gives floor(log(n)) so add 1.
    let num_prev_leaf_transformations = log2(num_leaf_transformations) + 1;
    // TODO: Change to be based on size of `LeafTransformation`.
    const SEGMENT_SIZE: usize = 8 * (1 << 20); // TODO: Should this be MB? Figure out segment size later.
    let mut prev_leaf_transformations = VecDeque::new();
    let mut merkle_tree = merkletree::Tree::default();
    let mut leaf_transformation_map = HashMap::new();
    for leaf_transformation in
        download.iterate_leaf_transformations(num_leaf_transformations, SEGMENT_SIZE)?
    {
        // 22. For each leafTransformation, there will be an initialState and a
        //     finalState. Scan the two values to determine whether or not this update is
        //     an accountUpdate. Whether or not this is an account update will depend on
        //     the type of the leaf and which fields changed. If only the NextLeaf field
        //     changed, it is not an accountUpdate. If the type is not associated with an
        //     account, it is not an accountUpdate. If the leafTransformation is an
        //     accountUpdate, derive the accountUpdateLeaf object and check the
        //     accountUpdates hashmap we created earlier. If the key is not in the
        //     hashmap, the block is invalid. If the initialState or finalState in the
        //     hashmap does not match the initialState in the leafTransformation, the block
        //     is invalid. If everything matches, set the 'Verified' bool to 'true' and
        //     update the hashmap. We will check later that every object in the hashmap
        //     was verified.
        leaf_transformation.verify_update(&mut account_updates_map)?;

        // 23. For each leafTransformation, verify that the leafIndex is strictly greater
        //     than the leafIndex of the previous leafTransformation.
        leaf_transformation.verify_leaf_index(prev_leaf_transformations.back())?;

        // 24. For each leafTransformation, verify the transformationProof. This is a
        //     Merkle proof that the initialState of the leaf exists in the Merkle root of
        //     the chainstate of the parentBlock as reported by the leafTransformation.
        //     Note that each transformationProof builds upon the previous
        //     tranformationProofs in the set of leafTransformations. Because the index of each
        //     leaf is strictly increasing, the node should only need to remember log(n)
        //     total leaves as it scans through the leafTransformations. There will be no
        //     leafTransformation objects in this array for new accounts, so we will be
        //     able to confirm that the leafTransformations have correct initialState values
        //     as soon as we process the final leafTransformation by comparing our final
        //     Merkle root to the Merkle root of the chainstate in the parent block. If it
        //     does not match, the block is invalid.
        leaf_transformation.verify_transformation_proof(&prev_leaf_transformations)?;

        // 25. For each leafTransformation, hash up the finalState of the leaf and
        //     incorporate it into a MerkleTree object that is attempting to compute the
        //     MerkleRoot of the block after all changes have been applied. We will know
        //     if the MerkleRoot matches after we have processed all of the newLeafProofs. More
        //     information can be found in the [Proof fill algorithm](#proof-fill-algorithm)
        //     section.
        leaf_transformation.proof_fill(&mut merkle_tree);

        // 26. Add each leaf to a hashmap that maps from the LeafID to the
        //     LeafTransformationVerifier of that leaf. Set the currentState of the
        //     verifier equal to the initial state of the leaf. By the end of
        //     the block verification, the two should match exactly. This hashmap will
        //     also get entries from the newLeafProofs section of the block. We don't need
        //     to worry about the index in this hashmap because we verify the index when
        //     we build the Merkle root of the block.
        leaf_transformation.add_leaf_to_map(&mut leaf_transformation_map);

        // Add the leaf transformation to the running queue of leaf transformations.
        prev_leaf_transformations.push_back(leaf_transformation);
        if prev_leaf_transformations.len() > num_prev_leaf_transformations as usize {
            prev_leaf_transformations.pop_front();
        }
    }

    // 27. Finish streaming all of the leaves and computing the Merkle root based on
    //     the initialState of the block. If the Merkle root does not match the
    //     chainstate merkle root reported in the parent header, the block is invalid.
    leaves::verify_leaf_transformations_root(parent_header, &mut merkle_tree)?;

    // 28. Begin streaming all of the newLeafProofs. If the leafTransformations were
    //     being streamed in batches, some of the newLeafProofs may have been pulled
    //     in as part of a batch. Take care to start from the beginning and use any
    //     data that got pulled in as part of the final leafTransformation batch.
    let mut non_new_account_found = false;
    for new_leaf_proof in download.iterate_new_leaf_proofs()? {
        // 29. For each newLeafProof, look up the provingLeafID in the leafTransformations
        //     hashmap. If the provingLeafID is not found, the block is invalid. Check the
        //     currentState of the verifier and make sure that the NextLeaf field points to an
        //     AccountID which is alphabetically greater than the ID of the new account.
        //     Also check that the AccountID of the new account is alphabetically greater than
        //     the provingLeafID. Update the 'currentState' of the verifier to change the
        //     NextLeaf field to point to the new leaf.
        new_leaf_proof.verify_in_leaf_transformations_map(&leaf_transformation_map)?;

        // 30. Create a LeafTransformationVerifier for the new account. Have the
        //     'currentState' be blank except for the NextLeaf value, which should be set
        //     to be equal to the NextLeaf value that was originally provided by the proving
        //     leaf. Set the 'finalState' equal to the final state established by the
        //     newLeafProof.
        new_leaf_proof.create_leaf_transformation_verifier(&mut leaf_transformation_map)?;

        // 31. Incorporate the final state of the leaf into the Merkle root that we are
        //     building to verify the chainstate merkle root declared by the block header.
        //     The new leaves are provided in order of their index, with each new leaf
        //     having an index that is exactly 1 greater than the previous index.
        new_leaf_proof.incorporate_leaf_into_merkle_root(&mut merkle_tree);

        // 32. Check whether the new leaf is an account update. If it is, derive the
        //     accountUpdateLeaf object and check the accountUpdates hashmap we created
        //     earlier. If the key is not in the hashmap, the block is invalid. If the
        //     initialState or finalState in the hashmap does not match the initialState in
        //     the leafTransformation, the block is invalid. If everything matches, set the
        //     'Verified' bool to 'true' and update the hashmap. We will check later that
        //     every object in the hashmap was verified.
        new_leaf_proof
            .verify_account_update(&mut account_updates_map, &mut non_new_account_found)?;

        // 33. Check the leafFinalState of the newLeafProof to determine whether this leaf
        //     is a new account. If this leaf is a new account, create a
        //     newAccountVerifier object using the AccountID which is declared in the
        //     leafFinalState and the implied index of the new leaf. Check that this leaf
        //     exists as a key in the new accounts hashmap. If it does, set the value to
        //     'true'. If it does not, the block is invalid. Check that all of the new
        //     accounts are clustered at the very front of the new leaf proofs. Once there
        //     is a newLeafProof which is not a new account, more new accounts are not
        //     allowed.
        new_leaf_proof.verify_new_account(&mut new_accounts_map, non_new_account_found)?;
    }

    // 34. Finish scanning the newLeafProofs. By the time this is done, we will have
    //     completed the Merkle root of the new block. Verify that the Merkle root of
    //     the new block matches the merkle root established in the header. If it does not
    //     match, the block is invalid.
    leaves::verify_final_merkle_root(header, merkle_tree)?;

    // 35. Scan through the accountUpdates hashmap. At this point, every
    //     accountUpdateVerifier should have the 'Verified' field set to 'true'. If
    //     any accountUpdateVerifier does not have the 'Verified' field set to 'true', the
    //     block is invalid. Discard the accountUpdates hashmap.
    accounts::verify_account_updates_map(account_updates_map)?;

    // 36. Scan through the new accounts hashmap. Every key in the hashmap should map
    //     to the value 'true'. If any key does not map to the value 'true', the block
    //     is invalid. Discord the new accounts hashmap.
    accounts::verify_new_accounts_map(new_accounts_map)?;

    // 37. Start streaming the transactions. For each transaction, execute the update
    //     and modify the corresponding 'currentState' values in the
    //     leafTransformations hashmap. If any leaves are updated in the transactions
    //     which aren't represented in the leafTransformation hashmap, fail the block.
    for transaction in download.iterate_transactions()? {
        // 38. For each transaction, determine which leafs are impacted and what updates
        //     need to be made to the 'currentState' of those leaves in the leaf
        //     transformations hashmap. Make the updates accordingly. There is no need to
        //     verify Merkle proofs or check any chainstate indexes, we have already
        //     confirmed all of these things. All we need to do is update the 'currentState'
        //     fields of each affected leaf in the leaf transformations hashamp.
        transaction.execute_update(&mut leaf_transformation_map)?;
    }

    // 39. Once all transactions have been processed, scan through the leaf
    //     transformations hashmap. Every single value in the hashmap should have an
    //     indentical 'currentState' and 'finalState'. If any value has any mismatch,
    //     the block is invalid.
    leaves::verify_leaf_transformations_map(leaf_transformation_map)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::build_var;
    use crate::consensus::block::verify::TOTAL_HEADERS_SIZE;

    // NOTE: Keep the value in the spec in sync with the value here.
    #[test]
    fn test_total_headers_size() {
        build_var! {
            const EXPECTED: usize = {
                standard = 1_580_234,
                testing = 1814,
            };
        }
        assert_eq!(TOTAL_HEADERS_SIZE, EXPECTED);
    }
}
