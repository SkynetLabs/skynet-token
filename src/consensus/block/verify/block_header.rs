use super::super::{BlockHeader, LiteHeader, NUM_PREV_BLOCK_HEADERS};
use super::{
    error::{Error, Result},
    TOTAL_HEADERS_SIZE,
};
use crate::consensus::{
    difficulty::{decay_total_target, decay_total_time},
    fee,
    lowpow::low_pow,
    time::{current_timestamp, SECS_IN_HOUR},
};
use crate::encoding::{encode, Encode};

impl BlockHeader {
    /// The size of the account updates section of the block, computed from
    /// num_leaves and num_account_updates.
    pub fn account_updates_size(&self) -> usize {
        unimplemented!();
    }

    /// The size of the new accounts section of the block. This should not
    /// exceed more than 2056 KiB.
    pub fn new_accounts_size(&self) -> usize {
        unimplemented!();
    }

    /// Verify that the parentid in the BlockHeader matches the id of the header
    /// we are using for the parent block. Step 1 of [`verify_block`].
    pub fn verify_parent_id(&self, parent_header: &BlockHeader) -> Result<()> {
        let expected_id = parent_header.compute_id();
        if self.lite_header.parent_id != expected_id {
            return Err(Error::ParentIdDidNotMatch {
                parent_id: self.lite_header.parent_id.clone(),
                expected_id,
            });
        }
        Ok(())
    }

    /// Verify that the header meets the work requirements, which can be
    /// determined by looking at the difficultyState of the parent block. Step 2
    /// of [`verify_block`].
    ///
    /// Use `child_target` in difficulty.rs to determine the expected target for
    /// the new block and then verify that solving that block with the provided
    /// nonce beats that target.
    pub fn verify_work(&self, parent_header: &BlockHeader) -> Result<()> {
        // Get the expected target for the new block.
        //
        // We are actually looking at 3 blocks here. The current block and the
        // parent of the current block together decide the target of the child.
        // The child is the block that we are trying to verify here.
        let expected_target = parent_header.child_target;

        // Verify that solving the new block with the provided nonce beats the
        // expected target.
        //
        // Encode header (nonce, pow_mask and a hash of other fields) and input
        // to `low_pow`.
        let encoded_header = self.encode_for_pow();
        let mut hash = low_pow(&encoded_header.0);
        self.apply_pow_mask(&mut hash);
        if hash.0 <= expected_target.0 {
            return Ok(());
        }

        Err(Error::PowDidNotBeatTarget {
            target: expected_target,
        })
    }

    /// Verify that the timestamp in the BlockHeader is less than 3 hours into
    /// the future. If it is more than 3 hours into the future but less than 6
    /// hours into the future, the block should be kept and re-validated once it
    /// is no longer more than 3 hours into the future. If the block is more
    /// than 6 hours into the future, it should be discarded entirely. Step 3 of
    /// [`verify_block`].
    pub fn verify_timestamp_future(&self) -> Result<()> {
        let block_timestamp = self.lite_header.timestamp;
        let current_timestamp = current_timestamp();

        if block_timestamp > current_timestamp + 6 * SECS_IN_HOUR {
            Err(Error::TimestampMoreThanSixHoursAway(block_timestamp))
        } else if block_timestamp > current_timestamp + 3 * SECS_IN_HOUR {
            Err(Error::TimestampMoreThanThreeHoursAway(block_timestamp))
        } else {
            Ok(())
        }
    }

    /// Verify that the timestamp is greater than the median timestamp of the
    /// previous 11 blocks. The timestamps of the previous 11 blocks can be
    /// found in the difficultyState. Step 4 of [`verify_block`].
    pub fn verify_timestamp_median(&self) -> Result<()> {
        let block_timestamp = self.lite_header.timestamp;
        let median_timestamp = self.prev_timestamps.median();

        if block_timestamp <= median_timestamp {
            return Err(Error::TimestampNotGreaterThanMedian {
                block_timestamp,
                median_timestamp,
            });
        }
        Ok(())
    }

    /// Verify that the new difficulty state in the BlockHeader is correct. The
    /// new difficulty state can be computed by using the difficulty state of
    /// the parent block. Make sure to cover all fields, including height,
    /// decayedTotalTarget, and the timestamps. Step 5 of [`verify_block`].
    pub fn verify_difficulty_state(&self, parent_header: &BlockHeader) -> Result<()> {
        // Verify height.
        let parent_height = parent_header.height;
        if parent_height == u64::MAX {
            return Err(Error::MaximumParentHeight { parent_height });
        }
        if self.height != parent_height + 1 {
            return Err(Error::HeightDidNotMatch {
                height: self.height,
                parent_height,
            });
        }

        // The decayed total target of a block should match the parent decayed
        // total target of the next one.
        if self.parent_decayed_total_target != parent_header.decayed_total_target {
            return Err(Error::ParentDecayedTotalTargetDidNotMatch {
                parent_target: self.parent_decayed_total_target,
                expected_target: parent_header.decayed_total_target,
            });
        }
        if self.target != parent_header.child_target {
            return Err(Error::ParentChildTargetDidNotMatch {
                parent_child_target: parent_header.child_target,
                expected_target: self.target,
            });
        }

        // Verify previous timestamps.
        //
        // The first ten timestamps should equal the last ten timestamps of the
        // parent. The last timestamp should equal the parent timestamp.
        if self.prev_timestamps.0[0..10] != parent_header.prev_timestamps.0[1..11] {
            return Err(Error::PrevTimestampsDidNotMatch);
        }
        if self.prev_timestamps.parent() != parent_header.lite_header.timestamp {
            return Err(Error::PrevTimestampsDidNotMatch);
        }

        // Verify decayed total target.
        let new_decayed_total_target =
            decay_total_target(parent_header.decayed_total_target, parent_header.target);
        if self.decayed_total_target != new_decayed_total_target {
            return Err(Error::NewDecayedTotalTargetDidNotMatch {
                new_target: new_decayed_total_target,
                expected_target: self.decayed_total_target,
            });
        }

        // Verify decayed times.
        let new_parent_decayed_total_time = decay_total_time(
            parent_header.parent_decayed_total_time,
            self.prev_timestamps.parent(),
            self.prev_timestamps.grandparent(),
        )?;
        if new_parent_decayed_total_time != self.parent_decayed_total_time {
            return Err(Error::NewDecayedTotalTimeDidNotMatch {
                new_time: new_parent_decayed_total_time,
                expected_time: self.parent_decayed_total_time,
            });
        }

        Ok(())
    }

    /// Verify that the baseFeeStep was updated correctly. This can be verified
    /// by looking at the baseFeeStep in the parent header and also the
    /// numAccountUpdates in the parent header. Step 6 of [`verify_block`].
    pub fn verify_base_fee_step(
        &self,
        parent_header: &BlockHeader,
        emergency_mode: bool,
    ) -> Result<()> {
        let base_fee_step = fee::base_fee_step_for_child(
            parent_header.base_fee_step,
            parent_header.lite_header.num_account_updates as u64,
            emergency_mode,
        );

        if base_fee_step != self.base_fee_step {
            return Err(Error::BaseFeeStepDidNotMatch {
                step: self.base_fee_step,
                expected_step: base_fee_step,
            });
        }
        Ok(())
    }
}

/// Verify the 10,000 headers that are listed in the block. Unfortunately, this
/// requires downloading the 10,000 headers listed in the parent block. This
/// strays from the ideal that a full node only needs the current block and the
/// header of the parent, but the usefulness of having 10,000 headers in each
/// block is enough to justify the consequences. The prevHeader field of the
/// current block can be verified by computing it from the prevHeader of the
/// previous block and the first element of the blockHeaders array. The next
/// 9998 headers can be verified by ensuring they match byte-for-byte the
/// corresponding headers listed in the parent block. The final header (the newest)
/// should match the parent header exactly. Step 8 of [`verify_block`].
///
/// # Visualisation
///
/// We compare the block histories like this:
///
/// current block headers:     o - o - ... - o - o - O - H
///                            |   |    |    |   |   |
/// parent blockheaders:   o - o - o - ... - o - O - P
///
/// where:
/// - |: comparison
/// - o: lite header
/// - O: block header
/// - H: current header
/// - P: parent header
pub fn verify_parent_headers(
    headers_bytes: &[u8],
    parent_headers_bytes: &[u8],
    parent_header: &BlockHeader,
) -> Result<()> {
    let block_header_index = TOTAL_HEADERS_SIZE - BlockHeader::SIZE;
    let block_header_end_index = block_header_index + LiteHeader::SIZE;

    // Compare the lite headers that both histories have in common. This
    // includes comparing the parent header's parent block header to the
    // corresponding lite header in the current block's history.
    if headers_bytes[..block_header_index]
        != parent_headers_bytes[LiteHeader::SIZE..block_header_end_index]
    {
        // We do a bulk comparison above for better performance. Now let's find
        // the specific index of the mismatch for the error message.
        for (i, (history_i, parent_history_i)) in (0..block_header_index)
            .step_by(LiteHeader::SIZE)
            .zip((LiteHeader::SIZE..block_header_end_index).step_by(LiteHeader::SIZE))
            .enumerate()
        {
            if headers_bytes[history_i..history_i + LiteHeader::SIZE]
                != parent_headers_bytes[parent_history_i..parent_history_i + LiteHeader::SIZE]
            {
                // The height of the earliest block in `headers_bytes`.
                let start_height = parent_header.height() as i128 - NUM_PREV_BLOCK_HEADERS as i128;
                // The height of the mismatched block. If it ends up being below
                // 1 we set it to `None` later.
                let height = start_height + i as i128;
                return Err(Error::ParentBlockHistoryDidNotMatch {
                    index: history_i,
                    parent_index: parent_history_i,
                    height: if height >= 1 {
                        Some(height as u64)
                    } else {
                        None
                    },
                });
            }
        }
    }

    // Compare the current block's parent header bytes to the parent header
    // directly.
    if headers_bytes[block_header_index..] != encode(parent_header) {
        return Err(Error::ParentBlockDidNotMatchHistory {
            height: parent_header.height(),
        });
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::consensus::{
        block::{
            test_consts::*,
            verify, {BlockId, PrevTimestamps},
        },
        test_miner::TEST_MINER,
    };
    use crate::test_utils::Random;
    use anyhow;
    use rand::Rng;

    // Test for step 1.
    #[test]
    fn test_verify_parent_id() {
        // Test with hard-coded values.
        let mut header = BlockHeader::default();
        let parent_id = BlockId::default();
        header.lite_header.parent_id = parent_id.clone();
        let parent_header = TEST_BLOCK_HEADER;
        let parent_header_id = parent_header.compute_id();

        let result = header.verify_parent_id(&parent_header);
        assert!(matches!(
            result,
            Err(Error::ParentIdDidNotMatch {
                parent_id: a,
                expected_id: b
            }) if a == parent_id && b == parent_header_id
        ));

        // Test with random values.
        let parent_id = BlockId::random();
        let mut header = BlockHeader::random();
        header.lite_header.parent_id = parent_id.clone();
        let parent_header = BlockHeader::random();
        let parent_header_id = parent_header.compute_id();

        let result = header.verify_parent_id(&parent_header);
        assert!(matches!(
            result,
            Err(Error::ParentIdDidNotMatch {
                parent_id: a,
                expected_id: b
            }) if a == parent_id && b == parent_header_id
        ));

        // Success case.
        header.lite_header.parent_id = parent_header.compute_id();
        let result = header.verify_parent_id(&parent_header);
        assert!(matches!(result, Ok(())));
    }

    // Test for step 2.
    #[test]
    fn test_verify_work() -> anyhow::Result<()> {
        // Test with a known unsolved block.
        let block_header = TEST_BLOCK_HEADER;
        let parent_header = BlockHeader::default();
        let result = block_header.verify_work(&parent_header);
        assert!(matches!(
            result,
            Err(Error::PowDidNotBeatTarget { target: a }) if a == parent_header.child_target
        ));

        // Success case. Test with a solved block.
        let block_header = BlockHeader::random();
        let parent_header = BlockHeader::random();
        let solved_block = block_header.solve(parent_header.child_target)?;
        let result = solved_block.header.verify_work(&parent_header);
        assert!(matches!(result, Ok(())));

        Ok(())
    }

    // Test for step 3.
    #[test]
    fn test_verify_timestamp_future() {
        let mut header = BlockHeader::random();

        // Test for TimestampMoreThanThreeHoursAway error.
        //
        // Provide a couple seconds leeway since `verify_timestamp_future()`
        // will call `current_timestamp()` again.
        let (lower_bound, upper_bound) = (3 * SECS_IN_HOUR + 3, 6 * SECS_IN_HOUR);
        let timestamp =
            current_timestamp() + rand::thread_rng().gen_range(lower_bound..upper_bound);
        header.lite_header.timestamp = timestamp;
        let result = header.verify_timestamp_future();
        assert!(matches!(
            result,
            Err(Error::TimestampMoreThanThreeHoursAway(t)) if t == timestamp
        ));

        // Test for TimestampMoreThanSixHoursAway error.
        //
        // Provide a couple seconds leeway since `verify_timestamp_future()`
        // will call `current_timestamp()` again.
        let (lower_bound, upper_bound) = (6 * SECS_IN_HOUR + 3, 9 * SECS_IN_HOUR);
        let timestamp =
            current_timestamp() + rand::thread_rng().gen_range(lower_bound..upper_bound);
        header.lite_header.timestamp = timestamp;
        let result = header.verify_timestamp_future();
        assert!(matches!(
            result,
            Err(Error::TimestampMoreThanSixHoursAway(t)) if t == timestamp
        ));

        // Success case.
        let (lower_bound, upper_bound) = (0, 3 * SECS_IN_HOUR - 1);
        let timestamp =
            current_timestamp() + rand::thread_rng().gen_range(lower_bound..upper_bound);
        header.lite_header.timestamp = timestamp;
        let result = header.verify_timestamp_future();
        assert!(matches!(result, Ok(())));
    }

    // Test for step 4.
    #[test]
    fn test_verify_timestamp_median() {
        // Verify the expected behavior for random timestamps.
        let mut header = BlockHeader::random();
        let median_timestamp = header.prev_timestamps.median();

        let result = header.verify_timestamp_median();
        if header.lite_header.timestamp <= median_timestamp {
            assert!(matches!(
              result,
              Err(Error::TimestampNotGreaterThanMedian{
                  block_timestamp: a,
                  median_timestamp: b
              }) if a == header.lite_header.timestamp && b == median_timestamp
            ));
        } else {
            assert!(matches!(result, Ok(())));
        }

        // Verify that when the timestamp equals the median timestamp we get an
        // error.
        header.lite_header.timestamp = median_timestamp;
        let result = header.verify_timestamp_median();
        assert!(matches!(
          result,
          Err(Error::TimestampNotGreaterThanMedian{
              block_timestamp: a,
              median_timestamp: b
          }) if a == header.lite_header.timestamp && b == median_timestamp
        ));

        // Verify that when the timestamp is one less than the median timestamp
        // we get an error.
        header.lite_header.timestamp = median_timestamp - 1;
        let result = header.verify_timestamp_median();
        assert!(matches!(
          result,
          Err(Error::TimestampNotGreaterThanMedian{
              block_timestamp: a,
              median_timestamp: b
          }) if a == header.lite_header.timestamp && b == median_timestamp
        ));

        // Verify that when the timestamp is one more than the median timestamp
        // we don't get an error.
        header.lite_header.timestamp = median_timestamp + 1;
        let result = header.verify_timestamp_median();
        assert!(matches!(result, Ok(())));
    }

    // Test for step 5.
    #[test]
    fn test_verify_difficulty_state() -> anyhow::Result<()> {
        let mut header = BlockHeader::random();
        let mut parent_header = BlockHeader::random();

        // Make sure the decayed total time is valid.
        while decay_total_time(
            parent_header.parent_decayed_total_time,
            parent_header.lite_header.timestamp,
            parent_header.prev_timestamps.parent(),
        )
        .is_err()
        {
            parent_header = BlockHeader::random();
        }

        // Test the height overflow case.
        parent_header.height = u64::MAX;
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(
            result,
            Err(Error::MaximumParentHeight {
                parent_height: a,
            }) if a == parent_header.height
        ));

        // Get the heights ready for the HeightDidNotMatch check.
        //
        // 1. Make sure the heights are not the max value.
        //
        // 2. Make sure the heights don't match to start with so we can test the
        //    error case. The rest of the checks are highly unlikely to pass.
        loop {
            header.height = rand::thread_rng().gen_range(0..u64::MAX - 1);
            parent_header.height = rand::thread_rng().gen_range(0..u64::MAX - 1);

            if header.height != parent_header.height + 1 {
                break;
            }
        }

        // Test the height check.
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(
            result,
            Err(Error::HeightDidNotMatch {
                height: a,
                parent_height: b,
            }) if a == header.height && b == parent_header.height
        ));
        header.height = parent_header.height + 1;

        // Test the target checks.
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(
            result,
            Err(Error::ParentDecayedTotalTargetDidNotMatch {
                parent_target: a,
                expected_target: b,
            }) if a == header.parent_decayed_total_target && b == parent_header.decayed_total_target
        ));
        header.parent_decayed_total_target = parent_header.decayed_total_target;
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(
            result,
            Err(Error::ParentChildTargetDidNotMatch {
                parent_child_target: a,
                expected_target: b,
            }) if a == parent_header.child_target && b == header.target
        ));
        header.target = parent_header.child_target;

        // Test the previous timestamps check.
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(result, Err(Error::PrevTimestampsDidNotMatch)));
        let mut prev_timestamps = PrevTimestamps::default();
        prev_timestamps.0[0..10].copy_from_slice(&parent_header.prev_timestamps.0[1..11]);
        prev_timestamps.0[10] = parent_header.lite_header.timestamp;
        header.prev_timestamps = prev_timestamps;

        // Test the decayed total target calculation check.
        let new_decayed_total_target =
            decay_total_target(parent_header.decayed_total_target, parent_header.target);
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(
            result,
            Err(Error::NewDecayedTotalTargetDidNotMatch {
                new_target: a,
                expected_target: b,
            }) if a == new_decayed_total_target && b == header.decayed_total_target
        ));
        header.decayed_total_target = new_decayed_total_target;

        // Test the decayed total time calculation check.
        let new_decayed_total_time = decay_total_time(
            parent_header.parent_decayed_total_time,
            header.prev_timestamps.parent(),
            header.prev_timestamps.grandparent(),
        )?;
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(
            result,
            Err(Error::NewDecayedTotalTimeDidNotMatch {
                new_time: a,
                expected_time: b,
            }) if a == new_decayed_total_time && b == header.parent_decayed_total_time
        ));
        header.parent_decayed_total_time = new_decayed_total_time;

        // Test the success case.
        let result = header.verify_difficulty_state(&parent_header);
        assert!(matches!(result, Ok(())));

        Ok(())
    }

    // Test for step 6.
    #[test]
    fn test_verify_base_fee_step() {
        let test = |emergency_mode: bool| {
            // Test with hard-coded values.
            let header = BlockHeader::default();
            let parent_header = TEST_BLOCK_HEADER;
            let result = header.verify_base_fee_step(&parent_header, emergency_mode);
            assert!(matches!(
                result,
                Err(Error::BaseFeeStepDidNotMatch {
                    step: 0,
                    expected_step: 1003
                })
            ));

            // Test with random values.
            let mut header = BlockHeader::random();
            let parent_header = BlockHeader::random();
            let expected_base_fee_step = fee::base_fee_step_for_child(
                parent_header.base_fee_step,
                parent_header.lite_header.num_account_updates as u64,
                emergency_mode,
            );
            let result = header.verify_base_fee_step(&parent_header, emergency_mode);
            assert!(matches!(
                result,
                Err(Error::BaseFeeStepDidNotMatch {
                    step: a,
                    expected_step: b
                }) if a == header.base_fee_step && b == expected_base_fee_step
            ));

            // Test success case.
            header.base_fee_step = expected_base_fee_step;
            let result = header.verify_base_fee_step(&parent_header, emergency_mode);
            assert!(matches!(result, Ok(())));
        };

        // Run tests once without and once with emergency mode enabled.
        test(true);
        test(false);
    }

    // Test for step 7.
    #[test]
    fn test_download_read_all_headers() -> anyhow::Result<()> {
        // Make sure we have at least 11 blocks.
        TEST_MINER.write().unwrap().mine_up_until(11);

        // Create a reader from the 11th block.
        let mut download =
            verify::Reader::from_block(TEST_MINER.read().unwrap().blocks[10].header.block());

        // Read the block's previous block history.
        let headers_bytes = download.read_all_headers()?;

        // Compare the downloaded bytes to the expected blocks.
        for (i, header_index) in (0..TOTAL_HEADERS_SIZE - BlockHeader::SIZE)
            .step_by(LiteHeader::SIZE)
            .enumerate()
        {
            assert_eq!(
                headers_bytes[header_index..header_index + LiteHeader::SIZE],
                encode(&TEST_MINER.read().unwrap().blocks[i].header.lite_header)
            );
        }
        assert_eq!(
            headers_bytes[TOTAL_HEADERS_SIZE - BlockHeader::SIZE..],
            encode(&TEST_MINER.read().unwrap().blocks[9].header)
        );

        Ok(())
    }

    // Test for steps 8-9.
    #[test]
    fn test_download_and_verify_parent_headers() -> anyhow::Result<()> {
        const PARENT_HEADER_INDEX: usize = TOTAL_HEADERS_SIZE - BlockHeader::SIZE;
        const PARENT_LITE_HEADER_INDEX: usize = PARENT_HEADER_INDEX - LiteHeader::SIZE;

        // Make sure we have at least 11 blocks.
        TEST_MINER.write().unwrap().mine_up_until(11);

        let genesis_block = &TEST_MINER.read().unwrap().blocks[0];
        let mut header_cache = verify::HeaderCache::initialize(&genesis_block.header)?;

        // Test blocks with heights 2 through 11.
        for i in 1..11 {
            let test_miner = TEST_MINER.read().unwrap();
            let block = &test_miner.blocks[i];
            let parent_header = &test_miner.blocks[i - 1].header;

            // Create a reader from the block.
            let mut download = verify::Reader::from_block(block.header.block());

            // Read the block's previous block history.
            let mut headers_bytes = download.read_all_headers()?;

            // Download and verify parent headers.
            let parent_headers_bytes = header_cache.read(parent_header)?;
            let result =
                verify_parent_headers(&headers_bytes, &parent_headers_bytes, parent_header);
            assert!(matches!(result, Ok(())));

            // Test lite header mismatch.
            headers_bytes[0] += 1;
            let result =
                verify_parent_headers(&headers_bytes, &parent_headers_bytes, parent_header);
            let height = if i >= 10 { Some(i as u64 - 9) } else { None };
            assert!(matches!(result, Err(
                Error::ParentBlockHistoryDidNotMatch {
                    index: a,
                    parent_index: b,
                    height: c
                }) if a == 0 && b == LiteHeader::SIZE && c == height
            ));
            headers_bytes[0] -= 1;

            // Test parent header's parent header mismatch.
            headers_bytes[PARENT_LITE_HEADER_INDEX] += 1;
            let result =
                verify_parent_headers(&headers_bytes, &parent_headers_bytes, parent_header);
            let height = if i >= 2 { Some(i as u64 - 1) } else { None };
            assert!(matches!(result, Err(
                Error::ParentBlockHistoryDidNotMatch {
                    index: a,
                    parent_index: b,
                    height: c
                }) if a == PARENT_LITE_HEADER_INDEX &&
                             b == PARENT_HEADER_INDEX &&
                             c == height
            ));
            headers_bytes[PARENT_LITE_HEADER_INDEX] -= 1;

            // Test parent header mismatch.
            headers_bytes[PARENT_HEADER_INDEX] += 1;
            let result =
                verify_parent_headers(&headers_bytes, &parent_headers_bytes, parent_header);
            assert!(matches!(result, Err(
                Error::ParentBlockDidNotMatchHistory { height: a }) if a == i as u64
            ));

            // Verification succeeded. Add the parent header to the cache.
            header_cache.add_parent_header(parent_header)?;
        }

        Ok(())
    }
}
