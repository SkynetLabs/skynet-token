//! Parent headers history cache.
//!
//! We don't always want to download all of the parent block's 10,000 headers.
//! We only need to do that once and then we can reuse them for verifying the
//! next block. Only a reboot should force us to redownload some of them.
//!
//! Once we have the header history, we can just add the header of the parent,
//! and drop the oldest one. That way we don't need to download 10,000 headers
//! every time we verify a block.
//!
//! When we start verifying from the genesis block, we theoretically don't ever
//! need to do that download. Because we know that at first there are 0 previous
//! block headers and then we just remember the block headers as we go.
//!
//! NOTE: The current implementation is not thread safe and it expects
//! sequential calls to `add_parent_header`.
//!
//! TODO: Test for more cases (e.g. non-sequential headers and cache not updated
//! on `verify_block` failure). Didn't want to spend too much time on this
//! because it will probably change to account for the NOTE above.

use super::super::{BlockHeader, BlockHeight, LiteHeader};
use super::{
    error::{Error, Result},
    Reader, TOTAL_HEADERS_SIZE,
};
use crate::encoding::{encode, Encode};
use std::collections::VecDeque;

/// Cache for the downloaded parent header history.
pub struct HeaderCache {
    cache: VecDeque<u8>,
    latest_height: BlockHeight,
}

impl HeaderCache {
    /// Creates a new `HeaderCache`. Will initialize to all 0's if we're
    /// starting from the genesis block, otherwise it will download the parent
    /// headers.
    pub fn initialize(parent_header: &BlockHeader) -> Result<Self> {
        // When we start verifying from the genesis block, we theoretically
        // don't ever need to do that download. Because we know that at first
        // there are 0 previous block headers and then we just remember the
        // block headers as we go.
        assert!(parent_header.height != 0);
        let bytes = if parent_header.height == 1 {
            vec![0; TOTAL_HEADERS_SIZE]
        } else {
            // Not genesis block, we have to download the headers.
            let mut download = Reader::from_block(parent_header.block());
            download.read_all_headers()?
        };

        // Initialize the cache and add the parent header.
        Ok(Self {
            cache: bytes.into(),
            latest_height: parent_header.height - 1,
        })
    }

    /// Reads from the cache.
    pub fn read(&mut self, parent_header: &BlockHeader) -> Result<Vec<u8>> {
        self.validate_parent_header_height(parent_header)?;
        Ok(self.cache.make_contiguous().to_vec())
    }

    /// Once we have the header history, we can just add the header of the
    /// parent, and drop the oldest one. That way we don't need to download
    /// 10,000 headers every time we verify a block.
    pub fn add_parent_header(&mut self, parent_header: &BlockHeader) -> Result<()> {
        self.validate_parent_header_height(parent_header)?;

        // Trim the latest full block header, making it into a lite header.
        let block_header_index = TOTAL_HEADERS_SIZE - BlockHeader::SIZE + LiteHeader::SIZE;
        self.cache.drain(block_header_index..);

        // Trim the oldest lite header.
        self.cache.drain(..LiteHeader::SIZE);

        // Add the parent header to the cache as a full block header.
        let mut encoded_parent_header: VecDeque<u8> = encode(parent_header).into();
        self.cache.append(&mut encoded_parent_header);

        assert!(self.cache.len() == TOTAL_HEADERS_SIZE);

        self.latest_height = parent_header.height;

        Ok(())
    }

    fn validate_parent_header_height(&self, parent_header: &BlockHeader) -> Result<()> {
        if parent_header.height == 0 || parent_header.height - 1 != self.latest_height {
            return Err(Error::ParentHeaderHeightDidNotMatchHeaderCache {
                parent_header_height: parent_header.height,
                cache_height: self.latest_height,
            });
        }
        Ok(())
    }
}

// Unit tests for header_cache.
//
// Cases we test:
//
// 1. Initializing the cache when the parent header is the genesis block.
//
// 2. Initializing the cache when the parent header is not the genesis block, so
// we have to download the header history.
//
//   2a. Partial history
//   2b. Full history
//
// 3. Adding a header to the cache.
#[cfg(test)]
mod tests {
    use super::*;
    use crate::consensus::{block::verify, test_miner::TEST_MINER};
    use anyhow;

    fn get_block_headers(block_index: usize) -> anyhow::Result<Vec<u8>> {
        let block = &TEST_MINER.read().unwrap().blocks[block_index];
        let mut download = verify::Reader::from_block(block.header.block());
        Ok(download.read_all_headers()?)
    }

    // Test case (1): The parent header is the genesis block.
    #[test]
    fn test_header_cache_genesis_block() -> anyhow::Result<()> {
        // Make sure we have at least 2 blocks.
        TEST_MINER.write().unwrap().mine_up_until(2);

        // Initialize cache for the genesis block (block 1).
        let genesis_block = &TEST_MINER.read().unwrap().blocks[0];
        let mut header_cache = HeaderCache::initialize(&genesis_block.header)?;

        // Cache should contain all 0's.
        let cache_bytes = header_cache.read(&genesis_block.header)?;
        let expected_bytes = vec![0; TOTAL_HEADERS_SIZE];
        assert_eq!(cache_bytes, expected_bytes);

        // Test case (3): Test adding a parent header to the cache.
        header_cache.add_parent_header(&genesis_block.header)?;

        // Cache should start with all 0's and also include the genesis block
        // bytes.
        let block = &TEST_MINER.read().unwrap().blocks[1];
        let cache_bytes = header_cache.read(&block.header)?;
        let mut expected_bytes = vec![0; TOTAL_HEADERS_SIZE - BlockHeader::SIZE];
        expected_bytes.append(&mut encode(&genesis_block.header));
        assert_eq!(cache_bytes, expected_bytes);

        Ok(())
    }

    // Test case (2): The parent header is not the genesis block and we don't have the cache,
    // so we have to download the header history.
    #[test]
    fn test_header_cache_download_history() -> anyhow::Result<()> {
        // Make sure we have at least 12 blocks.
        TEST_MINER.write().unwrap().mine_up_until(12);

        // Test cases (2a), (2b): Test cache with both partial and full header
        // histories.
        for i in [5, 11] {
            // Initialize cache with block with height i.
            let block = &TEST_MINER.read().unwrap().blocks[i - 1];
            let mut header_cache = HeaderCache::initialize(&block.header)?;

            // Cache should contain the parent headers for the block.
            let cache_bytes = header_cache.read(&block.header)?;
            let expected_bytes = get_block_headers(i - 1)?;
            assert_eq!(cache_bytes, expected_bytes);

            // Test case (3): Test adding a parent header to the cache.
            header_cache.add_parent_header(&block.header)?;

            // Verify the cached headers for this block.
            let block = &TEST_MINER.read().unwrap().blocks[i];
            let cache_bytes = header_cache.read(&block.header)?;
            let expected_bytes = get_block_headers(i)?;
            assert_eq!(cache_bytes, expected_bytes);
        }

        Ok(())
    }
}
