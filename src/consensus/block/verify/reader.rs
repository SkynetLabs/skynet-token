use super::{
    error::Result,
    leaves::{LeafTransformation, NewLeafProof},
    transactions::Transaction,
    TOTAL_HEADERS_SIZE,
};
use crate::consensus::block::Skylink;
use std::io;

/// Reader for block downloads.
pub struct Reader();

impl Reader {
    /// Creates a `Reader` for the given block.
    pub fn from_block(_block: &Skylink) -> Self {
        unimplemented!();
    }

    /// Reads a given amount of bytes from the download stream.
    pub fn read(&mut self, _size: usize) -> io::Result<Vec<u8>> {
        unimplemented!();
    }

    /// Reads all the headers.
    pub fn read_all_headers(&mut self) -> io::Result<Vec<u8>> {
        self.read(TOTAL_HEADERS_SIZE)
    }

    // TODO: Return an iterator.
    /// Begin streaming the leafTransformations field of the block. This field
    /// is the largest part of the block, potentially as large as 100 MB, so it
    /// cannot be downloaded all at once. Each element will be at most 2000
    /// bytes, so it can be productively streamed in small segments (suggested:
    /// 8 MB at a time). Step 21 of [`verify_block`].
    pub fn iterate_leaf_transformations(
        &self,
        _num_leaf_transformations: u32,
        _segment_size: usize,
    ) -> Result<Vec<LeafTransformation>> {
        unimplemented!();
    }

    /// Begin streaming all of the newLeafProofs. If the leafTransformations
    /// were being streamed in batches, some of the newLeafProofs may have been
    /// pulled in as part of a batch. Take care to start from the beginning and
    /// use any data that got pulled in as part of the final leafTransformation
    /// batch. Step 28 of [`verify_block`].
    pub fn iterate_new_leaf_proofs(&self) -> Result<Vec<NewLeafProof>> {
        unimplemented!();
    }

    /// Start streaming the transactions. For each transaction, execute the
    /// update and modify the corresponding 'currentState' values in the
    /// leafTransformations hashmap. If any leaves are updated in the
    /// transactions which aren't represented in the leafTransformation hashmap,
    /// fail the block. Step 37 of [`verify_block`].
    pub fn iterate_transactions(&self) -> Result<Vec<Transaction>> {
        unimplemented!();
    }
}
