use super::{
    error::Result,
    leaves::{LeafTransformation, NewLeafProof},
    transactions::Transaction,
    TOTAL_HEADERS_SIZE,
};
use crate::consensus::{
    block::{BlockHeader, PrevBlockHeaders, Skylink, NUM_PREV_BLOCK_HEADERS},
    test_miner::TEST_MINER,
};
use crate::encoding::{encode, Encode};
use std::io;

pub struct Reader {
    bytes: Vec<u8>,
    index: usize,
}

impl Reader {
    /// Creates a `Reader` with the expected block data for the given block.
    pub fn from_block(block: &Skylink) -> Self {
        let mut prev_block_headers = PrevBlockHeaders::default();

        // Find the block in the mined blocks, searching by skylink.
        let test_miner_blocks = &TEST_MINER.read().unwrap().blocks;
        let block_index = test_miner_blocks
            .iter()
            .position(|b| b.header.block() == block);
        let block_index = block_index.unwrap_or_else(|| {
            panic!("Block not found with skylink {:?}", block);
        });

        // Get the previous lite headers.
        for (i, block_index) in ((block_index as i64 - 1 - NUM_PREV_BLOCK_HEADERS as i64)
            ..block_index as i64 - 1)
            .enumerate()
        {
            if block_index < 0 {
                prev_block_headers.0[i] = None;
            } else {
                let prev_block = &test_miner_blocks[block_index as usize];
                prev_block_headers.0[i] = Some(prev_block.header.lite_header.clone());
            }
        }

        // Get the previous block header.
        let prev_block_bytes = if block_index > 0 {
            let prev_block = &test_miner_blocks[block_index - 1];
            encode(&prev_block.header)
        } else {
            vec![0; BlockHeader::SIZE]
        };

        let mut bytes = vec![];
        bytes.extend(encode(&prev_block_headers));
        bytes.extend(prev_block_bytes);

        Self { bytes, index: 0 }
    }

    /// Reads a given amount of bytes from the download stream.
    pub fn read(&mut self, size: usize) -> io::Result<Vec<u8>> {
        let result = self.bytes[self.index..self.index + size].to_vec();
        self.index += size;
        Ok(result)
    }

    /// Reads all the headers.
    pub fn read_all_headers(&mut self) -> io::Result<Vec<u8>> {
        self.read(TOTAL_HEADERS_SIZE)
    }

    // TODO: Return an iterator.
    /// Helper function to download leaf transformations in segments but return them
    /// one by one.
    pub fn iterate_leaf_transformations(
        &self,
        _num_leaf_transformations: u32,
        _segment_size: usize,
    ) -> Result<Vec<LeafTransformation>> {
        unimplemented!();
    }

    pub fn iterate_new_leaf_proofs(&self) -> Result<Vec<NewLeafProof>> {
        unimplemented!();
    }

    pub fn iterate_transactions(&self) -> Result<Vec<Transaction>> {
        unimplemented!();
    }
}
