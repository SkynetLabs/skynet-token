use super::{error::Result, LeafTransformationsMap};

// Dummy struct for now.
pub struct Transaction();

impl Transaction {
    pub fn execute_update(
        &self,
        _leaf_transformation_map: &mut LeafTransformationsMap,
    ) -> Result<()> {
        unimplemented!();
    }
}
