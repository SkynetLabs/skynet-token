use super::super::BlockHeader;
use super::{
    accounts::{AccountUpdateLeaf, AccountUpdateVerifier},
    error::Result,
    AccountUpdatesMap, LeafTransformationsMap, NewAccountsMap,
};
use crate::consensus::merkletree;
use std::collections::{HashMap, VecDeque};

// Dummy struct for now.
pub struct LeafTransformation();

impl LeafTransformation {
    pub fn verify_update(
        &self,
        _account_updates_map: &mut HashMap<AccountUpdateLeaf, AccountUpdateVerifier>,
    ) -> Result<()> {
        unimplemented!();
    }

    pub fn verify_leaf_index(
        &self,
        _prev_leaf_transformation: Option<&LeafTransformation>,
    ) -> Result<()> {
        unimplemented!();
    }

    pub fn verify_transformation_proof(
        &self,
        _prev_leaf_transformations: &VecDeque<LeafTransformation>,
    ) -> Result<()> {
        unimplemented!();
    }

    // TODO: Move to a field on the struct? I didn't implement the type yet.
    fn final_state(&self) -> Vec<u8> {
        unimplemented!();
    }

    // TODO: Move to a field on the struct? I didn't implement the type yet.
    pub fn leaf_id(&self) -> LeafId {
        unimplemented!();
    }

    // TODO: Move to a field on the struct? I didn't implement the type yet.
    pub fn verifier(&self) -> LeafTransformationVerifier {
        unimplemented!();
    }

    pub fn proof_fill(&self, _merkle_tree: &mut merkletree::Tree) {
        unimplemented!();
    }

    pub fn add_leaf_to_map(
        &self,
        leaf_transformation_map: &mut HashMap<LeafId, LeafTransformationVerifier>,
    ) {
        leaf_transformation_map.insert(self.leaf_id(), self.verifier());
        // TODO: Set the currentState of the verifier equal to the initial state of
        // the leaf.
        unimplemented!();
    }
}

// Dummy struct for now.
#[derive(Hash, PartialEq, Eq)]
pub struct LeafId();

// Dummy struct for now.
pub struct LeafTransformationVerifier();

// Dummy struct for now.
pub struct NewLeafProof();

impl NewLeafProof {
    pub fn verify_in_leaf_transformations_map(
        &self,
        _leaf_transformation_map: &LeafTransformationsMap,
    ) -> Result<()> {
        unimplemented!();
    }

    pub fn create_leaf_transformation_verifier(
        &self,
        _leaf_transformation_map: &mut LeafTransformationsMap,
    ) -> Result<()> {
        unimplemented!();
    }

    pub fn incorporate_leaf_into_merkle_root(&self, _merkle_tree: &mut merkletree::Tree) {
        unimplemented!();
    }

    pub fn verify_account_update(
        &self,
        _account_updates_map: &mut AccountUpdatesMap,
        _non_new_account_found: &mut bool,
    ) -> Result<()> {
        unimplemented!();
    }

    pub fn verify_new_account(
        &self,
        _new_accounts_map: &mut NewAccountsMap,
        _non_new_account_found: bool,
    ) -> Result<()> {
        unimplemented!();
    }
}

pub fn verify_leaf_transformations_root(
    _parent_header: &BlockHeader,
    _root: &mut merkletree::Tree,
) -> Result<()> {
    unimplemented!();
}

pub fn verify_final_merkle_root(
    _header: &BlockHeader,
    _merkle_tree: merkletree::Tree,
) -> Result<()> {
    unimplemented!();
}

pub fn verify_leaf_transformations_map(
    _leaf_transformation_map: LeafTransformationsMap,
) -> Result<()> {
    unimplemented!();
}
