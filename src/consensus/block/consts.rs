//! Block constants.

// TODO: Remove once we have the production genesis block. Need this for now
// since some imports are only used in 'testing' mode.
#![allow(unused_imports)]

use crate::build_var_lazy;
use crate::consensus::{
    block::{BlockHeader, BlockHeight, BlockId, Hash, LiteHeader, Nonce, PrevTimestamps, Skylink},
    consts::BLOCK_FREQUENCY,
    difficulty, merkletree,
    target::{Target, ROOT_DEPTH},
    time,
};
use lazy_static::lazy_static;

build_var_lazy! {
    /// The hard-coded genesis block.
    pub static ref GENESIS_BLOCK: BlockHeader = {
        standard = unimplemented!(), // TODO: Replace with actual value.
        testing = BlockHeader {
            lite_header: LiteHeader {
                parent_id: GENESIS_PARENT_ID,
                timestamp: GENESIS_TIMESTAMP.unwrap(),
                pow_mask: Hash([0; 32]),
                nonce: Nonce([0; 8]),
                block: Skylink([0; 34]),
                state_root: GENESIS_STATE_ROOT,
                num_leaves: 0,
                num_account_updates: 0,
                num_new_accounts: 0,
            },
            base_fee_step: 0,
            decayed_total_target: ROOT_DEPTH,
            target: GENESIS_TARGET,
            child_target: *GENESIS_CHILD_TARGET,
            prev_timestamps: GENESIS_PREV_TIMESTAMPS.clone(),
            parent_decayed_total_target: GENESIS_PARENT_DECAYED_TOTAL_TARGET,
            parent_decayed_total_time: GENESIS_PARENT_DECAYED_TOTAL_TIME,
            height: GENESIS_HEIGHT,
        },
    };

    /// The ID of the genesis block.
    pub static ref GENESIS_ID: BlockId = {
        standard = unimplemented!(), // TODO: Replace with actual value.
        testing = GENESIS_BLOCK.clone().compute_id(),
    };

    /// The timestamp of the genesis block.
    pub static ref GENESIS_TIMESTAMP: Option<u64> = {
        standard = None, // TODO: Replace with actual value.
        testing = Some(time::current_timestamp() - BLOCK_FREQUENCY),
    };
}

/// The target for the genesis block.
pub const GENESIS_TARGET: Target = ROOT_DEPTH;

const GENESIS_PARENT_ID: BlockId = BlockId([0; 32]);
const GENESIS_STATE_ROOT: merkletree::Hash = merkletree::Hash([0; 32]);
const GENESIS_PARENT_DECAYED_TOTAL_TARGET: Target = ROOT_DEPTH;
const GENESIS_PARENT_DECAYED_TOTAL_TIME: u64 = 0;
const GENESIS_HEIGHT: BlockHeight = 1;

lazy_static! {
    /// The previous timestamps for the genesis block. These timestamps are
    /// dummy values as they don't actually correspond to any blocks. Frequency
    /// is 10 minutes in production and 1 second in testing.
    static ref GENESIS_PREV_TIMESTAMPS: PrevTimestamps = PrevTimestamps([
        GENESIS_TIMESTAMP.unwrap() - 11 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 10 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 9 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 8 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 7 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 6 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 5 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 4 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 3 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - 2 * BLOCK_FREQUENCY,
        GENESIS_TIMESTAMP.unwrap() - BLOCK_FREQUENCY
    ]);

    /// The child target of the genesis block.
    pub static ref GENESIS_CHILD_TARGET: Target = difficulty::child_target(
        GENESIS_PARENT_DECAYED_TOTAL_TIME,
        GENESIS_PARENT_DECAYED_TOTAL_TARGET,
        GENESIS_TARGET,
        GENESIS_HEIGHT - 1,               // parent_height
        GENESIS_PREV_TIMESTAMPS.parent(), // parent_timestamp
        &difficulty::CONSENSUS_CONFIG_NORMAL,
    );
}
