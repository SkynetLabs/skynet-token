//! This module describes the layout of the blocks and their headers and also
//! provides some information about related types.

#![allow(dead_code)]

pub mod consts;
#[cfg(test)]
mod random;
mod solve;
#[cfg(test)]
mod test_consts;
pub mod verify;

use crate::build_var;
use crate::consensus::{
    difficulty::{self, decay_total_time},
    fee, merkletree,
    target::Target,
    time::BlockTimestamp,
};
use crate::encoding::{Decode, Decoder, Encode, Encoder, Result as EncodingResult};
use num::bigint::{BigInt, TryFromBigIntError};
use sha3::{Digest, Sha3_256};

/// The raw size in bytes of a skylink.
const RAW_SKYLINK_SIZE: usize = 34;

/// The raw size in bytes of a header encoded for PoW.
const ENCODED_HEADER_FOR_POW_SIZE: usize = 40;

build_var! {
    /// The number of previous block lite headers. This does not include the
    /// parent block we store which is a full block header.
    pub const NUM_PREV_BLOCK_HEADERS: usize = {
        standard = 9999,
        testing = 9,
    };
}

/// Identifier of the block, computed as a hash of the `BlockHeader`.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct BlockId(pub [u8; 32]);

impl Encode for BlockId {
    const SIZE: usize = <[u8; 32]>::SIZE;

    fn encode(&self, bytes: &mut [u8]) {
        self.0.encode(bytes);
    }
}
impl Decode for BlockId {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        <[u8; 32]>::decode(bytes).map(Self)
    }
}

/// A 32-byte hash.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Hash(pub [u8; 32]);

impl Encode for Hash {
    const SIZE: usize = <[u8; 32]>::SIZE;

    fn encode(&self, bytes: &mut [u8]) {
        self.0.encode(bytes);
    }
}
impl Decode for Hash {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        <[u8; 32]>::decode(bytes).map(Self)
    }
}

/// A 8-byte nonce.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Nonce(pub [u8; 8]);

impl Encode for Nonce {
    const SIZE: usize = <[u8; 8]>::SIZE;

    fn encode(&self, bytes: &mut [u8]) {
        self.0.encode(bytes);
    }
}
impl Decode for Nonce {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        <[u8; 8]>::decode(bytes).map(Self)
    }
}

/// A 34-byte string.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Skylink(pub [u8; RAW_SKYLINK_SIZE]);

impl Encode for Skylink {
    const SIZE: usize = RAW_SKYLINK_SIZE;

    fn encode(&self, bytes: &mut [u8]) {
        bytes.copy_from_slice(&self.0);
    }
}
impl Decode for Skylink {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        Ok(Self(bytes.try_into()?))
    }
}

// NOTE: can't derive Default for this type.
impl Default for Skylink {
    fn default() -> Self {
        Self([0; RAW_SKYLINK_SIZE])
    }
}

/// Describes the associated height of a block.
pub type BlockHeight = u64;

/// The byte of a header encoded for PoW.
pub struct EncodedHeaderForPow(pub [u8; ENCODED_HEADER_FOR_POW_SIZE]);

/// The header is derived from the block and is the object that gets hashed for
/// the proof of work. It covers all the fields of the block itself in some
/// form. That means modifying the block always modifies the header as well.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct BlockHeader {
    /// Subset of information in the block header that is contained within the
    /// block's body for subsequent blocks.
    pub lite_header: LiteHeader,

    /// Block state.
    ///
    /// The following fields make up a part of the block state, it contains
    /// additional information about the block we need in the header. That way
    /// we keep the amount of data we need to rehash in every iteration of the
    /// pow small.

    /// The baseFee establishes how many tokens need to be burned for a
    /// transaction to be accepted into the block. This does not need to be part
    /// of the LiteHeader as it can be computed from all of the data in the
    /// parent header.
    base_fee_step: u16,

    /// Difficulty state.
    ///
    /// The following fields are required to compute the difficulty of the next
    /// block. The difficulty trails by one block. The difficulty of a child
    /// block is determined by the decayedTotalTarget and decayedTotalTime of
    /// its grandparent.

    /// The decayed total target. This is the total amount of work that has been
    /// put into the block. You can compute this by summing up the inverses of
    /// all the block targets and then inverting them.
    decayed_total_target: Target,
    /// The target of the current block.
    target: Target,
    /// The target of the next block.
    child_target: Target,

    /// The timestamps of the last 11 blocks. The timestamp of a new
    /// block is not allowed to be lower than the median timestamp of
    /// the past 11 blocks.
    prev_timestamps: PrevTimestamps,

    /// The decayed total target as of the parent block. This is like target,
    /// except it gets decayed every time a new block is produced.
    parent_decayed_total_target: Target,

    /// The decayed total time as of the parent block. This is the amount of
    /// time that has elapsed since genesis, except it gets decayed every time a
    /// new block is produced.
    parent_decayed_total_time: u64,

    /// The height of the block within the chain.
    height: BlockHeight,
}

/// The timestamps of the last 11 blocks. The timestamp of a new block is not
/// allowed to be lower than the median timestamp of the past 11 blocks.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct PrevTimestamps(pub [BlockTimestamp; 11]);

impl Encode for PrevTimestamps {
    const SIZE: usize = BlockTimestamp::SIZE * 11;

    fn encode(&self, bytes: &mut [u8]) {
        let mut encoder = Encoder::with_bytes(bytes);
        for block_timestamp in self.0 {
            encoder.encode_field(&block_timestamp);
        }
    }
}
impl Decode for PrevTimestamps {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        let mut result = [0u64; 11];
        let mut decoder = Decoder::with_bytes(bytes);
        for prev_timestamp in &mut result {
            *prev_timestamp = decoder.decode_field()?;
        }
        decoder.finalize()?;
        Ok(PrevTimestamps(result))
    }
}

impl PrevTimestamps {
    /// Returns the timestamp of the parent.
    pub fn parent(&self) -> BlockTimestamp {
        self.0[10]
    }
    /// Returns the timestamp of the grandparent.
    pub fn grandparent(&self) -> BlockTimestamp {
        self.0[9]
    }

    /// Returns the median timestamp of the previous 11 blocks. Note that this
    /// requires us to sort first before we get the middle timestamp, as blocks
    /// are not required to be in strictly chronological order.
    pub fn median(&self) -> BlockTimestamp {
        let mut prev_timestamps = self.clone();
        // Unstable sort is faster and doesn't allocate. We do not need this
        // sort to be stable.
        prev_timestamps.0.sort_unstable();
        prev_timestamps.0[5]
    }
}

impl Encode for BlockHeader {
    const SIZE: usize =
        LiteHeader::SIZE       // lite_header
        + u16::SIZE            // base_fee_step
        + Target::SIZE         // decayed_total_target
        + Target::SIZE         // target
        + Target::SIZE         // child_target
        + PrevTimestamps::SIZE // prev_timestamps
        + Target::SIZE         // parent_decayed_total_target
        + u64::SIZE            // parent_decayed_total_time
        + BlockHeight::SIZE    // height
        ;

    fn encode(&self, bytes: &mut [u8]) {
        let mut encoder = Encoder::with_bytes(bytes);
        encoder.encode_field(&self.lite_header);
        encoder.encode_field(&self.base_fee_step);
        encoder.encode_field(&self.decayed_total_target);
        encoder.encode_field(&self.target);
        encoder.encode_field(&self.child_target);
        encoder.encode_field(&self.prev_timestamps);
        encoder.encode_field(&self.parent_decayed_total_target);
        encoder.encode_field(&self.parent_decayed_total_time);
        encoder.encode_field(&self.height);
    }
}
impl Decode for BlockHeader {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        let mut decoder = Decoder::with_bytes(bytes);
        let result = Self {
            lite_header: decoder.decode_field()?,
            base_fee_step: decoder.decode_field()?,
            decayed_total_target: decoder.decode_field()?,
            target: decoder.decode_field()?,
            child_target: decoder.decode_field()?,
            prev_timestamps: decoder.decode_field()?,
            parent_decayed_total_target: decoder.decode_field()?,
            parent_decayed_total_time: decoder.decode_field()?,
            height: decoder.decode_field()?,
        };
        decoder.finalize()?;
        Ok(result)
    }
}

impl BlockHeader {
    /// Returns the child target.
    pub fn child_target(&self) -> &Target {
        &self.child_target
    }

    /// Returns the decayed total target for this block.
    pub fn decayed_total_target(&self) -> Target {
        self.decayed_total_target
    }

    /// Calculates and returns the decayed total time for this block.
    pub fn decayed_total_time(&self) -> Result<u64, TryFromBigIntError<BigInt>> {
        decay_total_time(
            self.parent_decayed_total_time,
            self.lite_header.timestamp,
            self.prev_timestamps.parent(),
        )
    }

    /// Returns the block skylink.
    pub fn block(&self) -> &Skylink {
        &self.lite_header.block
    }

    /// Returns the block height.
    pub fn height(&self) -> BlockHeight {
        self.height
    }

    /// Encodes the header with the nonce and a hash of all other fields.
    ///
    /// ```ignore
    /// EncodedHeaderForPow {
    ///     nonce // 8 bytes
    ///     hashOfRemainingHeader // 32 bytes
    /// }
    /// ```
    ///
    /// where
    ///
    /// ```ignore
    /// hashOfRemainingHeader = Hash(parent_id, timestamp, pow_mask, block, state_root, num_leaves,
    /// num_account_updates, num_new_accounts, base_fee_step, ...difficulty_state)
    /// ```
    pub fn encode_for_pow(&self) -> EncodedHeaderForPow {
        let mut result = [0; ENCODED_HEADER_FOR_POW_SIZE];

        result[0..Nonce::SIZE].copy_from_slice(&self.lite_header.nonce.0);

        // Some fields need to be encoded first. Use a single allocation. Note
        // that each call of `encode` is one allocation, so we avoid its use.
        // TODO: Extract this into a macro or something?
        const BUFFER_SIZE: usize = BlockTimestamp::SIZE // lite_header.timestamp
            + u64::SIZE            // lite_header.num_leaves
            + u16::SIZE            // lite_header.num_account_updates
            + u16::SIZE            // lite_header.num_new_accounts
            + u16::SIZE            // base_fee_step
            + PrevTimestamps::SIZE // prev_timestamps
            + u64::SIZE            // parent_decayed_total_time
            + BlockHeight::SIZE; // height
        let mut buffer = vec![0; BUFFER_SIZE];
        {
            let mut encoder = Encoder::with_bytes(&mut buffer);
            encoder.encode_field(&self.lite_header.timestamp);
            encoder.encode_field(&self.lite_header.num_leaves);
            encoder.encode_field(&self.lite_header.num_account_updates);
            encoder.encode_field(&self.lite_header.num_new_accounts);
            encoder.encode_field(&self.base_fee_step);
            encoder.encode_field(&self.prev_timestamps);
            encoder.encode_field(&self.parent_decayed_total_time);
            encoder.encode_field(&self.height);
        }
        let mut buffer_i = 0;

        let mut hasher = Sha3_256::new();
        {
            hasher.update(self.lite_header.parent_id.0);
            hasher.update(&buffer[0..BlockTimestamp::SIZE]); // timestamp
            buffer_i += BlockTimestamp::SIZE;
            hasher.update(self.lite_header.pow_mask.0);
            hasher.update(self.lite_header.block.0);
            hasher.update(self.lite_header.state_root.0);
            hasher.update(&buffer[buffer_i..buffer_i + u64::SIZE]); // num_leaves
            buffer_i += u64::SIZE;
            hasher.update(&buffer[buffer_i..buffer_i + u16::SIZE]); // num_account_updates
            buffer_i += u16::SIZE;
            hasher.update(&buffer[buffer_i..buffer_i + u16::SIZE]); // num_new_accounts
            buffer_i += u16::SIZE;
            hasher.update(&buffer[buffer_i..buffer_i + u16::SIZE]); // base_fee_step
            buffer_i += u16::SIZE;
            hasher.update(self.decayed_total_target.0);
            hasher.update(self.target.0);
            hasher.update(self.child_target.0);
            hasher.update(&buffer[buffer_i..buffer_i + PrevTimestamps::SIZE]); // prev_timestamps
            buffer_i += PrevTimestamps::SIZE;
            hasher.update(self.parent_decayed_total_target.0);
            hasher.update(&buffer[buffer_i..buffer_i + u64::SIZE]); // parent_decayed_total_time
            buffer_i += u64::SIZE;
            hasher.update(&buffer[buffer_i..buffer_i + u64::SIZE]); // height
        }

        let hash_of_remaining_header = hasher.finalize();
        result[Nonce::SIZE..].copy_from_slice(&hash_of_remaining_header);

        EncodedHeaderForPow(result)
    }
}

/// Contains all of the information of the BlockHeader which is put into a
/// block's body for subsequent blocks.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct LiteHeader {
    /// The id of the previous block in the chain.
    parent_id: BlockId,

    /// The unix timestamp at which the block was formed.
    pub timestamp: BlockTimestamp,

    /// The block header makes use of a trick to allow for trustless mining
    /// pools where miners can't cheat the pool by hiding solved shares which
    /// would be good enough to create a full block. That works by xor'ing a
    /// secret pow_mask to the hash provided by the miner which will then be
    /// compared to the network target instead of directly comparing the hash.
    ///
    /// An explanation + implementation can be found here:
    /// https://github.com/handshake-org/hsd/blob/master/lib/primitives/abstractblock.js#L368-L408
    pow_mask: Hash,

    /// The grinding space for the miner. It's an arbitrary data field which is
    /// modified when mining.
    pub nonce: Nonce,

    /// A skylink that points towards the actual body of the block.
    block: Skylink,

    /// The root of the merkle tree over all accounts and their state.
    state_root: merkletree::Hash,

    /// Block state.
    ///
    /// The following fields make up one part of the block state, they will be
    /// summarized as a hash when doing the PoW.

    /// Number of leaves in the chainstate.
    num_leaves: u64,

    /// Number of account updates that occured in the block. This number will be
    /// smaller than the number of leaves that changed, as there can be leaf
    /// updates which do not impact any accounts. Note that numAccountUpdates
    /// will include updates to accounts that were just created in this block
    num_account_updates: u16,

    /// Number of new accounts that got created in this block. Note that not all
    /// leaves in the chainstate are accounts, and the non-account leaves that
    /// get added will not be added to numNewAccounts.
    num_new_accounts: u16,
}

impl Encode for LiteHeader {
    const SIZE: usize =
        BlockId::SIZE            // parent_id
        + BlockTimestamp::SIZE   // timestamp
        + Hash::SIZE             // pow_mask
        + Nonce::SIZE            // nonce
        + Skylink::SIZE          // block
        + merkletree::Hash::SIZE // state_root
        + u64::SIZE              // num_leaves
        + u16::SIZE              // num_account_updates
        + u16::SIZE              // num_new_accounts
        ;

    fn encode(&self, bytes: &mut [u8]) {
        let mut encoder = Encoder::with_bytes(bytes);
        encoder.encode_field(&self.parent_id);
        encoder.encode_field(&self.timestamp);
        encoder.encode_field(&self.pow_mask);
        encoder.encode_field(&self.nonce);
        encoder.encode_field(&self.block);
        encoder.encode_field(&self.state_root);
        encoder.encode_field(&self.num_leaves);
        encoder.encode_field(&self.num_account_updates);
        encoder.encode_field(&self.num_new_accounts);
    }
}
impl Decode for LiteHeader {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        let mut decoder = Decoder::with_bytes(bytes);
        let result = Self {
            parent_id: decoder.decode_field()?,
            timestamp: decoder.decode_field()?,
            pow_mask: decoder.decode_field()?,
            nonce: decoder.decode_field()?,
            block: decoder.decode_field()?,
            state_root: decoder.decode_field()?,
            num_leaves: decoder.decode_field()?,
            num_account_updates: decoder.decode_field()?,
            num_new_accounts: decoder.decode_field()?,
        };
        decoder.finalize()?;
        Ok(result)
    }
}

/// A solved block, containing the underlying block header and the block ID. The
/// ID depends on the final nonce, hence why the block has to be solved. Also,
/// the ID is expensive to compute, so we store it here instead of computing it
/// each time. [SolvedBlockHeader] can be derived by calling `solve` on
/// `BlockHeader`.
#[derive(Debug, Clone)]
pub struct SolvedBlockHeader {
    /// The underlying block header.
    pub header: BlockHeader,
    /// The ID of the solved block. This depends on the final nonce, hence why
    /// the block has to be solved.
    pub id: BlockId,
}

impl SolvedBlockHeader {
    /// Derive a child given the current block header, the skylink of the
    /// uploaded child block, and the pow mask.
    pub fn derive_child(
        &self,
        child_block: Skylink,
        pow_mask: Hash,
        emergency_mode: bool,
        current_timestamp: u64,
    ) -> Result<BlockHeader, TryFromBigIntError<BigInt>> {
        let base_fee_step = fee::base_fee_step_for_child(
            self.header.base_fee_step,
            self.header.lite_header.num_account_updates as u64,
            emergency_mode,
        );

        let decayed_total_target =
            difficulty::decay_total_target(self.header.decayed_total_target, self.header.target);
        let target = self.header.child_target;
        let child_target = difficulty::child_target(
            self.header.parent_decayed_total_time,
            self.header.parent_decayed_total_target,
            self.header.target,                   // current_target
            self.header.height - 1,               // parent_height
            self.header.prev_timestamps.parent(), // parent_timestamp
            &difficulty::ConsensusConfig::from_emergency_mode(emergency_mode),
        );

        let mut prev_timestamps = PrevTimestamps::default();
        prev_timestamps.0[0..10].copy_from_slice(&self.header.prev_timestamps.0[1..11]);
        prev_timestamps.0[10] = self.header.lite_header.timestamp;

        let parent_decayed_total_target = self.header.decayed_total_target;
        let parent_decayed_total_time = difficulty::decay_total_time(
            self.header.parent_decayed_total_time, // prev_decayed_total_time
            self.header.lite_header.timestamp,     // new_timestamp
            self.header.prev_timestamps.parent(),  // prev_timestamp
        )?;

        Ok(BlockHeader {
            lite_header: LiteHeader {
                parent_id: self.id.clone(),
                timestamp: current_timestamp,
                pow_mask,
                nonce: Nonce([0; 8]),
                block: child_block,
                state_root: merkletree::Hash([0; 32]), // TODO
                num_leaves: 0,                         // TODO
                num_account_updates: 0,                // TODO
                num_new_accounts: 0,                   // TODO
            },
            base_fee_step,
            decayed_total_target,
            target,
            child_target,
            prev_timestamps,
            parent_decayed_total_target,
            parent_decayed_total_time,
            height: self.header.height + 1,
        })
    }
}

/// Describes a Skynet Token Block. It links to the previous block and contains
/// not only transactions but also a summary of the transactions.
#[derive(Debug, Default, PartialEq, Eq)]
pub struct Block {
    // TODO: What will this be for the first block?
    /// The first previous header.
    prev_header: BlockHeader,

    /// The partial headers of the previous 9999 blocks, before the
    /// `prev_header`. A [`LiteHeader`] is a [`BlockHeader`] minus
    /// anything that can be computed given the previous `BlockHeader`. For the
    /// most part, this is the `difficulty_state` and the `base_fee` for the
    /// transaction fees.
    block_headers: PrevBlockHeaders,
    // TODO:
    //
    // - accountShardOffsets
    // - activeAccounts
    // - accountUpdates
    //
    // - newAccountShardOffsets
    // - newAccounts
    //
    // - numLeafTransformations
    // - leafTransformations
    // - newLeafProofs
    // - transactions
}

impl Encode for Block {
    const SIZE: usize = BlockHeader::SIZE + PrevBlockHeaders::SIZE;

    fn encode(&self, bytes: &mut [u8]) {
        let mut encoder = Encoder::with_bytes(bytes);
        encoder.encode_field(&self.prev_header);
        encoder.encode_field(&self.block_headers);
    }
}
impl Decode for Block {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        let mut decoder = Decoder::with_bytes(bytes);
        let result = Self {
            prev_header: decoder.decode_field()?,
            block_headers: decoder.decode_field()?,
        };
        decoder.finalize()?;
        Ok(result)
    }
}

type PrevBlockHeadersArray = [Option<LiteHeader>; NUM_PREV_BLOCK_HEADERS];

/// Contains the previous 9999 headers of a block, before the previous header.
#[derive(Debug, PartialEq, Eq)]
pub struct PrevBlockHeaders(
    // NOTE: Need the box allocation to prevent this array from overflowing the
    // stack.
    Box<PrevBlockHeadersArray>,
);

impl Encode for PrevBlockHeaders {
    const SIZE: usize = LiteHeader::SIZE * NUM_PREV_BLOCK_HEADERS;

    fn encode(&self, bytes: &mut [u8]) {
        let default_lite_header = LiteHeader::default();

        let mut encoder = Encoder::with_bytes(bytes);
        for option in &*self.0 {
            if let Some(block_header) = option {
                encoder.encode_field(block_header);
            } else {
                // Encode all 0's for null headers.
                encoder.encode_field(&default_lite_header);
            }
        }
    }
}
impl Decode for PrevBlockHeaders {
    fn decode(bytes: &[u8]) -> EncodingResult<Self> {
        let mut result = Self::default();
        let default_lite_header = LiteHeader::default();

        let mut decoder = Decoder::with_bytes(bytes);
        for block_header in &mut *result.0 {
            let value = decoder.decode_field::<LiteHeader>()?;
            *block_header = if value == default_lite_header {
                // If we encounter all 0s e.g. a null header, return None.
                None
            } else {
                Some(value)
            };
        }
        decoder.finalize()?;
        Ok(result)
    }
}

// NOTE: can't derive Default for this type.
impl Default for PrevBlockHeaders {
    fn default() -> Self {
        // Need this because `[None; 9999]` doesn't work.
        const NONE: Option<LiteHeader> = None;
        Self(Box::new([NONE; NUM_PREV_BLOCK_HEADERS]))
    }
}

#[cfg(test)]
mod tests {
    use super::test_consts::*;
    use super::*;
    use crate::encoding::{decode, encode};
    use crate::test_utils::Random;

    mod block_id {
        use super::*;

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_block_id() {
            const INNER_BYTES: [u8; 32] = TEST_BLOCK_ID.0;

            let encoded = encode(&TEST_BLOCK_ID);
            assert_eq!(encoded, INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_block_id() -> EncodingResult<()> {
            let value = BlockId::default();
            assert_eq!(decode::<BlockId>(&encode(&value))?, value);

            let value = BlockId::random();
            assert_eq!(decode::<BlockId>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_block_id() {
            assert_ne!(BlockId::random(), BlockId::random());
        }
    }

    mod hash {
        use super::*;

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_hash() {
            const INNER_BYTES: [u8; 32] = TEST_HASH.0;

            let encoded = encode(&TEST_HASH);
            assert_eq!(encoded, INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_hash() -> EncodingResult<()> {
            let value = Hash::default();
            assert_eq!(decode::<Hash>(&encode(&value))?, value);

            let value = Hash::random();
            assert_eq!(decode::<Hash>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_hash() {
            assert_ne!(Hash::random(), Hash::random());
        }
    }

    mod nonce {
        use super::*;

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_nonce() {
            const INNER_BYTES: [u8; 8] = TEST_NONCE.0;

            let encoded = encode(&TEST_NONCE);
            assert_eq!(encoded, INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_nonce() -> EncodingResult<()> {
            let value = Nonce::default();
            assert_eq!(decode::<Nonce>(&encode(&value))?, value);

            let value = Nonce::random();
            assert_eq!(decode::<Nonce>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_nonce() {
            assert_ne!(Nonce::random(), Nonce::random());
        }
    }

    mod skylink {
        use super::*;

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_skylink() {
            const INNER_BYTES: [u8; 34] = TEST_SKYLINK.0;

            let encoded = encode(&TEST_SKYLINK);
            assert_eq!(encoded, INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_skylink() -> EncodingResult<()> {
            let value = Skylink::default();
            assert_eq!(decode::<Skylink>(&encode(&value))?, value);

            let value = Skylink::random();
            assert_eq!(decode::<Skylink>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_skylink() {
            assert_ne!(Skylink::random(), Skylink::random());
        }
    }

    mod prev_timestamps {
        use super::*;
        use std::cmp::Ordering;

        #[test]
        fn should_return_correct_median() {
            let prev_timestamps = PrevTimestamps::random();
            let median = prev_timestamps.median();

            // Count the number of timestamps less than, greater than, and equal
            // to the median.
            let (mut num_before, mut num_after, mut num_equal) = (0, 0, 0);
            for timestamp in prev_timestamps.0 {
                match timestamp.cmp(&median) {
                    Ordering::Less => num_before += 1,
                    Ordering::Greater => num_after += 1,
                    Ordering::Equal => num_equal += 1,
                }
            }
            // Any timestamps that were equal to the median should be added to
            // the lower bucket. Leave one that is equal - this is the median.
            assert!(num_equal >= 1);
            while num_equal > 1 {
                match num_before.cmp(&num_after) {
                    Ordering::Less => num_before += 1,
                    _ => num_after += 1,
                }
                num_equal -= 1;
            }

            assert_eq!(num_before, num_after);
        }

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_prev_timestamps() {
            const INNER_BYTES: &[u8] = &[
                139, 0, 0, 0, 0, 0, 0, 0, 140, 0, 0, 0, 0, 0, 0, 0, 141, 0, 0, 0, 0, 0, 0, 0, 142,
                0, 0, 0, 0, 0, 0, 0, 143, 0, 0, 0, 0, 0, 0, 0, 144, 0, 0, 0, 0, 0, 0, 0, 145, 0, 0,
                0, 0, 0, 0, 0, 146, 0, 0, 0, 0, 0, 0, 0, 147, 0, 0, 0, 0, 0, 0, 0, 148, 0, 0, 0, 0,
                0, 0, 0, 149, 0, 0, 0, 0, 0, 0, 0,
            ];

            let encoded = encode(&TEST_PREV_TIMESTAMPS);
            assert_eq!(encoded, INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_prev_timestamps() -> EncodingResult<()> {
            let value = PrevTimestamps::default();
            assert_eq!(decode::<PrevTimestamps>(&encode(&value))?, value);

            let value = PrevTimestamps::random();
            assert_eq!(decode::<PrevTimestamps>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_prev_timestamps() {
            assert_ne!(PrevTimestamps::random(), PrevTimestamps::random());
        }
    }

    mod lite_header {
        use super::*;

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_lite_header() {
            let encoded = encode(&TEST_LITE_HEADER);
            assert_eq!(encoded, TEST_LITE_HEADER_INNER_BYTES);
        }

        #[test]
        fn should_encode_and_decode_lite_header() -> EncodingResult<()> {
            let value = LiteHeader::default();
            assert_eq!(decode::<LiteHeader>(&encode(&value))?, value);

            let value = LiteHeader::random();
            assert_eq!(decode::<LiteHeader>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_lite_header() {
            assert_ne!(LiteHeader::random(), LiteHeader::random());
        }

        // NOTE: Keep the value in the spec in sync with the value here.
        #[test]
        fn test_lite_header_size() {
            assert_eq!(LiteHeader::SIZE, 158);
        }
    }

    mod block_header {
        use super::*;

        // Hard-code the expected encoding.
        #[test]
        fn should_encode_block_header() {
            let encoded = encode(&TEST_BLOCK_HEADER);
            assert_eq!(encoded, make_test_block_header_inner_bytes());
        }

        #[test]
        fn should_encode_and_decode_block_header() -> EncodingResult<()> {
            let value = BlockHeader::default();
            assert_eq!(decode::<BlockHeader>(&encode(&value))?, value);

            let value = BlockHeader::random();
            assert_eq!(decode::<BlockHeader>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_block_header() {
            assert_ne!(BlockHeader::random(), BlockHeader::random());
        }

        // Test against hard-coded values to catch any change in the
        // encoding.
        #[test]
        fn should_encode_for_pow() {
            let encoded_for_pow = TEST_BLOCK_HEADER.encode_for_pow();
            assert_eq!(encoded_for_pow.0, TEST_ENCODED_FOR_POW_BYTES);
            assert_eq!(
                encoded_for_pow.0[0..8],
                TEST_BLOCK_HEADER.lite_header.nonce.0
            );
        }

        // Test this function against the alternative, simpler implementation
        // which allocates for every field.
        #[test]
        fn should_encode_for_pow_with_manual_encoding() {
            let b = TEST_BLOCK_HEADER;
            let mut manual_encoding = [0; ENCODED_HEADER_FOR_POW_SIZE];
            manual_encoding[0..Nonce::SIZE].copy_from_slice(&b.lite_header.nonce.0);

            let mut hasher = Sha3_256::new();
            hasher.update(b.lite_header.parent_id.0);
            hasher.update(encode(&b.lite_header.timestamp));
            hasher.update(b.lite_header.pow_mask.0);
            hasher.update(b.lite_header.block.0);
            hasher.update(b.lite_header.state_root.0);
            hasher.update(encode(&b.lite_header.num_leaves));
            hasher.update(encode(&b.lite_header.num_account_updates));
            hasher.update(encode(&b.lite_header.num_new_accounts));
            hasher.update(encode(&b.base_fee_step));
            hasher.update(b.decayed_total_target.0);
            hasher.update(b.target.0);
            hasher.update(b.child_target.0);
            hasher.update(encode(&b.prev_timestamps));
            hasher.update(b.parent_decayed_total_target.0);
            hasher.update(encode(&b.parent_decayed_total_time));
            hasher.update(encode(&b.height));
            let hash_of_remaining_header = hasher.finalize();

            manual_encoding[8..].copy_from_slice(&hash_of_remaining_header);

            let encoded_for_pow = TEST_BLOCK_HEADER.encode_for_pow();
            assert_eq!(encoded_for_pow.0, manual_encoding);
        }

        // NOTE: Keep the value in the spec in sync with the value here.
        #[test]
        fn test_block_header_size() {
            assert_eq!(BlockHeader::SIZE, 392);
        }
    }

    mod block_headers {
        use super::*;

        #[test]
        fn should_encode_block_headers() {
            // Test the default.
            let inner_bytes = vec![0; PrevBlockHeaders::SIZE];
            let test_block_headers = PrevBlockHeaders::default();

            let encoded = encode(&test_block_headers);
            assert_eq!(encoded, inner_bytes);

            // Test some `None`s and some `Some`s.
            let test_block_headers = make_test_block_headers();

            let encoded = encode(&test_block_headers);
            assert_eq!(encoded, make_test_block_headers_inner_bytes().as_slice());
        }

        #[test]
        fn should_encode_and_decode_block_headers() -> EncodingResult<()> {
            let value = PrevBlockHeaders::default();
            assert_eq!(decode::<PrevBlockHeaders>(&encode(&value))?, value);

            let value = PrevBlockHeaders::random();
            assert_eq!(decode::<PrevBlockHeaders>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_block_headers() {
            assert_ne!(PrevBlockHeaders::random(), PrevBlockHeaders::random());
        }
    }

    mod block {
        use super::*;

        #[test]
        fn block_size_should_not_change() {
            build_var! {
                const EXPECTED: usize = {
                    standard = 1_580_234,
                    testing = 1_814,
                };
            }
            assert_eq!(Block::SIZE, EXPECTED);
        }

        #[test]
        fn should_encode_block() {
            // Test the default.
            let inner_bytes = vec![0; Block::SIZE];
            let test_block_headers = Block::default();

            let encoded = encode(&test_block_headers);
            assert_eq!(encoded, inner_bytes);

            // Test a more interesting block.
            let test_block = make_test_block();

            let encoded = encode(&test_block);
            assert_eq!(encoded, make_test_block_inner_bytes().as_slice());
        }

        #[test]
        fn should_encode_and_decode_block() -> EncodingResult<()> {
            let value = Block::default();
            assert_eq!(decode::<Block>(&encode(&value))?, value);

            let value = Block::random();
            assert_eq!(decode::<Block>(&encode(&value))?, value);

            Ok(())
        }

        #[test]
        fn should_generate_random_block() {
            assert_ne!(Block::random(), Block::random());
        }
    }
}
