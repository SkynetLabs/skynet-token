//! Testing values that are used throughout the `block` module.

use super::*;

pub const TEST_BLOCK_ID: BlockId = BlockId([
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32,
]);

pub const TEST_HASH: Hash = Hash([
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32,
]);

pub const TEST_NONCE: Nonce = Nonce([1, 2, 3, 4, 5, 6, 7, 8]);

pub const TEST_SKYLINK: Skylink = Skylink([
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32, 33, 34,
]);

pub const TEST_DECAYED_TOTAL_TARGET: Target = Target([
    107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125,
    126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,
]);

pub const TEST_TARGET: Target = Target([
    108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126,
    127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
]);

pub const TEST_CHILD_TARGET: Target = Target([
    109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
    128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
]);

pub const TEST_PARENT_DECAYED_TOTAL_TARGET: Target = Target([
    110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128,
    129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141,
]);

pub const TEST_PREV_TIMESTAMPS: PrevTimestamps =
    PrevTimestamps([139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149]);

pub const TEST_LITE_HEADER: LiteHeader = LiteHeader {
    parent_id: TEST_BLOCK_ID,
    timestamp: 256,
    pow_mask: TEST_HASH,
    nonce: TEST_NONCE,
    block: TEST_SKYLINK,
    state_root: merkletree::Hash([
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
        26, 27, 28, 29, 30, 31, 32,
    ]),
    num_leaves: 1337,
    num_account_updates: 1338,
    num_new_accounts: 1339,
};

pub const TEST_BLOCK_HEADER: BlockHeader = BlockHeader {
    lite_header: TEST_LITE_HEADER,
    base_fee_step: 1004,
    decayed_total_target: TEST_DECAYED_TOTAL_TARGET,
    target: TEST_TARGET,
    child_target: TEST_CHILD_TARGET,
    prev_timestamps: TEST_PREV_TIMESTAMPS,
    parent_decayed_total_target: TEST_PARENT_DECAYED_TOTAL_TARGET,
    parent_decayed_total_time: 1005,
    height: 1007,
};

pub const TEST_ENCODED_FOR_POW_BYTES: &[u8] = &[
    1, 2, 3, 4, 5, 6, 7, 8, 33, 112, 70, 28, 60, 71, 55, 180, 108, 86, 218, 191, 75, 159, 29, 39,
    87, 135, 198, 100, 9, 149, 181, 124, 13, 9, 122, 112, 176, 228, 113, 153,
];

/// Build the test PrevBlockHeaders.
pub fn make_test_block_headers() -> PrevBlockHeaders {
    const NUM_NONE: usize = NUM_PREV_BLOCK_HEADERS / 3;

    let mut test_block_headers = PrevBlockHeaders::default();
    // Set a bunch of headers to be `None` to test the case of not having enough
    // headers.
    for i in 0..NUM_NONE {
        test_block_headers.0[i] = None;
    }
    for i in NUM_NONE..NUM_PREV_BLOCK_HEADERS {
        test_block_headers.0[i] = Some(TEST_LITE_HEADER.clone());
    }

    test_block_headers
}

/// Build the test Block.
pub fn make_test_block() -> Block {
    Block {
        prev_header: TEST_BLOCK_HEADER,
        block_headers: make_test_block_headers(),
    }
}

// Expected inner bytes.
pub const TEST_LITE_HEADER_INNER_BYTES: &[u8] = &[
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32, 0, 1, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 1, 2, 3, 4, 5, 6, 7, 8,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32, 33, 34, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 57, 5, 0, 0, 0, 0, 0, 0, 58, 5, 59, 5,
];

pub fn make_test_block_header_inner_bytes() -> Vec<u8> {
    let mut inner_bytes_vec = TEST_LITE_HEADER_INNER_BYTES.to_vec();
    inner_bytes_vec.extend([
        236, 3, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122,
        123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 108, 109,
        110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
        128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 109, 110, 111, 112, 113, 114,
        115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132,
        133, 134, 135, 136, 137, 138, 139, 140, 139, 0, 0, 0, 0, 0, 0, 0, 140, 0, 0, 0, 0, 0, 0, 0,
        141, 0, 0, 0, 0, 0, 0, 0, 142, 0, 0, 0, 0, 0, 0, 0, 143, 0, 0, 0, 0, 0, 0, 0, 144, 0, 0, 0,
        0, 0, 0, 0, 145, 0, 0, 0, 0, 0, 0, 0, 146, 0, 0, 0, 0, 0, 0, 0, 147, 0, 0, 0, 0, 0, 0, 0,
        148, 0, 0, 0, 0, 0, 0, 0, 149, 0, 0, 0, 0, 0, 0, 0, 110, 111, 112, 113, 114, 115, 116, 117,
        118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135,
        136, 137, 138, 139, 140, 141, 237, 3, 0, 0, 0, 0, 0, 0, 239, 3, 0, 0, 0, 0, 0, 0,
    ]);
    inner_bytes_vec
}

/// Inner bytes for the test block headers. The bytes should have zeros for all
/// the `None`s, and the expected lite header bytes for each `Some`.
pub fn make_test_block_headers_inner_bytes() -> Vec<u8> {
    const NUM_NONE: usize = NUM_PREV_BLOCK_HEADERS / 3;
    const NUM_SOME: usize = NUM_PREV_BLOCK_HEADERS - NUM_NONE;

    let mut inner_bytes_vec = [0].repeat(NUM_NONE * LiteHeader::SIZE);
    inner_bytes_vec.extend(TEST_LITE_HEADER_INNER_BYTES.repeat(NUM_SOME));
    inner_bytes_vec
}

/// Inner bytes for the test block.
pub fn make_test_block_inner_bytes() -> Vec<u8> {
    let mut inner_bytes_vec = make_test_block_header_inner_bytes();
    inner_bytes_vec.extend(make_test_block_headers_inner_bytes());
    inner_bytes_vec
}
