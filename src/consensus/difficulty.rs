#![allow(dead_code)]

use crate::consensus::{
    block::{consts::GENESIS_TIMESTAMP, BlockHeight},
    consts::BLOCK_FREQUENCY,
    emergency_mode::{EMERGENCY_OAK_MAX_DROP, EMERGENCY_OAK_MAX_RISE},
    target::{Target, ROOT_DEPTH},
    time::BlockTimestamp,
};
use lazy_static::lazy_static;
use num::traits::Zero;
use num::BigRational;
use num_bigint::{BigInt, TryFromBigIntError};

/// The block shift determines the most that the difficulty adjustment
/// algorithm is allowed to shift the target block time. With a block
/// frequency of 600 seconds, the min target block time is 200 seconds,
/// and the max target block time is 1800 seconds.
const OAK_MAX_BLOCK_SHIFT: u64 = 3;

// These types can't be constructed in a const context.
// See https://github.com/rust-num/num-bigint/issues/181.
lazy_static! {
    /// The decay is kept at 995/1000, or a decay of about 0.5% each block.
    /// This puts the halflife of a block's relevance at about 1 day. This
    /// allows the difficulty to adjust rapidly if the hashrate is adjusting
    /// rapidly, while still keeping a relatively strong insulation against
    /// random variance.
    pub static ref OAK_DECAY_NUMERATOR: BigInt = 995.into();
    /// See [`OAK_DECAY_NUMERATOR`].
    pub static ref OAK_DECAY_DENOMINATOR: BigInt = 1_000.into();

    /// The max rise and max drop for the difficulty is kept at 0.4% per
    /// block, which means that in 1008 blocks the difficulty can move a
    /// maximum of about 55x. This is significant, and means that dramatic
    /// hashrate changes can be responded to quickly, while still forcing an
    /// attacker to do a significant amount of work in order to execute a
    /// difficulty raising attack, and minimizing the chance that an attacker
    /// can get lucky and fake a ton of work.
    static ref OAK_MAX_RISE: BigRational = BigRational::new(1004.into(), 1_000.into());
    /// See [`OAK_MAX_RISE`].
    static ref OAK_MAX_DROP: BigRational = BigRational::new(1_000.into(), 1004.into());

    pub static ref CONSENSUS_CONFIG_NORMAL: ConsensusConfig = ConsensusConfig{
        max_rise: OAK_MAX_RISE.clone(),
        max_drop: OAK_MAX_DROP.clone(),
        max_block_shift: OAK_MAX_BLOCK_SHIFT,
        block_frequency: BLOCK_FREQUENCY,
        genesis_timestamp: GENESIS_TIMESTAMP.unwrap(),
    };
    pub static ref CONSENSUS_CONFIG_EMERGENCY: ConsensusConfig = ConsensusConfig{
        max_rise: EMERGENCY_OAK_MAX_RISE.clone(),
        max_drop: EMERGENCY_OAK_MAX_DROP.clone(),
        max_block_shift: OAK_MAX_BLOCK_SHIFT,
        block_frequency: BLOCK_FREQUENCY,
        genesis_timestamp: GENESIS_TIMESTAMP.unwrap(),
    };
}

/// Configuration params related to consensus which affects the behaviour of the
/// difficulty adjustment algorithm.
#[derive(Clone)]
pub struct ConsensusConfig {
    pub max_rise: BigRational,
    pub max_drop: BigRational,
    pub max_block_shift: u64,
    pub block_frequency: u64,
    pub genesis_timestamp: BlockTimestamp,
}

impl ConsensusConfig {
    pub fn from_emergency_mode(emergency_mode: bool) -> Self {
        if emergency_mode {
            CONSENSUS_CONFIG_EMERGENCY.clone()
        } else {
            CONSENSUS_CONFIG_NORMAL.clone()
        }
    }
}

/// Computes the target of a block's child given the provided information of the
/// the block and the block's parent block.
///
/// # Arguments
///
/// * 'parent_decayed_total_time' - the total decayed time that has passed since
///   the genesis block for the parent block.
/// * 'parent_decayed_total_target' - the total decayed target of all blocks up
///   until and including the parent block.
/// * 'current_target' - the target of the current block.
/// * 'parent_height' - the height of the parent block.
/// * 'parent_timestamp' - the BlockTimestamp of the parent block.
/// * 'cfg' - a collection of consensus-specific configuration params.
pub fn child_target(
    parent_decayed_total_time: u64,
    parent_decayed_total_target: Target,
    current_target: Target,
    parent_height: BlockHeight,
    parent_timestamp: BlockTimestamp,
    cfg: &ConsensusConfig,
) -> Target {
    // Some basic assertions to avoid multiplications and assertions by 0 later.
    assert_ne!(cfg.block_frequency, 0);
    assert_ne!(cfg.max_block_shift, 0);
    assert_ne!(cfg.max_block_shift, 0);
    assert_ne!(cfg.genesis_timestamp, 0);
    assert!(!cfg.max_rise.is_zero());
    assert!(!cfg.max_drop.is_zero());

    // Convenience vars.
    let max_rise = &cfg.max_rise;
    let max_drop = &cfg.max_drop;
    let max_shift = cfg.max_block_shift;
    let block_frequency = cfg.block_frequency;
    let genesis_timestamp = cfg.genesis_timestamp;

    // Make sure parentDecayedTotalTime is at least 1 second to avoid divisions by 0 later.
    let mut ptt = parent_decayed_total_time;
    if ptt < 1 {
        ptt = 1;
    }

    // Compute the expected time of the parent block.
    let expected_parent_time = block_frequency * parent_height + genesis_timestamp;

    // Compute delta between the expectation and the actual timestamp.
    let delta = expected_parent_time as i64 - parent_timestamp as i64;

    // The difficulty shift is 1/10e6th of the square.
    let mut delta_square = delta.pow(2);
    if delta < 0 {
        delta_square *= -1;
    }
    let shift = delta_square / 10e6 as i64; // 10e3 second delta leads to 10 second shift

    // Apply the shift to the frequency to get the target for the next block.
    let mut target_block_time = block_frequency as i64 + shift;

    // Clamp the target block time to 1/3 and 3x of the frequency.
    if target_block_time < (block_frequency / max_shift) as i64 {
        target_block_time = (block_frequency / max_shift) as i64;
    }
    if target_block_time > (block_frequency * max_shift) as i64 {
        target_block_time = (block_frequency * max_shift) as i64;
    }

    // target_block_time can never be zero because block_frequency and max_shift
    // will never be zero either.
    assert!(!target_block_time.is_zero());

    // Compute the visible hashrate using the total time that has passed for
    // a given parent as well as the cumulative difficulty.
    let visible_hashrate = parent_decayed_total_target.difficulty() / BigInt::from(ptt);

    // Hashrate can never be zero because the difficulty of the
    // parent_decayed_total_target can never be zero.
    assert!(!visible_hashrate.is_zero());

    // Determine the new target by multiplying the visible hashrate by the
    // target block time.
    let mut new_target = Target::from_bigint(
        ROOT_DEPTH.bigint() / (visible_hashrate * BigInt::from(target_block_time)),
    );

    // Clamp the newTarget to a 0.4% difficulty adjustment.
    let max_new_target = current_target * max_rise; // Max = difficulty increase (target decrease)
    let min_new_target = current_target * max_drop; // Min = difficulty decrease (target increase)
    if new_target < max_new_target {
        new_target = max_new_target
    }
    if new_target > min_new_target {
        // This can only possibly trigger if the BlockFrequency is less than 3
        // seconds, but during testing it is 1 second.
        new_target = min_new_target
    }

    new_target
}

/// Decay the total target.
///
/// Formula:
/// newDecayedTotalTarget = prevDecayedTotalTarget.MulDifficulty(big.NewRat(types.OakDecayNum, types.OakDecayDenom))
///                         .AddDifficulties(targetOfNewBlock)
pub fn decay_total_target(
    prev_decayed_total_target: Target,
    target_of_new_block: Target,
) -> Target {
    decay_total_target_custom(
        prev_decayed_total_target,
        target_of_new_block,
        &OAK_DECAY_NUMERATOR,
        &OAK_DECAY_DENOMINATOR,
    )
}

fn decay_total_target_custom(
    prev_decayed_total_target: Target,
    target_of_new_block: Target,
    decay_numerator: &BigInt,
    decay_denominator: &BigInt,
) -> Target {
    prev_decayed_total_target
        * &BigRational::new(decay_numerator.clone(), decay_denominator.clone())
        + target_of_new_block
}

/// Decay the total time.
///
/// Formula:
/// newDecayedTotalTime = (prevDecayedTotalTime * OakDecayNumerator / OakDecayDenominator) +
///                       (newTimestamp - prevTimestamp)
pub fn decay_total_time(
    prev_decayed_total_time: u64,
    new_timestamp: u64,
    prev_timestamp: u64,
) -> Result<u64, TryFromBigIntError<BigInt>> {
    decay_total_time_custom(
        prev_decayed_total_time,
        new_timestamp,
        prev_timestamp,
        &OAK_DECAY_NUMERATOR,
        &OAK_DECAY_DENOMINATOR,
    )
}

fn decay_total_time_custom(
    prev_decayed_total_time: u64,
    new_timestamp: u64,
    prev_timestamp: u64,
    decay_numerator: &BigInt,
    decay_denominator: &BigInt,
) -> Result<u64, TryFromBigIntError<BigInt>> {
    (prev_decayed_total_time * decay_numerator / decay_denominator
        // NOTE: we may get a negative delta here because the timestamps are
        // not in chronological order. This is fine and the formula will
        // still work.
        + (BigInt::from(new_timestamp) - BigInt::from(prev_timestamp)))
    .try_into()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::consensus::time::current_timestamp;
    use anyhow;
    use pretty_assertions::assert_eq;

    // convenience function for creating a consensus config for testing.
    fn test_cfg() -> ConsensusConfig {
        ConsensusConfig {
            max_rise: BigRational::new(BigInt::from(1004), BigInt::from(1000)),
            max_drop: BigRational::new(BigInt::from(1000), BigInt::from(1004)),
            max_block_shift: 3,
            block_frequency: 600, // 10 mins
            genesis_timestamp: current_timestamp() + 1000,
        }
    }

    // convenience function for creating a root target for testing.
    fn test_root_target() -> Target {
        Target::from([
            0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0,
        ])
    }

    /// unit tests the child_target method.
    #[test]
    fn test_child_target() {
        // Declare the consensus config.
        let cfg = test_cfg();
        let root_target = test_root_target();

        // Start with some values that are normal, resulting in no change in target.
        let parent_height: BlockHeight = 100;

        // The total time and total target will be set to 100 uncompressed blocks.
        // Though the actual algorithm is compressing the times to achieve an
        // exponential weighting, this test only requires that the visible hashrate
        // be measured as equal to the root target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        let parent_timestamp = cfg.genesis_timestamp + cfg.block_frequency * parent_height;
        let parent_target = root_target;

        // newTarget should match the root target, as the hashrate and blocktime all
        // match the existing target - there should be no reason for adjustment.
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );

        // New target should be barely moving. Some imprecision may cause slight
        // adjustments, but the total difference should be less than 0.01%.
        let max_new_target =
            parent_target * &BigRational::new(BigInt::from(10000), BigInt::from(10001));
        let min_new_target =
            parent_target * &BigRational::new(BigInt::from(10001), BigInt::from(10000));
        assert!(new_target <= max_new_target);
        assert!(new_target >= min_new_target);

        // Set the total time such that the difficulty needs to be adjusted down.
        // Shifter clamps and adjustment clamps will both be in effect.
        let parent_height: BlockHeight = 100;
        // Set the visible hashrate to root_target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp far in the future, triggering the shifter to increase
        // the block time to the point that the shifter clamps activate.
        let parent_timestamp =
            cfg.genesis_timestamp + (cfg.block_frequency * parent_height) + 500 * 10_u64.pow(6);
        // Set the target to root_target, causing the max difficulty adjustment
        // clamp to be in effect.
        let parent_target = root_target;
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        assert!(parent_target.difficulty() > new_target.difficulty());
        // Check that the difficulty decreased by the maximum amount.
        let min_new_target = parent_target * &cfg.max_drop;
        assert_eq!(min_new_target, new_target);

        // Set the total time such that the difficulty needs to be adjusted down.
        // Neither the shiftor clamps nor the adjustor clamps will be in effect.
        let parent_height: BlockHeight = 100;
        // Set the visible hashrate to root_target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp in the future, but little enough in the future that
        // neither the shifter clamp nor the adjustment clamp will trigger.
        let parent_timestamp = cfg.genesis_timestamp + (cfg.block_frequency * parent_height) + 5000;
        // Set the target to root_target.
        let parent_target = root_target;
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        // Check that the difficulty decreased, but not by the max amount.
        let min_new_target = parent_target * &cfg.max_drop;
        assert!(parent_target.difficulty() > new_target.difficulty());
        assert!(min_new_target.difficulty() < new_target.difficulty());

        // Set the total time such that the difficulty needs to be adjusted up.
        // Neither the shift nor clamps nor the adjustor clamps will be in effect.
        let parent_height: BlockHeight = 100;
        // Set the visible hashrate to root_target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp in the past, but little enough in the future that
        // neither the shifter clamp nor the adjustment clamp will trigger.
        let parent_timestamp = cfg.genesis_timestamp + (cfg.block_frequency * parent_height) - 5000;
        // Set the target to root_target.
        let parent_target = root_target;
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        // Check that the difficulty increased, but not by the max amount.
        let max_new_target = parent_target * &cfg.max_rise;
        assert!(parent_target.difficulty() < new_target.difficulty());
        assert!(max_new_target.difficulty() > new_target.difficulty());

        // Set the total time such that the difficulty needs to be adjusted down.
        // Adjustor clamps will be in effect, shifter clamps will not be in effect.
        let parent_height: BlockHeight = 100;
        // Set the visible hashrate to root_target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp in the future, but little enough in the future that
        // neither the shifter clamp nor the adjustment clamp will trigger.
        let parent_timestamp =
            cfg.genesis_timestamp + (cfg.block_frequency * parent_height) + 10000;
        // Set the target to root_target.
        let parent_target = root_target;
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        // Check that the difficulty decreased, but not by the max amount.
        let min_new_target = parent_target * &cfg.max_drop;
        assert!(parent_target.difficulty() > new_target.difficulty());
        assert_eq!(min_new_target, new_target);

        // Set the total time such that the difficulty needs to be adjusted up.
        // Adjustor clamps will be in effect, shifter clamps will not be in effect.
        let parent_height: BlockHeight = 100;
        // Set the visible hashrate to types.RootTarget per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp in the past, but little enough in the future that
        // neither the shifter clamp nor the adjustment clamp will trigger.
        let parent_timestamp =
            cfg.genesis_timestamp + (cfg.block_frequency * parent_height) - 10000;
        // Set the target to root_target.
        let parent_target = root_target;
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        // Check that the difficulty increased, but not by the max amount.
        let max_new_target = parent_target * &cfg.max_rise;
        assert!(parent_target.difficulty() < new_target.difficulty());
        assert_eq!(max_new_target, new_target);

        // Set the total time such that the difficulty needs to be adjusted down.
        // Shifter clamps will be in effect, adjustor clamps will not be in effect.
        let parent_height: BlockHeight = 100;
        // Set the visible hashrate to root_target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp in the future, but little enough in the future that
        // neither the shifter clamp nor the adjustment clamp will trigger.
        let parent_timestamp =
            cfg.genesis_timestamp + (cfg.block_frequency * parent_height) + 500 * 10_u64.pow(6);
        // Set the target to root_target / max_block_shift.
        let parent_target =
            root_target * &BigRational::new(BigInt::from(1), BigInt::from(cfg.max_block_shift));
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        // New target should be barely moving. Some imprecision may cause slight
        // adjustments, but the total difference should be less than 0.01%.
        let max_new_target =
            parent_target * &BigRational::new(BigInt::from(10000), BigInt::from(10001));
        let min_new_target =
            parent_target * &BigRational::new(BigInt::from(10001), BigInt::from(10000));
        assert!(new_target < max_new_target);
        assert!(new_target > min_new_target);

        // Set the total time such that the difficulty needs to be adjusted up.
        // Shifter clamps will be in effect, adjustor clamps will not be in effect.
        let parent_height: BlockHeight = 10_u64.pow(6);
        // Set the visible hashrate to root_target per block.
        let parent_decayed_total_time = cfg.block_frequency * parent_height;
        let parent_decayed_total_target =
            root_target * &BigRational::from_integer(BigInt::from(parent_height));
        // Set the timestamp in the past, but little enough in the past that
        // neither the shifter clamp nor the adjustment clamp will trigger.
        let parent_timestamp =
            cfg.genesis_timestamp + (cfg.block_frequency * parent_height) - 500 * 10_u64.pow(6);
        // Set the target to root_target * max_block_shift.
        let parent_target =
            root_target * &BigRational::from_integer(BigInt::from(cfg.max_block_shift));
        let new_target = child_target(
            parent_decayed_total_time,
            parent_decayed_total_target,
            parent_target,
            parent_height,
            parent_timestamp,
            &cfg,
        );
        // New target should be barely moving. Some imprecision may cause slight
        // adjustments, but the total difference should be less than 0.01%.
        let max_new_target =
            parent_target * &BigRational::new(BigInt::from(10000), BigInt::from(10001));
        let min_new_target =
            parent_target * &BigRational::new(BigInt::from(10001), BigInt::from(10000));
        assert!(new_target < max_new_target);
        assert!(new_target > min_new_target);
    }

    // This test runs child_target with very edge-casey arguments to try and
    // produce divide-by-zero panics.
    #[test]
    fn test_divide_by_zero_checks() {
        let cfg = test_cfg();

        // This should not produce an underflow even with '0' for the total
        // time.
        let _new_target = child_target(0, ROOT_DEPTH, ROOT_DEPTH, 100, cfg.genesis_timestamp, &cfg);
    }

    // Test that deriving block totals results in known, hard-coded values
    // (taken from siad). Based on `TestStoreBlockTotals` in siad.
    #[test]
    fn test_derive_block_totals() -> anyhow::Result<()> {
        // Hard-code expected values from siad.
        #[rustfmt::skip]
        const EXPECTED_VALUES: &[(u64, [u8; 32])] = &[
            (600, [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 255, 255, 255, 255]),
            (1197, [0, 0, 0, 0, 128, 82, 32, 20, 72, 5, 60, 89, 249, 152, 3, 231, 168, 22, 89, 41, 3, 108, 213, 116, 217, 20, 37, 66, 49, 145, 56, 124]),
            (1791, [0, 0, 0, 0, 85, 194, 236, 204, 186, 228, 247, 35, 5, 218, 114, 128, 13, 151, 154, 221, 139, 196, 41, 130, 138, 140, 97, 35, 171, 69, 127, 255]),
            (2382, [0, 0, 0, 0, 64, 123, 100, 173, 250, 14, 183, 116, 8, 73, 223, 91, 51, 249, 242, 166, 2, 210, 45, 145, 229, 228, 81, 234, 228, 241, 247, 61]),
            (2970, [0, 0, 0, 0, 51, 182, 237, 210, 176, 138, 150, 160, 201, 24, 198, 121, 223, 249, 168, 14, 98, 183, 21, 17, 92, 177, 88, 129, 160, 210, 229, 159]),
            (3555, [0, 0, 0, 0, 43, 51, 255, 153, 18, 22, 158, 71, 229, 31, 239, 1, 86, 163, 142, 96, 190, 38, 168, 138, 229, 5, 10, 136, 244, 215, 197, 75]),
            (4137, [0, 0, 0, 0, 37, 31, 168, 151, 49, 58, 169, 230, 102, 51, 9, 164, 22, 7, 133, 114, 51, 39, 149, 224, 15, 6, 116, 114, 111, 175, 106, 83]),
            (4716, [0, 0, 0, 0, 32, 144, 112, 24, 20, 59, 25, 104, 69, 160, 234, 117, 35, 96, 187, 143, 34, 95, 23, 0, 100, 179, 77, 197, 249, 238, 28, 214]),
            (5292, [0, 0, 0, 0, 29, 4, 161, 69, 102, 162, 42, 81, 8, 190, 208, 125, 4, 1, 113, 141, 149, 66, 151, 24, 46, 114, 235, 184, 194, 90, 44, 99]),
            (5865, [0, 0, 0, 0, 26, 46, 105, 56, 38, 162, 80, 209, 52, 153, 0, 118, 207, 92, 45, 197, 253, 230, 251, 93, 202, 67, 137, 110, 204, 111, 253, 206]),
            (6435, [0, 0, 0, 0, 23, 220, 65, 186, 104, 64, 90, 27, 87, 156, 213, 15, 155, 59, 136, 175, 209, 24, 71, 71, 216, 32, 199, 242, 165, 34, 144, 90]),
            (7002, [0, 0, 0, 0, 21, 237, 38, 168, 54, 231, 131, 161, 228, 218, 5, 1, 73, 150, 154, 110, 53, 140, 142, 106, 75, 126, 82, 101, 93, 146, 12, 25]),
            (7566, [0, 0, 0, 0, 20, 74, 60, 134, 54, 66, 168, 24, 203, 241, 31, 206, 41, 151, 76, 233, 75, 228, 102, 142, 95, 191, 231, 245, 96, 121, 79, 197]),
            (8128, [0, 0, 0, 0, 18, 227, 47, 179, 34, 184, 41, 58, 166, 65, 128, 39, 158, 48, 57, 165, 41, 24, 191, 115, 186, 245, 222, 253, 127, 31, 31, 48]),
            (8687, [0, 0, 0, 0, 17, 172, 7, 31, 30, 128, 254, 158, 242, 171, 223, 20, 115, 240, 220, 200, 97, 231, 169, 8, 40, 137, 208, 147, 69, 116, 202, 241]),
            (9243, [0, 0, 0, 0, 16, 155, 199, 254, 65, 215, 19, 152, 232, 179, 120, 62, 176, 130, 176, 234, 30, 111, 226, 175, 130, 143, 121, 165, 187, 192, 45, 195]),
            (9796, [0, 0, 0, 0, 15, 171, 148, 105, 82, 154, 201, 151, 166, 108, 179, 73, 165, 226, 128, 135, 160, 88, 248, 216, 167, 190, 202, 58, 109, 116, 55, 27]),
            (10347, [0, 0, 0, 0, 14, 214, 21, 30, 62, 193, 76, 98, 101, 69, 211, 196, 235, 142, 84, 227, 240, 9, 143, 222, 161, 135, 201, 254, 176, 87, 231, 110]),
            (10895, [0, 0, 0, 0, 14, 23, 18, 178, 246, 213, 243, 17, 238, 84, 79, 249, 239, 122, 204, 95, 53, 170, 252, 52, 128, 209, 141, 129, 251, 189, 76, 109]),
            (11440, [0, 0, 0, 0, 13, 107, 45, 159, 109, 84, 208, 141, 41, 153, 75, 20, 242, 61, 226, 137, 183, 56, 192, 80, 146, 206, 187, 41, 169, 125, 236, 150]),
            (11982, [0, 0, 0, 0, 12, 207, 170, 215, 3, 235, 100, 47, 216, 81, 72, 247, 159, 90, 0, 46, 171, 184, 243, 41, 16, 223, 4, 237, 37, 23, 204, 233]),
        ];

        // Set the constants to match the real-network constants.
        let test_block_frequency = 600;
        let test_oak_decay_numerator: BigInt = 995.into();
        let test_oak_decay_denominator: BigInt = 1_000.into();
        let test_root_target = Target([
            0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0,
        ]);

        let mut total_time = 0;
        let mut current_timestamp = 0;
        let current_target = test_root_target;
        let mut total_target = ROOT_DEPTH;

        #[allow(clippy::needless_range_loop)] // Clippy bug.
        for i in 0..8000 {
            let parent_timestamp = current_timestamp;
            current_timestamp += test_block_frequency;
            total_time = decay_total_time_custom(
                total_time,
                current_timestamp,
                parent_timestamp,
                &test_oak_decay_numerator,
                &test_oak_decay_denominator,
            )?;
            total_target = decay_total_target_custom(
                total_target,
                current_target,
                &test_oak_decay_numerator,
                &test_oak_decay_denominator,
            );

            // Check that the values match the expected hard-coded values.
            if i < EXPECTED_VALUES.len() {
                assert_eq!(total_time, EXPECTED_VALUES[i].0);
                assert_eq!(total_target, Target(EXPECTED_VALUES[i].1));
            }
        }

        // Do a final iteration, but keep the old totals. After 8000 iterations,
        // the totals should no longer be changing, yet they should be hundreds
        // of times larger than the original values.
        let parent_timestamp = current_timestamp;
        current_timestamp += test_block_frequency;
        let new_total_time = decay_total_time_custom(
            total_time,
            current_timestamp,
            parent_timestamp,
            &test_oak_decay_numerator,
            &test_oak_decay_denominator,
        )?;
        let new_total_target = decay_total_target_custom(
            total_target,
            current_target,
            &test_oak_decay_numerator,
            &test_oak_decay_denominator,
        );

        assert!(
            new_total_time == total_time
                && new_total_target.difficulty() == total_target.difficulty()
        );
        assert!(
            new_total_time >= test_block_frequency * 199
                && new_total_time <= test_block_frequency * 205
        );
        assert!(
            new_total_target.difficulty() >= test_root_target.difficulty() * 199
                && new_total_target.difficulty() <= test_root_target.difficulty() * 205
        );

        Ok(())
    }
}
