use crate::consensus::ACCOUNT_UPDATE_LIMIT;
use num::{BigRational, ToPrimitive};

/// Determines the target for the number of account updates within a block. Set
/// to 50% of the limit.
#[allow(unused)]
const TARGET_SATURATION_DENOMINATOR: u64 = 2;

/// 25% above and below the target there is a dead-zone. As long as the number of
/// account updates stays within the dead-zone, the base fee isn't adjusted.
#[allow(unused)]
const DEAD_ZONE_DENOMINATOR: u64 = 4;

/// The numerator of the fee step size computation.
#[allow(unused)]
const FEE_STEP_SIZE_NUMERATOR: u64 = 105;

/// The denominator of the fee step size computation.
#[allow(unused)]
const FEE_STEP_SIZE_DENOMINATOR: u64 = 100;

/// Algorithm to determine base fee per output for the next block.
#[allow(unused)]
pub fn base_fee_step_for_child(
    last_base_fee_step: u16,
    account_updates: u64,
    emergency_mode: bool,
) -> u16 {
    // Adjust the account update limit in emergency mode.
    let account_update_limit = if emergency_mode {
        ACCOUNT_UPDATE_LIMIT / 2 // cut limit in half during emergency mode.
    } else {
        ACCOUNT_UPDATE_LIMIT // use full limit
    };

    // Compute the target floor and ceil.
    let account_update_target = account_update_limit / TARGET_SATURATION_DENOMINATOR;
    let target_floor = account_update_target - (account_update_limit / DEAD_ZONE_DENOMINATOR);
    let target_ceil = account_update_target + (account_update_limit / DEAD_ZONE_DENOMINATOR);

    // Adjust the base_fee_step.
    if account_updates > target_ceil && last_base_fee_step < u16::MAX {
        last_base_fee_step + 1 // raise step
    } else if account_updates < target_floor && last_base_fee_step > 0 {
        last_base_fee_step - 1 // lower step
    } else {
        last_base_fee_step // keep same step
    }
}

/// Computes the baseFee for a given fee_step.
#[allow(unused)]
fn base_fee_from_step_size(fee_step: u16) -> u64 {
    // Compute FEE_STEP_SIZE_NOMINATOR^(new_base_fee_step) /
    // FEE_STEP_SIZE_DENOMINATOR while also checking for an overflow. If an
    // overflow happens, we return the highest potential value for a u64.
    let fee_step_size = BigRational::new(
        FEE_STEP_SIZE_NUMERATOR.into(),
        FEE_STEP_SIZE_DENOMINATOR.into(),
    );
    fee_step_size
        .pow(fee_step.into())
        .to_u64()
        .unwrap_or(u64::MAX)
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    /// Unit test for base_fee_step_for_child.
    #[test]
    fn test_base_fee_step_for_child() {
        let test = |emergency_mode: bool| {
            let account_update_limit = if emergency_mode {
                ACCOUNT_UPDATE_LIMIT / 2
            } else {
                ACCOUNT_UPDATE_LIMIT
            };
            let last_base_fee_step = 10_u16;
            let account_update_target = account_update_limit / TARGET_SATURATION_DENOMINATOR;
            let target_floor =
                account_update_target - (account_update_limit / DEAD_ZONE_DENOMINATOR);
            let target_ceil =
                account_update_target + (account_update_limit / DEAD_ZONE_DENOMINATOR);

            // Test hitting the target, the floor and the ceil. None of those should
            // change the fee.
            assert_eq!(
                last_base_fee_step,
                base_fee_step_for_child(last_base_fee_step, account_update_target, emergency_mode),
                "em: {}",
                emergency_mode
            );
            assert_eq!(
                last_base_fee_step,
                base_fee_step_for_child(last_base_fee_step, target_floor, emergency_mode),
                "em: {}",
                emergency_mode
            );
            assert_eq!(
                last_base_fee_step,
                base_fee_step_for_child(last_base_fee_step, target_ceil, emergency_mode),
                "em: {}",
                emergency_mode
            );

            // If the number of accounts is below the floor, the fee step should go down.
            assert_eq!(
                last_base_fee_step - 1,
                base_fee_step_for_child(last_base_fee_step, target_floor - 1, emergency_mode),
                "em: {}",
                emergency_mode
            );
            assert_eq!(
                last_base_fee_step - 1,
                base_fee_step_for_child(last_base_fee_step, 0, emergency_mode),
                "em: {}",
                emergency_mode
            );

            // If the number of accounts is above the ceil, the fee step should go
            // up.
            assert_eq!(
                last_base_fee_step + 1,
                base_fee_step_for_child(last_base_fee_step, target_ceil + 1, emergency_mode),
                "em: {}",
                emergency_mode
            );
            assert_eq!(
                last_base_fee_step + 1,
                base_fee_step_for_child(last_base_fee_step, u64::MAX, emergency_mode),
                "em: {}",
                emergency_mode
            );

            // Test edge case where the target is below the floor but the step is already 0.
            assert_eq!(
                0,
                base_fee_step_for_child(0, target_floor - 1, emergency_mode),
                "em: {}",
                emergency_mode
            );

            // Test edge case where the target is above the floor but the step is already u16::MAX.
            assert_eq!(
                u16::MAX,
                base_fee_step_for_child(u16::MAX, target_floor + 1, emergency_mode),
                "em: {}",
                emergency_mode
            );
        };

        // Run tests once without and once with emergency mode enabled.
        test(false);
        test(true);
    }

    /// Unit test for base_fee_from_step_size.
    #[test]
    fn test_base_fee_from_step_size() {
        let base_fee = |fee_step: u16| -> u64 {
            let step_size = BigRational::new(105.into(), 100.into());
            step_size.pow(fee_step.into()).to_u64().unwrap()
        };

        // Test for regular inputs.
        assert_eq!(base_fee(1), base_fee_from_step_size(1));
        assert_eq!(base_fee(2), base_fee_from_step_size(2));

        // Test for lowest possible input 0.
        assert_eq!(base_fee(0), base_fee_from_step_size(0));

        // Test for lowest possible input u16::MAX. This will overflow the u64
        // and result in u64::MAX.
        assert_eq!(u64::MAX, base_fee_from_step_size(u16::MAX));
    }
}
