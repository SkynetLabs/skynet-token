//! This modules describes the overall structure and encoding of the Blockchain.
//! This includes Blocks, Transactions, Transaction Outputs and proofs for
//! spending from accounts.

pub mod block;
mod consts;
mod currency;
mod difficulty;
mod emergency_mode;
mod fee;
mod lowpow;
mod merkletree;
pub mod target;
#[cfg(test)]
mod test_miner;
pub mod time;

/// The limit to the number of account updates a block may contain.
const ACCOUNT_UPDATE_LIMIT: u64 = 1 << 16;
