//! There is a target amount of activity every block. If the total amount of
//! activity in a block exceeds the target, the base fee is increased. If the
//! total amount of activity in a block is less than the target, the base fee is
//! decreased.

use crate::encoding::{Decode, Encode, Result};
#[cfg(test)]
use crate::test_utils::Random;
use num::traits::Inv;
use num::{BigRational, Zero};
use num_bigint::{BigInt, Sign};

/// ROOT_DEPTH describes the largest target possible. Since that is the easiest
/// target to mine a smaller block ID for, it is also the target with the
/// smallest difficulty possible.
pub const ROOT_DEPTH: Target = Target([255; 32]);

/// TARGET_SIZE is the size of the target in bytes. It aligns with the hash size
/// used for the PoW and as a result the block's ID size.
const TARGET_SIZE: usize = 32;

/// A Target is a hash that a block's ID must be "less than" in order for the
/// block to be considered valid. Miners vary the block's 'Nonce' field in order
/// to brute-force such an ID. The inverse of a Target is called the
/// "difficulty," because it is proportional to the amount of time required to
/// brute-force the Target.
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd)]
pub struct Target(pub [u8; TARGET_SIZE]);

impl Target {
    /// Returns the difficulty of the target that it is called on.
    /// It is given by `difficulty = ROOT_DEPTH / target`
    #[allow(dead_code)]
    pub fn difficulty(&self) -> BigInt {
        let denom = self.bigint();
        if denom.is_zero() {
            return ROOT_DEPTH.bigint();
        }
        ROOT_DEPTH.bigint() / denom
    }

    /// Turns the target into a number.
    pub fn bigint(&self) -> BigInt {
        BigInt::from_bytes_be(Sign::Plus, &self.0)
    }

    /// Creates a new target from a byte array.
    #[allow(dead_code)]
    pub fn from(t: [u8; TARGET_SIZE]) -> Target {
        Target(t)
    }

    /// Creates a target from a number.
    ///
    /// # Arguments
    /// * `i` - The BigInt to create the target from. Must be >= 0. Panics
    /// otherwise.
    pub fn from_bigint(i: BigInt) -> Self {
        if i.sign() == Sign::Minus {
            panic!("negative target")
        }
        if i.bits() > 8 * TARGET_SIZE as u64 {
            return ROOT_DEPTH;
        }
        let mut t = Self::default();
        let b = i.to_bytes_be();
        let offset = t.0.len() - b.1.len();
        t.0[offset..].copy_from_slice(&b.1);
        t
    }

    /// Creates a target from a rational.
    ///
    /// # Arguments
    /// * `r` - The BigRational to create the target from. Must be >= 0. Panics
    /// otherwise.
    pub fn from_rat(r: BigRational) -> Self {
        Target::from_bigint(r.to_integer())
    }

    /// Computes the inverse of a Target.
    fn inv(&self) -> BigRational {
        self.rat().inv()
    }

    /// Turns the Target into a BigRational.
    fn rat(&self) -> BigRational {
        BigRational::from_integer(self.bigint())
    }
}

impl std::ops::Add for Target {
    type Output = Self;

    /// Adds two targets by adding up their difficulties.
    fn add(self, rhs: Self) -> Self::Output {
        let sum_difficulties = self.inv() + rhs.inv();
        Target::from_rat(sum_difficulties.inv())
    }
}

impl std::ops::Sub for Target {
    type Output = Self;

    /// Subtracts one target from another by subtracting its difficulty.
    fn sub(self, rhs: Self) -> Self::Output {
        let sub_difficulties = self.inv() - rhs.inv();
        Target::from_rat(sub_difficulties.inv())
    }
}

impl std::ops::Mul<&BigRational> for Target {
    type Output = Self;

    /// Multiplies a target's difficulty with another difficulty given by a
    /// BigRational. By inverting the target first, we get its difficulty, then
    /// we multiply it with the BigRational and finally we revert it back into a
    /// target.
    fn mul(self, rhs: &BigRational) -> Self::Output {
        Target::from_rat((rhs * self.inv()).inv())
    }
}

impl Default for Target {
    fn default() -> Self {
        Target([0; TARGET_SIZE])
    }
}

impl Encode for Target {
    const SIZE: usize = TARGET_SIZE;

    fn encode(&self, bytes: &mut [u8]) {
        bytes.copy_from_slice(&self.0);
    }
}
impl Decode for Target {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(Self(bytes.try_into()?))
    }
}

#[cfg(test)]
impl Random for Target {
    fn random() -> Self {
        Self(<[u8; 32]>::random())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::encoding::{decode, encode};
    use pretty_assertions::assert_eq;

    /// Creates a new target for testing where the last byte of the target is
    /// set to `b`.
    fn new_test_target(b: u8) -> Target {
        let mut target = Target::default();
        target.0[TARGET_SIZE - 1] = b;
        target
    }

    #[test]
    fn test_add() {
        let target3 = new_test_target(3);
        let target5 = new_test_target(5);
        let target10 = new_test_target(10);

        // Adding 2 targets should add their difficulties.
        let expect5 = target10 + target10;
        assert_eq!(expect5, target5);

        let expect3 = target10 + target5;
        assert_eq!(expect3, target3);
    }

    /// Tests the implementation of Add on the target.
    #[test]
    fn test_bigint() {
        let target = new_test_target(1);
        assert_eq!(target.bigint(), BigInt::from(1));
    }

    /// Tests the implementation of default on the target.
    #[test]
    fn test_default() {
        let target = Target::default();
        assert_eq!(target.0, [0; TARGET_SIZE])
    }

    /// Tests the implementation of PartialOrd on the target.
    #[test]
    #[allow(clippy::eq_op)]
    fn test_ordering() {
        let target1 = new_test_target(1);
        let target2 = new_test_target(2);

        assert!(target1 < target2);
        assert_eq!(target2, target2);
        assert!(target2 > target1);
    }

    // Tests the implementation of difficulty on the target.
    #[test]
    fn test_difficulty() {
        let target1 = Target::default();
        let target2 = new_test_target(1);
        let target3 = new_test_target(2);

        let exp_difficulty1 = ROOT_DEPTH.bigint();
        let exp_difficulty2 = ROOT_DEPTH.bigint();
        let exp_difficulty3 = ROOT_DEPTH.bigint() / BigInt::from(2);

        assert_eq!(target1.difficulty(), exp_difficulty1);
        assert_eq!(target2.difficulty(), exp_difficulty2);
        assert_eq!(target3.difficulty(), exp_difficulty3);
    }

    /// Tests creating a target from a BigInt.
    #[test]
    fn test_from_bigint() {
        let target = new_test_target(5);
        assert_eq!(Target::from_bigint(BigInt::from(5)), target);
    }

    /// Tests inverting a target.
    #[test]
    fn test_inverse() {
        let target = new_test_target(2);
        let inv = target.inv();
        assert_eq!(*inv.numer(), BigInt::from(1));
        assert_eq!(*inv.denom(), BigInt::from(2));
    }

    /// Tests multiplying a target with a BigRational.
    #[test]
    fn test_mul() {
        let target2 = new_test_target(2);
        let target6 = new_test_target(6);
        let target10 = new_test_target(10);
        let target14 = new_test_target(14);
        let target20 = new_test_target(20);

        // Multiplying the difficulty of a target at '10' by 5 will yield a target
        // of '2'. Similar math follows for the remaining checks.
        assert_eq!(
            target10 * &BigRational::new(BigInt::from(5), BigInt::from(1)),
            target2
        );
        assert_eq!(
            target10 * &BigRational::new(BigInt::from(3), BigInt::from(2)),
            target6
        );
        assert_eq!(
            target10 * &BigRational::new(BigInt::from(7), BigInt::from(10)),
            target14
        );
        assert_eq!(
            target10 * &BigRational::new(BigInt::from(1), BigInt::from(2)),
            target20
        );
    }

    #[test]
    fn test_rat() {
        let target = new_test_target(3);
        let r = target.rat();
        assert_eq!(*r.numer(), BigInt::from(3));
        assert_eq!(*r.denom(), BigInt::from(1));
    }

    /// Makes sure that an overflow is handled correctly when creating a target
    /// from a BigInt.
    #[test]
    fn test_from_bigint_overflow() {
        let large_int = BigInt::from(1) << 260;
        assert_eq!(Target::from_bigint(large_int), ROOT_DEPTH);
    }

    /// Makes sure creating a target from a negative BigInt panics.
    #[test]
    #[should_panic]
    fn test_from_bigint_underflow() {
        Target::from_bigint(BigInt::from(-3));
    }

    /// Makes sure creating a target from a negative BigRational panics.
    #[test]
    #[should_panic]
    fn test_from_rat_underflow() {
        Target::from_rat(BigRational::from_integer(BigInt::from(-3)));
    }

    /// Makes sure subtracting a lower target from a higher one doesn't work.
    #[test]
    #[should_panic]
    fn test_sub_underflow() {
        let target1 = new_test_target(1);
        let target2 = new_test_target(2);
        let _ = target2 - target1;
    }

    // Hard-code the expected encoding.
    #[test]
    fn should_encode_target() {
        const TEST_TARGET: Target = Target([
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
            25, 26, 27, 28, 29, 30, 31, 32,
        ]);

        let encoded = encode(&TEST_TARGET);
        assert_eq!(encoded, TEST_TARGET.0);
    }

    #[test]
    fn should_encode_and_decode_target() -> Result<()> {
        let value = Target::default();
        assert_eq!(decode::<Target>(&encode(&value))?, value);

        let value = Target::random();
        assert_eq!(decode::<Target>(&encode(&value))?, value);

        Ok(())
    }

    #[test]
    fn should_generate_random_target() {
        assert_ne!(Target::random(), Target::random());
    }
}
