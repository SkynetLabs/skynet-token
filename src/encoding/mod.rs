//! Module for encoding/decoding of SKT data.
//!
//! Usually you will just use [`encode`] or [`decode`] from this module. They
//! work on any type that implements [`Encode`]. If you make your own type, you
//! can implement `Encode` on it and you can then call `encode` and `decode` on
//! values of that type.
//!
//! This module also exports the [`Encoder`] and [`Decoder`] utility structs
//! which may be helpful in implementing [`Encode`].

#![allow(dead_code)]

mod common;
mod error;

pub use error::{Error, Result};

/// Encode a value. Allocates just enough space for the resulting byte array.
/// You just have to `impl Encode` for the type of the value value, and you can
/// call this function on it.
pub fn encode<T: Encode>(value: &T) -> Vec<u8> {
    let mut vec = vec![0; T::SIZE];
    {
        // Make a new scope for the encoder to avoid lifetime issue.
        let mut encoder = Encoder::with_bytes(&mut vec);
        encoder.encode_field(value);
    }
    vec
}

/// Decode some bytes into a value of type `T`. You just have to implement
/// [`Encode`] for the type of the value, and you can call this function.
pub fn decode<T: Decode>(bytes: &[u8]) -> Result<T> {
    let mut decoder = Decoder::with_bytes(bytes);
    let result: T = decoder.decode_field()?;
    decoder.finalize()?;
    Ok(result)
}

/// Struct that is helpful for implementing [`Encode::encode`]. Initialize it
/// with the initial allocated bytes, of the required size, using
/// [`Encoder::with_bytes`], and encode each field sequentially with
/// [`encode_field`].
pub struct Encoder<'a> {
    bytes: &'a mut [u8],
    index: usize,
}

impl<'a> Encoder<'a> {
    /// Creates an `Encoder` with the given byte buffer.
    pub fn with_bytes(bytes: &'a mut [u8]) -> Self {
        Self { bytes, index: 0 }
    }

    /// Encodes a value of type `T` and appends it to the Encoder's byte buffer.
    pub fn encode_field<T: Encode>(&mut self, field: &T) {
        let size = T::SIZE;
        assert!(
            self.index + size <= self.bytes.len(),
            "Ran out of bytes while encoding"
        );
        field.encode(&mut self.bytes[self.index..self.index + size]);
        self.index += size;
    }
}

// Check that we used all the bytes whenever we're done with the encoder.
impl<'a> Drop for Encoder<'a> {
    fn drop(&mut self) {
        assert!(
            self.index == self.bytes.len(),
            "Did not use all bytes while decoding. Allocated: {}, used: {}",
            self.bytes.len(),
            self.index,
        );
    }
}

/// Struct that is helpful for implementing [`Encode::decode`]. Initialize it
/// with the initial allocated bytes, of the required size, using
/// [`Decoder::with_bytes`], and decode each field sequentially with
/// [`decode_field`]. [`finalize`] must be called after all of the expected
/// fields have been decoded.
pub struct Decoder<'a> {
    bytes: &'a [u8],
    index: usize,
    finalized: bool,
    errored: bool,
}

impl<'a> Decoder<'a> {
    /// Creates an `Encoder` with the given byte buffer.
    pub fn with_bytes(bytes: &'a [u8]) -> Self {
        Self {
            bytes,
            index: 0,
            finalized: false,
            errored: false,
        }
    }

    /// Decodes a value of type `T` from the Decoder's byte buffer.
    pub fn decode_field<T: Decode>(&mut self) -> Result<T> {
        let size = T::SIZE;
        let result = || -> Result<T> {
            if self.index + size > self.bytes.len() {
                return Err(Error::TooFewBytes {
                    allocated: self.bytes.len(),
                    used: self.index + size,
                });
            };
            T::decode(&self.bytes[self.index..self.index + size])
        }()
        .map_err(|e| {
            self.errored = true;
            e
        })?;
        self.index += size;
        Ok(result)
    }

    /// Check that we used all the bytes whenever we're done with the decoder.
    ///
    /// If you want to decode only a part of the encoding, take a slice of the
    /// bytes. All bytes of the slice must be used.
    pub fn finalize(&mut self) -> Result<()> {
        if self.index != self.bytes.len() {
            self.errored = true;
            return Err(Error::TooManyBytes {
                allocated: self.bytes.len(),
                used: self.index,
            });
        };
        self.finalized = true;
        Ok(())
    }
}

// Ensure that the decoder was finalized, if an error did not occur.
impl<'a> Drop for Decoder<'a> {
    fn drop(&mut self) {
        // Error occurred.
        if self.errored {
            return;
        }

        // Error did not occur, so we expected this to be finalized.
        if !self.finalized {
            panic!("Did not call finalize() after decoding");
        }
    }
}

/// A trait that signals that the implementing type has a known size and
/// encoding.
pub trait Encode: Sized {
    /// The encoded size of the type.
    const SIZE: usize;
    /// Encodes the type in the provided `bytes` buffer. Must fit exactly.
    fn encode(&self, bytes: &mut [u8]);
}

/// A trait that signals that the implementing type has a known decoding.
pub trait Decode: Encode {
    /// Decodes the type from the provided encoded `bytes`.
    fn decode(bytes: &[u8]) -> Result<Self>;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::Random;

    #[test]
    fn should_encode_and_decode_custom_struct() -> Result<()> {
        #[derive(PartialEq, Eq, Debug)]
        struct Foo {
            a: u16,
            b: u64,
            c: u16,
        }
        impl Encode for Foo {
            const SIZE: usize = u16::SIZE + u64::SIZE + u16::SIZE;
            fn encode(&self, bytes: &mut [u8]) {
                let mut encoder = Encoder::with_bytes(bytes);
                encoder.encode_field(&self.a);
                encoder.encode_field(&self.b);
                encoder.encode_field(&self.c);
            }
        }
        impl Decode for Foo {
            fn decode(bytes: &[u8]) -> Result<Self> {
                let mut decoder = Decoder::with_bytes(bytes);
                let result = Self {
                    a: decoder.decode_field()?,
                    b: decoder.decode_field()?,
                    c: decoder.decode_field()?,
                };
                decoder.finalize()?;
                Ok(result)
            }
        }

        let foo = Foo { a: 0, b: 0, c: 0 };
        assert_eq!(decode::<Foo>(&encode(&foo))?, foo);

        let foo = Foo { a: 1, b: 1, c: 1 };
        assert_eq!(decode::<Foo>(&encode(&foo))?, foo);

        // Use random numbers.
        let foo = Foo {
            a: u16::random(),
            b: u64::random(),
            c: u16::random(),
        };
        assert_eq!(decode::<Foo>(&encode(&foo))?, foo);

        Ok(())
    }

    #[test]
    #[should_panic]
    fn should_panic_if_encoder_runs_out_of_bytes() {
        let mut encoder = Encoder::with_bytes(&mut []);
        encoder.encode_field(&1u64);
    }

    #[test]
    #[should_panic]
    fn should_panic_if_encoder_does_not_use_all_bytes() {
        let bytes = &mut [1, 2, 3];
        let mut encoder = Encoder::with_bytes(bytes);
        encoder.encode_field(&1u16);
    }

    #[test]
    #[should_panic]
    fn should_panic_if_decoder_is_dropped_without_finalize() {
        let _ = Decoder::with_bytes(&[]);
    }
}
