use std::array::TryFromSliceError;
use thiserror::Error;

/// The encoding error type.
#[derive(Debug, Error)]
pub enum Error {
    /// Try from slice error.
    #[error("could not decode from slice")]
    TryFromSlice(#[from] TryFromSliceError),
    /// Ran out of bytes while decoding.
    #[error("ran out of bytes while decoding. (allocated {allocated:?}, used {used:?})")]
    TooFewBytes {
        /// The number of bytes allocated for the decoding.
        allocated: usize,
        /// The number of bytes actually used when decoding.
        used: usize,
    },
    /// Did not use all bytes while decoding.
    #[error("did not use all bytes while decoding. (allocated {allocated:?}, used {used:?})")]
    TooManyBytes {
        /// The number of bytes allocated for the decoding.
        allocated: usize,
        /// The number of bytes actually used when decoding.
        used: usize,
    },
}

/// Encoding result type.
pub type Result<T> = std::result::Result<T, Error>;
