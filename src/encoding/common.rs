//! Module that implements [`Encode`] for common types.

use super::error::Result;
use super::{Decode, Encode};
use byteorder::{ByteOrder, LittleEndian};

impl Encode for u16 {
    const SIZE: usize = 2;

    fn encode(&self, bytes: &mut [u8]) {
        LittleEndian::write_u16(bytes, *self);
    }
}
impl Decode for u16 {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(LittleEndian::read_u16(bytes))
    }
}

impl Encode for u32 {
    const SIZE: usize = 4;

    fn encode(&self, bytes: &mut [u8]) {
        LittleEndian::write_u32(bytes, *self);
    }
}
impl Decode for u32 {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(LittleEndian::read_u32(bytes))
    }
}

impl Encode for u64 {
    const SIZE: usize = 8;

    fn encode(&self, bytes: &mut [u8]) {
        LittleEndian::write_u64(bytes, *self);
    }
}
impl Decode for u64 {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(LittleEndian::read_u64(bytes))
    }
}

impl Encode for [u8; 8] {
    const SIZE: usize = 8;

    fn encode(&self, bytes: &mut [u8]) {
        bytes.copy_from_slice(self);
    }
}
impl Decode for [u8; 8] {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(bytes.try_into()?)
    }
}

impl Encode for [u8; 32] {
    const SIZE: usize = 32;

    fn encode(&self, bytes: &mut [u8]) {
        bytes.copy_from_slice(self);
    }
}
impl Decode for [u8; 32] {
    fn decode(bytes: &[u8]) -> Result<Self> {
        Ok(bytes.try_into()?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::encoding::{decode, encode, Error};
    use crate::test_utils::Random;

    mod test_u16 {
        use super::*;

        // Hard-code some expected values.
        #[test]
        fn should_encode_u16() {
            let bytes = encode(&0u16);
            assert_eq!(bytes, [0, 0]);

            let bytes = encode(&1u16);
            assert_eq!(bytes, [1, 0]);

            let bytes = encode(&255u16);
            assert_eq!(bytes, [255, 0]);

            let bytes = encode(&256u16);
            assert_eq!(bytes, [0, 1]);
        }

        #[test]
        fn should_encode_and_decode_u16() -> Result<()> {
            let num = 0u16;
            assert_eq!(decode::<u16>(&encode(&num))?, num);

            let num = 1u16;
            assert_eq!(decode::<u16>(&encode(&num))?, num);

            let num = u16::MAX;
            assert_eq!(decode::<u16>(&encode(&num))?, num);

            // Run on random numbers.
            let num = u16::random();
            assert_eq!(decode::<u16>(&encode(&num))?, num);

            Ok(())
        }

        // Failure cases should return the right error.
        #[test]
        fn should_return_err_when_decoding_bad_u16() {
            assert!(matches!(
                decode::<u16>(&[]),
                Err(Error::TooFewBytes {
                    allocated: 0,
                    used: 2
                })
            ));
            assert!(matches!(
                decode::<u16>(&[1]),
                Err(Error::TooFewBytes {
                    allocated: 1,
                    used: 2
                })
            ));
            assert!(matches!(
                decode::<u16>(&[1, 2, 3]),
                Err(Error::TooManyBytes {
                    allocated: 3,
                    used: 2
                })
            ));
        }
    }

    mod test_u64 {
        use super::*;

        // Hard-code some expected values.
        #[test]
        fn should_encode_u64() {
            let bytes = encode(&0u64);
            assert_eq!(bytes, [0, 0, 0, 0, 0, 0, 0, 0]);

            let bytes = encode(&1u64);
            assert_eq!(bytes, [1, 0, 0, 0, 0, 0, 0, 0]);

            let bytes = encode(&255u64);
            assert_eq!(bytes, [255, 0, 0, 0, 0, 0, 0, 0]);

            let bytes = encode(&256u64);
            assert_eq!(bytes, [0, 1, 0, 0, 0, 0, 0, 0]);
        }

        #[test]
        fn should_encode_and_decode_u64() -> Result<()> {
            let num = 0u64;
            assert_eq!(decode::<u64>(&encode(&num))?, num);

            let num = 1u64;
            assert_eq!(decode::<u64>(&encode(&num))?, num);

            let num = u64::MAX;
            assert_eq!(decode::<u64>(&encode(&num))?, num);

            // Run on random numbers.
            let num = u64::random();
            assert_eq!(decode::<u64>(&encode(&num))?, num);

            Ok(())
        }

        // Failure cases should return the right error.
        #[test]
        fn should_return_err_when_decoding_bad_u64() {
            assert!(matches!(
                decode::<u64>(&[]),
                Err(Error::TooFewBytes {
                    allocated: 0,
                    used: 8
                })
            ));
            assert!(matches!(
                decode::<u64>(&[1, 2, 3, 4, 5, 6, 7]),
                Err(Error::TooFewBytes {
                    allocated: 7,
                    used: 8
                })
            ));
            assert!(matches!(
                decode::<u64>(&[1, 2, 3, 4, 5, 6, 7, 8, 9]),
                Err(Error::TooManyBytes {
                    allocated: 9,
                    used: 8
                })
            ));
        }
    }

    mod test_u8_8_array {
        use super::*;

        #[test]
        fn should_encode_and_decode_u8_8_array() -> Result<()> {
            let bytes = [0, 0, 0, 0, 0, 0, 0, 0];
            assert_eq!(bytes.to_vec(), encode(&bytes));
            assert_eq!(decode::<[u8; 8]>(&encode(&bytes))?, bytes);

            let bytes = [1, 2, 3, 4, 5, 6, 7, 8];
            assert_eq!(bytes.to_vec(), encode(&bytes));
            assert_eq!(decode::<[u8; 8]>(&encode(&bytes))?, bytes);

            let bytes = <[u8; 8]>::random();
            assert_eq!(bytes.to_vec(), encode(&bytes));
            assert_eq!(decode::<[u8; 8]>(&encode(&bytes))?, bytes);

            Ok(())
        }

        // Failure cases should return the right error.
        #[test]
        fn should_return_err_when_decoding_bad_u8_8_array() {
            assert!(matches!(
                decode::<[u8; 8]>(&[]),
                Err(Error::TooFewBytes {
                    allocated: 0,
                    used: 8
                })
            ));
            assert!(matches!(
                decode::<[u8; 8]>(&[1]),
                Err(Error::TooFewBytes {
                    allocated: 1,
                    used: 8
                })
            ));
            assert!(matches!(
                decode::<[u8; 8]>(&[1, 2, 3, 4, 5, 6, 7, 8, 9]),
                Err(Error::TooManyBytes {
                    allocated: 9,
                    used: 8
                })
            ));
        }
    }

    mod test_u8_32_array {
        use super::*;

        #[test]
        fn should_encode_and_decode_u8_32_array() -> Result<()> {
            let bytes = <[u8; 32]>::default();
            assert_eq!(bytes.to_vec(), encode(&bytes));
            assert_eq!(decode::<[u8; 32]>(&encode(&bytes))?, bytes);

            let bytes = <[u8; 32]>::random();
            assert_eq!(bytes.to_vec(), encode(&bytes));
            assert_eq!(decode::<[u8; 32]>(&encode(&bytes))?, bytes);

            Ok(())
        }

        // Failure cases should return the right error.
        #[test]
        fn should_return_err_when_decoding_bad_u8_32_array() {
            assert!(matches!(
                decode::<[u8; 32]>(&[]),
                Err(Error::TooFewBytes {
                    allocated: 0,
                    used: 32
                })
            ));
            assert!(matches!(
                decode::<[u8; 32]>(&[1]),
                Err(Error::TooFewBytes {
                    allocated: 1,
                    used: 32
                })
            ));
            assert!(matches!(
                decode::<[u8; 32]>(&[1; 33]),
                Err(Error::TooManyBytes {
                    allocated: 33,
                    used: 32
                })
            ));
        }
    }
}
