//! Utilities for testing.

use rand::Rng;

/// Should generate values with a numerically uniform distribution, and with a
/// range appropriate to the type.
pub trait Random {
    fn random() -> Self;
}

impl Random for bool {
    fn random() -> Self {
        rand::thread_rng().gen()
    }
}

impl Random for u8 {
    fn random() -> Self {
        rand::thread_rng().gen()
    }
}

impl Random for u16 {
    fn random() -> Self {
        rand::thread_rng().gen()
    }
}

impl Random for u64 {
    fn random() -> Self {
        rand::thread_rng().gen()
    }
}

impl Random for [u8; 8] {
    fn random() -> Self {
        rand::thread_rng().gen()
    }
}

impl Random for [u8; 32] {
    fn random() -> Self {
        rand::thread_rng().gen()
    }
}
