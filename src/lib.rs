//! This repo contains the implementation of the Skynet Token (SKT) blockchain. SKT is designed to
//! be a blockchain that can be fully run and verified within a browser.

// TODO: Temporary fix for
// https://github.com/rustwasm/wasm-bindgen/issues/2774
#![allow(clippy::unused_unit)]
#![deny(missing_docs)]

mod build;
pub mod consensus;
pub mod encoding;
#[cfg(test)]
mod test_utils;
mod utils;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    /// Binds to `alert` in the browser.
    fn alert(s: &str);
}

/// Example entry point for wasm-pack binary.
#[wasm_bindgen]
pub fn greet() {
    utils::set_panic_hook();

    alert("Hello, world!");
}
