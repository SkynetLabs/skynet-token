use log::{Level, Metadata, Record};
use std::collections::HashSet;
use crate::utils::log;

/// WebLogger is a logger which allows for logging to a browser's console within
/// a wasm binary.
struct WebLogger<'a> {
    enabled_targets: HashSet<String>,
    level: Level,
    out: &'a (dyn Fn(&str) + Send + Sync),
}

impl<'a> WebLogger<'a> {
    /// Creates a new logger with the given log level.
    #[allow(dead_code)]
    fn new(level: Level) -> WebLogger<'a> {
        WebLogger {
            enabled_targets: HashSet::default(),
            level,
            out: &log,
        }
    }

    /// Enables a target for logging.
    #[allow(dead_code)]
    fn enable(&mut self, key: &str) {
        self.enabled_targets.insert(String::from(key));
    }

    /// Disables a target for logging.
    #[allow(dead_code)]
    fn disable(&mut self, key: &str) {
        self.enabled_targets.remove(key);
    }

    /// Sets the log-level.
    #[allow(dead_code)]
    fn set_level(&mut self, level: Level) {
        self.level = level;
    }
}

impl<'a> log::Log for WebLogger<'a> {
    // Returns whether the metadata should be logged or not.
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.level && self.enabled_targets.contains(metadata.target())
    }

    // Logs the given record.
    fn log(&self, record: &Record) {
        if !self.enabled(record.metadata()) {
            return;
        }
        let msg = format!("{} - {}", record.level(), record.args());
        (self.out)(&msg);
    }

    // Flushes the logger. No-op for web logger.
    fn flush(&self) {
        // Nothing to flush
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use log::Log;
    use std::sync::{Arc, Mutex};

    /// Basic testing for the logger and its associated functions.
    #[test]
    fn test_logger() {
        // Create a mock logger function which pushes the last logged line to
        // logged_lines.
        // NOTE: Since the function we need to pass in is expected to be a `Fn`
        // and not `FnMut`, we can't have it capture a mutable Vec. So we wrap
        // the Vec in a `RefCell`. Since a `RefCell` can't be shared between
        // threads which is a requirement by the `log` trait, we also wrap that
        // in the Arc/Mutex combo.
        let logged_lines = Arc::new(Mutex::new(Vec::<String>::new()));
        let log = |msg: &str| {
            logged_lines.lock().unwrap().push(String::from(msg));
        };

        // Create logger.
        let mut wl = WebLogger::new(Level::Warn);
        wl.out = &log;

        // Enable only the key "test" for logging.
        wl.enable("test");

        // Log something for that key and a different one. Both logs use the
        // Info level which it should log but since only one uses the enabled
        // 'test' key we should see 1 log message in logged_lines.
        let rec1 = log::RecordBuilder::new()
            .target("not_test")
            .level(Level::Warn)
            .args(format_args!("not_test"))
            .build();
        let rec2 = log::RecordBuilder::new()
            .target("test")
            .level(Level::Warn)
            .args(format_args!("test"))
            .build();
        wl.log(&rec1);
        wl.log(&rec2);

        assert_eq!(logged_lines.lock().unwrap().len(), 1);
        assert_eq!(logged_lines.lock().unwrap()[0], "WARN - test");

        // Log something with a higher and something with a lower level.
        let rec3 = log::RecordBuilder::new()
            .target("test")
            .level(Level::Info)
            .args(format_args!("info"))
            .build();
        let rec4 = log::RecordBuilder::new()
            .target("test")
            .level(Level::Error)
            .args(format_args!("error"))
            .build();
        wl.log(&rec3);
        wl.log(&rec4);

        // Only the error is logged.
        assert_eq!(logged_lines.lock().unwrap().len(), 2);
        assert_eq!(logged_lines.lock().unwrap()[0], "WARN - test");
        assert_eq!(logged_lines.lock().unwrap()[1], "ERROR - error");

        // Disable the target and re-log all the records. None should be logged.
        wl.disable("test");
        wl.log(&rec1);
        wl.log(&rec2);
        wl.log(&rec3);
        wl.log(&rec4);
        assert_eq!(logged_lines.lock().unwrap().len(), 2);
        assert_eq!(logged_lines.lock().unwrap()[0], "WARN - test");
        assert_eq!(logged_lines.lock().unwrap()[1], "ERROR - error");

        // Set the level to INFO. Rec3 should be logged now.
        wl.set_level(Level::Info);
        wl.enable("test");
        wl.log(&rec3);
        assert_eq!(logged_lines.lock().unwrap().len(), 3);
        assert_eq!(logged_lines.lock().unwrap()[0], "WARN - test");
        assert_eq!(logged_lines.lock().unwrap()[1], "ERROR - error");
        assert_eq!(logged_lines.lock().unwrap()[2], "INFO - info");
    }
}
