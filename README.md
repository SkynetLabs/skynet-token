# Skynet Token

This repo contains the implementation of the Skynet Token (SKT) blockchain.
SKT is designed to be a blockchain that can be fully run and verified within a browser.

The most up-to-date spec can be found [here](https://gitlab.com/SkynetLabs/skynet-token/-/blob/main/docs/spec.md).
