//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate wasm_bindgen_test;

use skynet_token::build_var;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn test_build_var() {
    build_var!(
        const NUMBER: u64 = {
            standard = 1337,
            testing = 42,
        };
    );
    assert_eq!(NUMBER, 42);
}
