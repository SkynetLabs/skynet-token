#![no_main]

use libfuzzer_sys::fuzz_target;
use skynet_token::{
    consensus::block::BlockHeader,
    encoding::{decode, encode, Encode, Result},
};

fuzz_target!(|data: &[u8]| {
    // Fuzzed code goes here.

    if data.len() < BlockHeader::SIZE {
        return;
    }

    // Don't do anything with errors. We're trying to trigger unexpected panics
    // - errors are not a pathological case.
    let block_header: Result<BlockHeader> = decode(&data[..BlockHeader::SIZE]);

    if let Ok(block_header) = block_header {
        let _ = encode(&block_header);
        let _ = block_header.encode_for_pow();
    }
});
