#![no_main]

use libfuzzer_sys::fuzz_target;
use skynet_token::{
    consensus::block::BlockHeader,
    encoding::{decode, Encode, Result},
};

fuzz_target!(|data: &[u8]| {
    // Fuzzed code goes here.

    if data.len() < BlockHeader::SIZE * 2 {
        return;
    }

    // Don't do anything with errors. We're trying to trigger unexpected panics
    // - errors are not a pathological case.
    let block_header: Result<BlockHeader> = decode(&data[..BlockHeader::SIZE]);
    let parent_header: Result<BlockHeader> =
        decode(&data[BlockHeader::SIZE..BlockHeader::SIZE * 2]);

    if let (Ok(block_header), Ok(parent_header)) = (block_header, parent_header) {
        let _ = block_header.verify_difficulty_state(&parent_header);
    }
});
