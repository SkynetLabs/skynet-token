#![no_main]

use libfuzzer_sys::fuzz_target;
use skynet_token::{
    consensus::{block::BlockHeader, target::Target},
    encoding::{decode, Result, Encode},
};

fuzz_target!(|data: &[u8]| {
    // Fuzzed code goes here.

    if data.len() < BlockHeader::SIZE + Target::SIZE {
        return;
    }

    // Don't do anything with errors. We're trying to trigger unexpected panics
    // - errors are not a pathological case.
    let block_header: Result<BlockHeader> = decode(&data[..BlockHeader::SIZE]);

    if let Ok(mut block_header) = block_header {
        let target = decode(&data[BlockHeader::SIZE..BlockHeader::SIZE + Target::SIZE]);
        if let Ok(target) = target {
            // Make sure the target won't take too long to solve.
            if target < Target([20; 32]) {
                return;
            }

            let _ = block_header.solve(target);
        }
    }
});
