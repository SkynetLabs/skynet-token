# Skynet Token Design

TODO: We need to make sure that when we sign transactions, we ignore/exclude
the NextAccount element in the signature, because that can change arbitrarily
as new leaves are added, but those additions should not be invalidating any
signatures.

## Index

Overview of the sections in this document with links.

* [Actors](#Actors)
* [Consensus](#Consensus)
    1. [Types](#Types)
    2. [Blocks](#Blocks)
    3. [Transactions](#Transactions)
    4. [Transaction Fees](#Transaction-Fees)
* [Wallet](#Wallet)
* [Miner](#Miner)
    1. [Mining](#Mining)
    2. [Oak difficulty algorithm](#Oak-difficulty-algorithm)
    3. [Persistence](#Persistence)
* [Gateway](#Gateway)

## Actors

In this section we describe the various actors in the Skynet Token ecosystem and
the requirements we impose on them. This is important because it will influence
the remaining design.

* [Full Node](#Full-Node)
* [Miner](#Miner)
* [Light Wallet](#Light-Wallet)

### Full Node
A full node a.k.a. fully validating node is a participant in the network that
fully validates each and every block and then stores a copy of it within its
own space on skynet from where other participants can download them. Validation
includes checking the block size, checking all txns in the body of each block,
making sure that the miner payout is correct, that the block was created at a
reasonable time, that it is otherwise well formed and contains enough PoW, and
that lite nodes looking at the block will be able to trust the block.

#### Requirements

* Uses less than 40 MiB of memory
* Can validate a block using only the block and the header of the parent block

### Miner
A miner is a participant on the network capable of creating (mining) new blocks
by building them out of new transactions and performing some proof-of-work (PoW)
on the resulting block. They are usually full nodes since they need to be able
to tell that a block that they are basing a new block off is valid but
theoretically they could start mining a block earlier after verifying the
submitted txns against the earlier blocks accounts root and verifying the PoW of
the block. That way they might be able to start mining a few seconds sooner.

Users use wallets to manage and spend their funds. Wallets send transactions on
behalf of the user to miners through public API endpoints. These transactions
can be proven to only contain spendable outputs thanks to a proof that the miner
will attach to the block.

For wallets to be aware of miners on the network, the miners need to announce
themselves through on-chain transactions. That means they add some information
about themselves like their IP to a transaction.

Miners are expected to broadly broadcast the latest blockchain they know, and
also to broadly broadcast all transactions that they know about. Other actors
in the ecosystem will help enforce this expectation by refusing to submit
transactions to miners that do not propagate blocks and transactions, or are
frequently broadcasting old or orphaned blocks.

#### Requirements

* Uses less than 56 GiB of RAM
* Uses less than 4 TB of m.2 SSD storage
* Uses less than 8 CPU cores at 3 GHz
* Have an announcable endpoint that people can submit txns to
* Can announce themselves through a txn on chain

### Lite Wallet

A lite wallet is a wallet that can identify a user's address using nothing more
than the header chain and the set of blocks. The lite wallet assumes that if
the blocks have enough PoW, they are valid. The goal of the lite wallet is to
rapidly determine the user's balance and transaction history.

Most users will be running a hybrid wallet which will run in lite mode until
the user's balance and transaction history have been determined, at which point
it will switch to selectively fully validating blocks until it has transformed
into a full node. While selectively validating blocks, the hybrid wallet will
survey the user's friends and trusted follows and validate the blocks that have
the least total validations.

#### Requirements

* Uses less than 10 MB of memory
* Downloads less than 10kb per block
* Can process 30+ blocks per second

## Consensus

This section describes the overall structure and encoding of the Blockchain.
This includes Blocks, Transactions, Transaction Outputs and proofs for spending
from accounts.

### Types

The following sections make use of generic types such as `crypto.Hash`.
These types are mapped to the following concrete types.

* `crypto.Hash` -> sha3-256 (32 bytes hash)
* `time.Time`   -> unix timestamp (`int64`)
* `RegistryEntryID` -> 32 byte Blake2B hash of a pubkey and datakey (see Skynet
  for implementation details)


### Blocks

This section describes the layout of the blocks and their headers and also
provides some information about related types.

```go
// The ID of a block is hash of its header
type BlockID crypto.Hash

// The BlockLink is a link to the body of a block.
type BlockLink Skylink

// LiteHeader contains all of the information of the BlockHeader which is
// put into a block's body for subsequent blocks. The full BlockHeader is described
// below and contains the LiteHeader embedded in the beginning.
type LiteHeader struct {
    // The id of the previous block in the chain.
    parentid                    BlockID
    
    // The timestamp of the Block. A block is not valid if its
    // time is too far in the future.
    timestamp                   time.Time
    
    // The block header makes use of a trick to allow for trustless mining pools where
    // miners can't cheat the pool by hiding solved shares which would be good enough
    // to create a full block. That works by xor'ing a secret pow_mask to the hash
    // provided by the miner which will then be compared to the network target instead
    // of directly comparing the hash.
    // An explanation + implementation can be found here:
    // https://github.com/handshake-org/hsd/blob/master/lib/primitives/abstractblock.js#L368-L408
    pow_mask crypto.Hash
    
    // Nonce describes the grinding space for the miner.
    // It's an arbitrary data field which is modified when mining.
    nonce                       [8]byte

    // block is the actual body of the block.
    block                       Blocklink
    
    // The root of the merkle tree over all accounts and their state.
    stateRoot crypto.Hash
    
    // Additional state regarding the block which will be summarized as a single hash
    // when doing the PoW. NOTE: The BlockHeader also contains a blockState field.
    // That's the second half of the state. Both should be summarized as a single hash.
    blockState struct {
        // numLeaves is the number of leaves in the chainstate.
        //
        // numAccountUpdates states the number of account updates that occured
        // in the block. This number will be smaller than the number of leaves
        // that changed, as there can be leaf updates which do not impact any
        // accounts. Note that numAccountUpdates will include updates to accounts
        // that were just created in this block.
        //
        // numNewAccounts states the number of new accounts that got
        // created in this block. Note that not all leaves in the
        // chainstate are accounts, and the non-account leaves that
        // get added will not be added to numNewAccounts.
        numLeaves         uint64
        numAccountUpdates uint16
        numNewAccounts    uint16
    }
}

// BlockHeader is derived from the Block and is the
// object that gets hashed for the PoW. It covers all the fields
// of the block itself in some form. That means modifying the block
// always modifies the header as well.
type BlockHeader struct {
    // All the info of the LiteHeader is embedded at the top.
    LiteHeader
    
    // block state contains any additional information about the
    // block we need in the header. That way we keep the amount of
    // data we need to rehash in every iteration of the pow small.
    blockState struct {
        // The baseFee establishes how many tokens need to be burned
        // for a transaction to be accepted into the block. This does
        // not need to be part of the LiteHeader as it can be
        // computed from all of the data in the parent header.
        baseFeeStep uint16
        
        // The state required to compute the difficulty of the next block.
        //
        // Note that the difficulty trails by one block. The difficulty of a
        // child block is determined by the decayedTotalTarget and
        // decayedTotalTime of its grandparent.
        difficultyState struct {
            // The total amount of work that has been put into the block. You
            // can compute this by summing up the inverses of all the block
            // targets and then inverting them.
            decayedTotalTarget [32]byte
        
            // The timestamps of the last 11 blocks. The timestamp of a new
            // block is not allowed to be lower than the median timestamp of
            // the past 11 blocks.
            prevTimestamps   [11]int64
            
            // The decayed total target as of the parent block. This is like
            // target, except it gets decayed every time a new block is
            // produced.
            parentDecayedTotalTarget      [32]byte // Used to determine target of child.

            // The decayed total time as of the parent block. This is the
            // amount of time that has elapsed since genesis, except it gets decayed every
            // time a new block is produced.
            parentDecayedTotalTime      int64 // Used to determine target of child block.

            // The next miner payout that reaches maturity. This payout will be part of the
            // next next block.
            nextMinerPayout DelayedMinerPayout
            
            // A hash over the next 143 miner payouts after the nextMinerPayout.
            futureMinerPayouts Hash

            // The height of the block within the chain.
            height           uint64
        }
    }
}

// Block describes a Skynet Token Block. It links to the previous block
// and contains not only transactions but also a summary of the transactions.
type Block struct {
    // This section contains the headers of the previous 10000 blocks. A
    // LiteHeader is a BlockHeader minus anything that can be
    // computed given the previous BlockHeader. For the most part, this
    // is the difficultyState and the baseFee for the transaction fees.
    prevHeader   BlockHeader
    blockHeaders [9999]LiteHeader
    
    // activeAccounts is a set of extendable integers which lists out the
    // indexes of all the Accounts that have had one or more of their
    // AccountEntry objects updated.
    //
    // An extendable integer has a varied number of bits depending on the
    // size of the chainstate. We use N bits, where 2^n-1 is less than the
    // total number of accounts in the chainstate, and 2^n is greater than
    // or equal to the total number of accounts in the chainstate. This
    // means that the ShardOffsets and ActiveAccounts arrays can be
    // non-integer numbers of bytes in length. The two objects are packed
    // together tightly, which means that each element of the
    // ActiveAccounts array (including the first) may start at a bit which
    // is not byte-aligned. AccountUpdates however *does* get byte-aligned,
    // the final byte of the ActiveAccounts object is padded out to the
    // nearest byte.
    //
    // An extendable integer is always at least 16 bits.
    //
    // The activeAccounts object gets sharded into a number of equal sized
    // arrays. There are Ceil(Sqrt(len(AccountUpdates))) shards in total,
    // each with Ceil(Sqrt(len(AccountUpdates))) elements. The final shard
    // may have fewer elements than the rest due to the rounding function.
    // The maximum number of AccountUpdates is 2^16, therefore the maximum
    // number of shards is 2^8.
    //
    // len(accountUpdates) can be found in the header of the block, meaning
    // lite nodes will be able to precompute the size and structure of the
    // AccountUpdates section of the block.
    //
    // The accountShardOffsets array has one element per shard. Each element in
    // accountShardOffsets is equal to the first value of the corresponding shard.
    // Lite wallets can download the accountShardOffsets to learn which ActiveAccounts
    // shard would have their account in it. Lite wallets would look for the
    // first accountShardOffset that is larger than their own account index, and then
    // know that their account would be listed in the previous shard. If all
    // elements are larger than their account index, they know their account
    // was not updated in this block. If all elements are smaller than their
    // account index, they know to check the last shard.
    //
    // If an account had multiple updates in this block
    // (including if the same leaf was updated multiple times), the
    // account's index will be listed multiple times. This means it is
    // possible for the account's index to appear in two shards. This does
    // not however require the lite wallet to download both shards.
    //
    // The first element of each shard is known because it is stated in the
    // accountShardOffsets array. Therefore, it can be repurposed to describe the
    // number of times the account index of the first element appears in
    // the previous shard. Because shards have a max length of 256, this
    // will always fit in 1 byte, which is the minimum allowed size of the
    // extendable integer.
    //
    // Note that the accountUpdates will include updates to accounts that were
    // just created in this block.
    //
    // PERFORMANCE INFORMATION:
    //
    // In the case where an account was not updated in a block, the lite
    // wallet will need to make two round trips total, one to download the
    // accountShardOffsets and one to download the shard that would contain the
    // wallet's account index.. The lite wallet will then learn that its
    // account was not updated and can move on to the next block. If there
    // are 10 billion accounts in the chainstate and 2^16 account updates
    // in a block, that will require 1152 bytes of downloading for each
    // round trip, plus as many as 928 bytes each round trip for range
    // proofs, resulting in a total of cost of 4224 bytes downloaded over 2
    // round trips.
    //
    // In the case where the account was updated, a third round trip will
    // be required which will need 41 bytes of download per account update,
    // plus up to another 928 bytes of range proofs. If there are 3 updates
    // for the account in this block, the total size of the third round
    // trip will be 1051 bytes.
    accountShardOffsets []Xint
    activeAccounts      [][]Xint
    accountUpdates      []AccountUpdate

    // newAccounts is a field that declares all of the IDs of the new accounts
    // that have been created in this block. It only declares new accounts, it does
    // not declare new leaves that are not accounts.
    //
    // Like activeAccounts, newAccounts is sorted and sharded. newAccounts is
    // sorted alphabetically. The number of shards is the square root of the
    // number of new accounts, and the size of each shard is the square root of the
    // number of new accounts. Lite wallets can find their accounts in the
    // block by downloading the full set of offsets, and then by downloading their
    // shard within the newAccounts.
    //
    // PERFORMANCE:
    //
    // The maximum number of newAccounts is 2^16. That means there will be 256
    // shards, and newAccountShardOffsets will be 8192 bytes. Each shard will
    // also be 8192 bytes. Each round trip will require 928 bytes in range
    // proofs, making the total performance overhead of scanning a block for new
    // accounts 2 round trips and 18,240 bytes in the worst case.
    //
    // An alternative design had us binary searching a non-sharded array of
    // NewAccounts. The worst case overhead of that design was 18 round trips
    // and 17.280 bytes of downloads. The new design was chosen because we
    // valued the reduced round trip count over the byte savings. The new design also
    // performs signficantly better in the average case. If there are only
    // 16,000 new accounts in a block, the current design has a total overhead
    // of 10,040 bytes and 2 round trips, whereas the binary search design has a total
    // overhead of 16 round trips and 15,360 bytes.
    newAccountShardOffsets []AccountID
    newAccounts            [][]AccountID

    // These fields contain information that is required for the full node
    // to fully validate the block.
    //
    // LeafTransformations is a sorted list of transformations of the
    // leaves of the merkle tree. The transformations are sorted by index,
    // and each transformation shows the before and after of each leaf that
    // changed in the block. LeafTransformations includes the leaves that
    // changed because new accounts were created and the state linked list
    // had to be updated. Each leaf transformation contains a merkle proof
    // which shows that the leaf was part of the old Merkle root of the
    // parent block. The Merkle proofs build upon each other. The very
    // first leaf transformation contains a full Merkle proof, and the
    // proof in each transformation after that assumes that the full node
    // has all the information from the previous proofs, and excludes
    // elements of the proof that would be redundant.
    //
    // Note that the leafTransformations array will only include
    // transformations of existing leaves, it does not include any leaves that were
    // created in the current block.
    //
    // The NewLeafProofs are simple pointers that point to the index of
    // the proving leaf, and the index of the leaf that is the new
    // next leaf for the proving leaf. Both of these leaves will appear in the
    // LeafTransformations array, which means the full node can compare and
    // make sure the proof is valid and the linked list is updated
    // correctly.
    //
    // The recentMinerPayouts is a list of the next 144 accounts awaiting their
    // coinbase payouts. The first payout of this list can also be found in this
    // block's header as the `nextMinerPayout`.
    //
    // The transactions are the list of raw transactions that need to be
    // verified. Each transaction updates a number of leaves, and the
    // leaves will all appear in the LeafTransformations. The full node
    // just needs to verify that after all transactions have run, all of
    // the leaf transformations match their originally claimed final state.
    numLeafTransformations uint32
    leafTransformations    []LeafTransformation
    newLeafProofs          []NewLeafProof
    transactions           []Transaction
    recentMinerPayouts     [144]DelayedMinerPayout
}

// The DelayedMinerPayout contains the address which the full block reward is paid
// out to and the amount that will be paid out. The block always contains the most
// recent 144 delayed payouts. The oldest one is also part of the block's header.
// The first leaf transformation of the next block will contain the oldest delayed
// payout of the previous block. If the receiving account doesn't exist, it will be
// created and be part of the newAccounts field as well as newAccountsShardOffsets.
// The transformation will also be accounted for in numLeafTransformations,
// leafTransformations and newLeafProofs. There will be no transaction in
// 'transactions' for the payout.
// NOTE: The payout always has a 144 block (1 day) maturity delay before the money
// can be spent again to protect against reorgs invalidating transaction which
// depend on the payout.
// The coinbase will be a constant 2 million SKT per year. So assuming that we mine
// 144*365 blocks a year that's about 38 SKT per block.
// Another 2 million SKT per year will go to SkynetLabs as a dev subsidy.
// Finally there will be a 100 million SKT genesis block allocation for SkynetLabs.
type DelayedMinerPayout struct {
    AccountID [32]byte
    Amount    Currency
}

// An AccountUpdate provides an update to a leaf in the chainstate. The 'index'
// value states the index of the leaf that is being updated. Note that this
// index may not correspond to the account itself, it may correspond to one of
// the leaves owned by the account instead. The ID of the leaf can be found in
// the initialState and finalState fields, as they are leaf objects which must
// declare their leafID.
type AccountUpdate struct {
    index        uint64
    initialState [PersistedLeafSize]byte
    finalState   [PersistedLeafSize]byte
}

// PersistedLeafSize is the size, in bytes, of a leaf in the chainstate. It's 
// set at 128 because 64 bytes isn't enough for all leaf types, and it has to be
// a power of two to ensure a round number of leafs fit in each disk sector.
const PersistedLeafSize = 128

// A LeafTransformation describes how a leaf in the chainstate
// transforms after the execution of a block. It shows the initial state, the
// final state, and then provides a Merkle proof that shows the inital state is
// part of the parent block. If the leaf object is less than 128 bytes, the
// remaining bytes are padded with zeroes.
//
// Note that the merkle proof is dependent on previous LeafTransformation
// objects, it assumes that the proofs provided by those objects have been
// cached, and therefore only new information needs to be provided.
type LeafTransformation struct {
    AccountUpdate

    transformationProofLength uint8
    transformationProof       []crypto.Hash
}

// A NewLeafProof provides the information required to verify that a new
// account does not yet exist in the Merkle tree. It only needs to point to
// the index that has the proving account. You don't need to know the index
// of the new account because the index of the new account is implied by the
// order of the NewLeafProof in the newLeafProofs array - each proof
// increments the index of the new account by 1.
type NewLeafProof struct {
    newLeafID      LeafID
    provingLeafID  LeafID
    leafFinalState [PersistedLeafSize]byte
}
```

#### State Merkle Tree

All existing accounts and their state are covered by a [merkle
tree](https://en.wikipedia.org/wiki/Merkle_tree). That allows us to include a
proof in the block that proves money is spent from an existing account with a
sufficient balance.

We also need miners, who create the blocks, to be able to prove that an account
which they append to the tree as a new leaf doesn't already exist in the tree.
That's because every account should only ever correspond to one leaf. For that
we need to be able to create non-membership proofs which merkle trees don't
support natively. Instead we can use a trick to achieve efficient non-membership
proofs.

The leaves of the tree do not only contain the account that they represent but
also the ID of the next account in an alphabetically sorted list of account IDs.

```go

// The AccountID is a hash of the account's corresponding pubkey.
type AccountID crypto.Hash

// SKTAssetID defines the asset ID for the SKT token.
const SKTAssetID = crypto.Hash{1}

// The public key associated with an account.
type PublicKey [64]byte

// AccountRules specify the spending rules of the account.
type AccountRules struct {
  // Version is the version of the rules struct. Version 1 requires only a signature
  // from the pubkey for spending from the account.
  Version byte
  
  // Pubkey which corresponds to the private key required to spend from the account.
  Pubkey  PublicKey
}

// ChainStateCommonLeaf is the common state between leaves of the chain state. At
// the time of writing 2 types of leaves exist. The Account and the TokenBalance.
type ChainStateCommonLeaf struct {
  // Type '1' - Account v1
  // Type '2' - Tokenbalance v1
  Type      byte
  
  // Next leaf in the tree's implicit linked list which is sorted by AccountID.
  NextLeaf LeafID
  
  // ID of the corresponding Account.
  AccountID AccountID
}

// TokenID is a unique identifier for a token in the Skynet Token ecosystem. 
// The TokenID is a hash of the token's metadata.
type TokenID [32]byte

// A TokenBalanceLeaf is one type of leaf in the chainstate. The type is 
// implied to be 'TokenBalanceV1' and the 'Type' byte.
// at the top must always be set to '2'.
//
// The chainstate ID of a token balance object is derived using
// H("tokenBalance" || AccountID || AssetID). This ID is used to
// ensure uniqueness, and participates in the linked list that
// sorts the chainstate leaves alphabetically by their ID.
type TokenBalanceLeaf struct {
  ChainStateCommmonLeaf
  
  // TokenID is the identifier for an asset. Can be SKT, or can be
  // another token. 
  TokenID TokenID
  
  // TokenMetadata is the token's associated metadata.
  TokenMetadata Skylink
  
  // The balance for the asset.
  Balance Currency
}

// LeafID derives the LeafID for a TokenBalanceLeaf. Each leaf has a unique LeafID
// but every type of leaf has a different way of deriving it.
func (leaf TokenBalanceLeaf) LeafID() LeafID {
  return Hash("tokenBalance" || leaf.AccountID || leaf.TokenID)
}

// An AccountLeaf is one type of leaf in the chainstate. It declares an Account,
// which has an associated set of 'Rules' for spending tokens from the account or
// otherwise performing administrative actions over the account. No token balances
// are declared in this object.
//
// The 'Type' byte must always be set to '1', and implies an AccountV1.
//
// The ID of the AccountLeaf object is determined by the hash of the Rules object
// that is provided for the account when the account is created. The ID of the
// account does not change if the Rules get changed.
type AccountLeaf struct {
    ChainStateCommonLeaf
    
    // The balance for the SKT token.
    Balance Currency
    
    // Hash of the account's AccountRules required to spend from it.
    Rules crypto.Hash
    
    // A nonce that is incremented whenever money is withdrawn
    // from the account to prevent replay attacks. If two 
    // transactions refer to an account with the same nonce, only
    // one will succeed.
    Nonce       uint64
}
```

Since the `nextAccount` fields form a sorted linked list, we can easily prove
that a new account should be appended to the end of the tree by providing a
merkle proof of the current last element. The last element is the element where
`nextAccount == 0`.

##### Example
Let's say only a single Account exists so far and that is `A`.
This results in the following merkle tree.

```
  nA
  |  
  A   
  
List: A -> nil
```

In this case `A.nextAcount = 0` since it's the first account as well as the last
one. Then another account `C` is added which is appended to the end of the tree.

```
   nAC 
  /  \
  nA nC
  |  |
  A  C
  
List: A -> C -> nil
```

Since `C` doesn't already exist, the Miner needs to include a proof in the block
which proves non-membership of `C`. To do so, it creates a merkle proof for `A`
to prove that it is the current last element in the alphabetically sorted list.
Then it updates `A` to point to `C` by setting `A.nextAccount = C.accountID` and
`C.nextAccount = 0`.

Let's add one more account `B`. Alphabetically, `B` is between `A` and `C`.
Chronologically it comes after both. So this results in the following tree and
list.

```
     nACB 
   /  |  \ 
  nA  nC  nB
  |   |   |
  A   C   B
  
List: A -> B -> C -> nil
```

Same as before, B is added to the end of the tree because it didn't already
exist. So we need to provide another proof. This time, since we are inserting in
the middle, we provide a proof of `A` again. That way we reveal that
`A.nextAccount == C.accountID` but since `B` is between `A` and `B` we can just
update `A` and `B` to `A.nextAccount = b.accountID` and `b.nextAccount =
c.accountID`.


#### Transactions

Transactions transfer tokens from one account to another. Their inputs are one
or more accounts and the outputs are account IDs. The miner will then know how
to update the accounts corresponding to those IDs.

```go
// Generic transaction type which supports multiple types of transactions depending on the soure of the transaction and the
// action.
type Transaction struct {
    // Version of the transaction format.
    Version byte
    
    // The ID of the leaf in the chain state which contains the balance to spend from.
    SourceLeaf LeafID
    
    // Maximum base fee to pay for this txn in base fee steps. This is burnt.
    BaseFeeStepLimit  [2]byte
   
    // Premium fee to pay to miners.
    PremiumFee  Currency
    
    // Action to perform in the transaction. Depends on the leaf's type. e.g. for balances it would be a Currency and receiving
    // address.
    Action Action
    
    // Signature contains data which authorizes the transaction. Depends on the leaf's type, the action's type and the
    // AccountRules. e.g. for a basic SKT transfer it would be a ed25519 signature.
    Signature []byte
}
```

This is very generic format. For a simple SKT balance transfer, the Transaction would look like the following:

```go
type SKTTransferTxn struct {
    Version =     byte(1)
    SourceLeaf =  [32]byte
    
    // Action consists of...
    Action = {
        // Type of action.
        Type = byte
        
        // Amount to pay the receiver.
        Amount =        [8]byte
        
        // Receiving address.
        Address =       [32]byte
    }
    
    // ed25519 Signature
    Signature =     [64]byte
}
```

The total transaction size is therefore 153 bytes. The SourceLeaf implies the
account that is spending the funds, which in turn implies the AccountRules,
which in turn defines what form the signature needs to have. When validating a
transaction, the full node would know to look all of this information up from
the prior information in the block, namely the LeafTransformations array. From
this transaction and prior information in the block, a full node should be able
to figure out:

- The index of the leaf that contains object being updated (like a balance being
  spent)
- The index of the leaf that contains the account rules (the account owner,
  basically)
- The index of the leaf that is receiving the funds

By the time the full node has gotten this far, it will have already verified
Merkle proofs on all of those chainstate leaves, it will be able to update any
changes to the state of those leaves, and then once all transactions are
processed it'll be able to verify that the updates were all honest and match the
new Merkle root of the new block.

This transaction format gives us a ton of flexibility. For example, if we change
the AccountRules to look more like Bitcoin script or even outright use
miniscript, the 'Signature' can include any data that has to be hashed, or
include multisig data, or whatever else we care about. The signature is an
arbitrary type and how it gets processed can be defined in any way (via a
hardfork upgrade of course). The same is true of the 'Action' part of the
transaction. If the LeafID points to an account leaf, the set of 'Actions' could
be 'change the AccountRules'. If the LeafID points to a token balance, the
action could be 'burn tokens', or 'lock tokens', or more likely it'll just be
'send tokens'. If the LeafID points to an AMM, the 'Action' could be 'purchase
tokens'.

This transaction format is nice and general and will suit most of our needs
going forward. And unlike Sia transactions, it's also nice and concise.

#### Transaction Fees

Skynet's transaction fee algorithm was inspired by
[EIP-1559](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1559.md). The
main idea is to set a base fee that all transactions have to pay each block,
and that base fee gets burned instead of going to miners. Users can pay the
miners an additional fee on top of the base fee if they wish.

There is a target amount of activity every block. If the total amount of
activity in a block exceeds the target, the base fee is increased. If the total
amount of activity in a block is less than the target, the base fee is
decreased.

Skynet's algorithm departs materially from EIP-1559 in two ways: it has a
deadzone, and base fee adjustements follow a step function.

The deadzone is a range of network activity that is seen as 'acceptable' and
does not result in a change to the base fee. The main idea is that we want the
base fee to remain stable if possible, because the base fee increasing will
price out transactions that could otherwise get into a block. If blocks are not
full, there is no need to increase the base fee. The skynet deadzone is set to
+/- 25%, which means that the base fee will not change if blocks are between
25% and 75% full.

The step function means that the baseFee always changes by a fixed amount. If
the base fee goes up one block and comes back down the next block, it will
always return to the exact same amount. This avoids situations where a
transaction cannot be added to a block because its maximum base fee is too low
by a tiny amount. Skynet's step function is set to 5%, which means that the
base fee can change up to 34% per hour or 1125x per day.

```go
// Global Constants
TargetSaturationDenominator = 2 // Target half full blocks on average
DeadzoneDenominator = 4 // Allow num transactions to drift by 25% each direction
outputLimit = 1<<16 // 2^16 outputs per block or half that in emergency mode
stepSize = 1.05

// Algorithm to determine base fee per output for the next block.
func (header BlockHeader) BaseFeeOfNextBlock() {
    target := outputLimit / TargetSaturationDenominator
    targetFloor := target - outputLimit / DeadzoneDenominator
    targetCeil := target + outputLimit / DeadzoneDenominator

    newBaseFeeStep := header.baseFeeStep
    if header.numAccountUpdates > targetCeil && newBaseFeeStep < uint16Max {
        newBaseFeeStep++
    } else if header.numAccountUpdates < targetFloor && newBaseFeeStep > 1 {
        newBaseFeeStep--
    }

    // Take the stepSize to the power of the newBaseFeeStep, convert it to a currency type,
    // and return it. The currency type in Skynet is a uint64. The max allowed fee step is
    // 65535, which is 10^1334, which well overflows a uint64. The `NewCurrency` method is
    // expected to check for overflows and set them to uint64Max.
    return NewCurrency(stepSize**newBaseFeeStep)
}
```

##### NOTE: Emergency Mode

When the chain is in 'emergency mode', the outputLimit is cut in half. This
reduced outputLimit will be used in the fee target algorithm, which means that
the baseFee will continue to adjust like normal.

### Verifying Blocks

This section is dedicated to explaining the different ways various participants
on the network verify blocks.

#### Lite Wallet

The lite wallet starts by downloading the full header chain and verifying the
proof-of-work to ensure that all of the blocks have sufficient work and that
the lite node is looking at the longest chain. From that point, the lite node
assumes that since the blocks have a valid PoW, the blocks themselves are fully
valid. This assumption allows the lite node to skip many verification steps and
scan through blocks much more rapidly to determine things like the balance of
the user's wallet.

When scanning a block while already knowing the user's account index, the lite
wallet takes the following steps:

1. Use the numLeaves field from the BlockHeader to determine the size of the
   Xint type within the block. The size of the Xint type is the minimum number
   of bits required to enumerate all of the leaves in the block. For example,
   if there are 4 billion leaves, the Xint is 32 bits. If there are 5 billion
   leaves, the Xint is 33 bits. The Xint must always be at least 16 bits.

2. Use the numAccountUpdates field from the BlockHeader to determine the number of
   shards in the activeAccounts array. The number of shards is the square root
   of the numAccountUpdates value. Because numAccountUpdates is a uint16, the number
   of shards is at most 256.

3. Determine the offset of the accountShardOffsets field in the block. The offset will
   be the BlockHeader size (392 bytes) plus 9999 times the LiteHeader
   size (158 bytes), which is 1,580,234 bytes.

4. Determine the size of the accountShardOffsets field in the block. The size will be
   the Xint size multiplied by number of shards in the activeAccounts array,
   then rounded up to the nearest byte.

5. Perform a range request on the block to download the accountShardOffsets array. If
   necessary, convert from Xints to uint64s to make life easier.

6. Loop over the accountShardOffsets array to determine which shard contains the
   user's account. Each element of accountShardOffsets points to an index in
   the chainstate. The final element in accountShardOffsets that points to an
   index that is less than or equal to the user's account index corresponds to
   the shard that contains the user's account. If the first element is already
   larger than the user's index, then the user's account did not get updated in
   this block. If the last element is smaller than the user's index, then the
   user's account can be found in the final shard of activeAccounts. NOTE: in
   rare cases, multiple elements of accountShardOffsets may list the user's
   account. In that case, use the shard with the highest index that lists the
   user's account.

7. Download the shard that corresponds with the shard that would contain the
   user's account. Note that the shard may not be byte-aligned, the initial
   offset of the shard and the final offset of the shard may be at awkward bit
   values. Download the surrounding bytes as necessary and convert the shard
   from Xints to uint64s for convenience.

8. Scan the shard for an index that exactly matches the user's account index.
   If there is no match, that means that the user's account was not updated in
   this block. If there is a match, count the number of times that the user's
   index appears consecutively. This corresponds to the number of updates that
   exist in the block for the user's account. If the user's account is listed
   exactly in the accountShardOffsets array, add the numerical value of the
   first element of the shard, as it will be set to a count rather than an
   account index. That count corresponds to the number of times that the user's
   account is listed in previous shards. By adding that number to the number of
   times the user's account index appears in the current shard, you get the
   total number of accountUpdates for the user's account that appear in this
   block. Note that the first element of the shard will always be 1 or more.

9. Determine the offset of the user's account updates within the accountUpdates
   array. This can be determined by multiplying the index of the user's shard
   by the number of items in each shard (this is the same as the number of
   shards, which was computed earlier) and then adding the index of the first
   location where the user's account appears within the shard. If the user's
   account is listed in the accountShardOffsets array, you need to add one and
   then subtract the value of the first element in the shard.

10. Determine the offset of the accountUpdates within the block. This can be
    determined using numAccountUpdates field of the BlockHeader. Multiply that by
    the size of the Xint, round up to the nearest byte, and then add the offset
    and length of the accountShardOffsets field to get the offset of the
    accountUpdates field.

11. Download all of the user's accountUpdates. Each account update is 264
    bytes exactly.

12. Integrate the accountUpdates into the user's wallet. How this is done will
    depend on what the accountUpdates are. This is the final step for
    processing one block.

When scanning a block to learn the user's account index, the lite wallet takes
the following steps:

1. Compute the offset of newAccountShardOffsets within the block. This can be
   done knowing the numLeaves and numAccountUpdates fields from the block header.
   See the above section for more details.

2. Compute the size of the newAccountShardOffsets array. This can be done by
   taking the square root of the numNewAccounts field from the BlockHeader,
   rounding up, and multiplying by 32.

3. Download the newAccountShardOffsets field from the block using a range
   request.

4. Use the newAccountShardOffsets data to determine which shard would contain
   our account id. Each element of the offsets array contains an account id
   that appears in the block. The latest element in the offsets array that
   points to an account id that is alphabetically less than our own id corresponds
   to the shard that would contain our account id. If our account id appears in
   the offsets array, we can skip downloading the shard because we know our
   account id will be the first element in that shard. Unlike the
   activeAccounts, newAccounts does not have any repeat elements.

5. Download the shard of newAccounts that would contain the user's account id.

6. Scan the shard for the user's account id. If the account id is not found,
   move on to the next block.

7. If the user's account id is found, compute the index of the id within the
   entire newAccounts object. This can be done by taking the shard index and
   multiplying it by the number of elements per shard, and then adding the index
   of the user's account id within the shard.

8. Compute the user's account index within the chainstate by adding their index
   within the newAccounts object and adding it to the number of leaves that
   were in the parent block, which can be found in the parent block header. The
   lite node has now found the user's account index and can scan future blocks
   for updates to the user's account.

##### Reorgs

If there is a reorg that goes deeper than the block which created the user's
account, the user's account index will change. The lite node needs to
essentially reset, rediscover the user's index, and then rediscover all of the
account updates for the user.

If there is a reorg that touches some of the user's account updates, but does
not change the user's account index, the lite wallet can reverse the effects of
those blocks by rewinding the accountUpdates. Every accountUpdate contains both
the initial state and the final state, so the lite wallet can rewind by
overwriting the user's leaf data with the initial state. Note that the lite
wallet will need to iterate over the accountUpdates in reverse order when
performing a reorg, because the same leaf can have multiple updates, and the
lite wallet needs to ensure that it ends up with the original initialState of
the leaf.

#### Full Block Validation

The full node validator starts by assuming that it is validating a block in a
valid header-chain. It assumes that the header of the parent block is
available, and it assumes that all prior blocks are valid. Assuming that all
prior blocks are valid, the full node is able to prove to itself that the
current block is valid as well. If there is an invalid block earlier in the
history, this block can potentially be made to appear valid without actually
being valid.

The full node needs to validate every single byte in the header of the block
and in the block itself. This is because the block is relied upon by lite nodes
to pull out the wallet state.

We will need the following in-memory structs:
```go
type accountUpdateLeaf struct {
	AccountID AccountID
	LeafIndex uint64
}

type accountUpdateVerifier struct {
	ExpectedInitialState [PersistedLeafSize]byte
	ExpectedFinalState   [PersistedLeafSize]byte
	Verified             bool
}

type newAccountVerifier struct {
	AccountID    AccountID
	impliedIndex uint64
}

type LeafTransformationVerifier struct {
	currentState [PersistedLeafSize]byte
	finalState   [PersistedLeafSize]byte
}
```

1. Verify that the parentid in the BlockHeader matches the id of the header we
   are using for the parent block.

2. Verify that the header meets the work requirements, which can be determined
   by looking at the difficultyState of the parent block.

3. Verify that the timestamp in the BlockHeader is less than 3 hours into the
   future. If it is more than 3 hours into the future but less than 6 hours
   into the future, the block should be kept and re-validated once it is no
   longer more than 3 hours into the future. If the block is more than 6 hours
   into the future, it should be discarded entirely.

4. Verify that the timestamp is greater than the median timestamp of the
   previous 11 blocks. The timestamps of the previous 11 blocks can be found in
   the difficultyState.

5. Verify that the new difficulty state in the BlockHeader is correct. The new
   difficulty state can be computed by using the difficulty state of the parent
   block. Make sure to cover all fields, including height, decayedTotalTarget,
   and the timestamps.

6. Verify that the baseFeeStep was updated correctly. This can be verified by
   looking at the baseFeeStep in the parent header and also the numAccountUpdates
   in the parent header.

7. Download the first 1,580,234 bytes of the block. This corresponds to the
   10,000 headers listed in the block.

8. Verify the 10,000 headers that are listed in the block. Unfortunately, this
   requires downloading the 10,000 headers listed in the parent block. This
   strays from the ideal that a full node only needs the current block and the
   header of the parent, but the usefulness of having 10,000 headers in each
   block is enough to justify the consequences. The prevHeader field of the
   current block can be verified by computing it from the prevHeader of the
   previous block and the first element of the blockHeaders array. The next
   9998 headers can be verified by ensuring they match byte-for-byte the
   corresponding headers listed in the parent block. The final header (the newest)
   should match the parent header exactly.

9. Discard the currently downloaded bytes, we no longer need them. We will be
   discarding key pieces of the block as we go to limit the total memory
   consumption.

10. Download the accountUpdates section of the block. The size of this section
    can be computed from the numLeaves and numAccountUpdates. This section
    includes the accountShardOffsets field, the activeAccounts field, and the
    accountUpdates field. This will always be less than 18 MiB total, and
    typically less than 9 MiB. Note that this implicitly checks the correctness
    of numLeaves and numAccountUpdates, because if those values are wrong we
    will not parse the block correctly.

11. Scan through the accountShardOffsets and ensure that the
    accountShardOffsets are monotonically increasing. Multiple consecutive
    elements with the same value is allowed.

12. Scan through the activeAccounts and ensure that the activeAccounts are
    monotonically increasing. Multiple consecutive elements with the same value
    is allowed. At the same time, ensure that the elements in accountShardOffsets
    correspond to the first element in each shard.

13. Scan through the accountUpdates. Ensure that every set of updates which is
    supposedly clustered under one account actually shares the same accountID.

14. While scanning through the accountUpdates, create a hashmap. For each
    accountUpdate, create an accountUpdateLeaf object which will be used as the
    key. Create an accountUpdateVerifier object that contains the initialState
    and finalState reported by the accountUpdate, and set the Verified field to
    false. Check whether the accountUpdateLeaf key already exists in the
    hashmap. If it does, the accountUpdateVerifier inside of the hashmap should
    exactly match the accountUpdateVerifier we just created. If the match is not
    exact, the block is invalid. If the key does not already exist, insert the new
    accountUpdateVerifier object using the key.

15. Keep the hashmap in memory, but free the rest of the accountUpdates section
    of the block. When we process the full set of transactions, we will update
    the hashmap and check that every change was properly represented.

16. Download the newAccounts section of the block. This should not exceed more
    than 2056 KiB of data. This section includes the newAccountShardOffsets and
    newAccounts fields.

17. Verify that the shard offsets of the newAccountShardOffsets field are
    monotonically increasing. There should be no repeats.

18. Verify that all of the accounts in the newAccounts objects are
    monotonically increasing with no repeats, and that the first element in
    each shard of newAccounts matches the corresponding element in the
    newAccountShardOffsets array.

19. Create a hashmap for the newAccounts that maps from a newAccountVerifier
    object to a bool. For each new account, create the newAccountVerifier
    object and map it to the value 'false'. The impliedIndex of the account is
    the numLeaves of the parent block plus the index of the account in the
    newAccounts object. We will check later that these new accounts were all
    represented correctly.

20. Keep the new hashmap but toss the newAccounts section of the block.

21. Begin streaming the leafTransformations field of the block. This field is
    the largest part of the block, potentially as large as 100 MB, so it cannot
    be downloaded all at once. Each element will be at most 2000 bytes, so it
    can be productively streamed in small segments (suggested: 8 MB at a time).

22. Verify that the first leaf transformation corresponds to the
    `nextMinerPayout` of the previous block.

23. For each leafTransformation, there will be an initialState and a
    finalState. Scan the two values to determine whether or not this update is
    an accountUpdate. Whether or not this is an account update will depend on
    the type of the leaf and which fields changed. If only the NextLeaf field
    changed, it is not an accountUpdate. If the type is not associated with an
    account, it is not an accountUpdate. If the leafTransformation is an
    accountUpdate, derive the accountUpdateLeaf object and check the
    accountUpdates hashmap we created earlier. If the key is not in the
    hashmap, the block is invalid. If the initialState or finalState in the
    hashmap does not match the initialState in the leafTransformation, the block
    is invalid. If everything matches, set the 'Verified' bool to 'true' and
    update the hashmap. We will check later that every object in the hashmap
    was verified.

24. For each leafTransformation, verify that the leafIndex is strictly greater
    than the leafIndex of the previous leafTransformation.

25. For each leafTransformation, verify the transformationProof. This is a
    Merkle proof that the initialState of the leaf exists in the Merkle root of
    the chainstate of the parentBlock as reported by the leafTransformation.
    Note that each transformationProof builds upon the previous
    tranformationProofs in the set of leafTransformations. Because the index of each
    leaf is strictly increasing, the node should only need to remember log(n)
    total leaves as it scans through the leafTransformations. There will be no
    leafTransformation objects in this array for new accounts, so we will be
    able to confirm that the leafTransformations have correct initialState values
    as soon as we process the final leafTransformation by comparing our final
    Merkle root to the Merkle root of the chainstate in the parent block. If it
    does not match, the block is invalid.

26. For each leafTransformation, hash up the finalState of the leaf and
    incorporate it into a MerkleTree object that is attempting to compute the
    MerkleRoot of the block after all changes have been applied. We will know
    if the MerkleRoot matches after we have processed all of the newLeafProofs. More
    information can be found in the [Proof fill algorithm](#proof-fill-algorithm)
    section.

27. Add each leaf to a hashmap that maps from the LeafID to the
    LeafTransformationVerifier of that leaf. Set the currentState of the
    verifier equal to the initial state of the leaf. By the end of
    the block verification, the two should match exactly. This hashmap will
    also get entries from the newLeafProofs section of the block. We don't need
    to worry about the index in this hashmap because we verify the index when
    we build the Merkle root of the block.

28. Finish streaming all of the leaves and computing the Merkle root based on
    the initialState of the block. If the Merkle root does not match the
    chainstate merkle root reported in the parent header, the block is invalid.

29. Begin streaming all of the newLeafProofs. If the leafTransformations were
    being streamed in batches, some of the newLeafProofs may have been pulled
    in as part of a batch. Take care to start from the beginning and use any
    data that got pulled in as part of the final leafTransformation batch.

30. For each newLeafProof, look up the provingLeafID in the leafTransformations
    hashmap. If the provingLeafID is not found, the block is invalid. Check the
    currentState of the verifier and make sure that the NextLeaf field points to an
    AccountID which is alphabetically greater than the ID of the new account.
    Also check that the AccountID of the new account is alphabetically greater than
    the provingLeafID. Update the 'currentState' of the verifier to change the
    NextLeaf field to point to the new leaf.

31. Create a LeafTransformationVerifier for the new account. Have the
    'currentState' be blank except for the NextLeaf value, which should be set
    to be equal to the NextLeaf value that was originally provided by the proving
    leaf. Set the 'finalState' equal to the final state established by the
    newLeafProof.

32. Incorporate the final state of the leaf into the Merkle root that we are
    building to verify the chainstate merkle root declared by the block header.
    The new leaves are provided in order of their index, with each new leaf
    having an index that is exactly 1 greater than the previous index.

33. Check whether the new leaf is an account update. If it is, derive the
    accountUpdateLeaf object and check the accountUpdates hashmap we created
    earlier. If the key is not in the hashmap, the block is invalid. If the
    initialState or finalState in the hashmap does not match the initialState in
    the leafTransformation, the block is invalid. If everything matches, set the
    'Verified' bool to 'true' and update the hashmap. We will check later that
    every object in the hashmap was verified.

34. Check the leafFinalState of the newLeafProof to determine whether this leaf
    is a new account. If this leaf is a new account, create a
    newAccountVerifier object using the AccountID which is declared in the
    leafFinalState and the implied index of the new leaf. Check that this leaf
    exists as a key in the new accounts hashmap. If it does, set the value to
    'true'. If it does not, the block is invalid. Check that all of the new
    accounts are clustered at the very front of the new leaf proofs. Once there
    is a newLeafProof which is not a new account, more new accounts are not
    allowed.

35. Finish scanning the newLeafProofs. By the time this is done, we will have
    completed the Merkle root of the new block. Verify that the Merkle root of
    the new block matches the merkle root established in the header. If it does not
    match, the block is invalid.

36. Scan through the accountUpdates hashmap. At this point, every
    accountUpdateVerifier should have the 'Verified' field set to 'true'. If
    any accountUpdateVerifier does not have the 'Verified' field set to 'true', the
    block is invalid. Discard the accountUpdates hashmap.

37. Scan through the new accounts hashmap. Every key in the hashmap should map
    to the value 'true'. If any key does not map to the value 'true', the block
    is invalid. Discord the new accounts hashmap.

38. Start streaming the transactions. For each transaction, execute the update
    and modify the corresponding 'currentState' values in the
    leafTransformations hashmap. If any leaves are updated in the transactions
    which aren't represented in the leafTransformation hashmap, fail the block.

39. For each transaction, determine which leafs are impacted and what updates
    need to be made to the 'currentState' of those leaves in the leaf
    transformations hashmap. Make the updates accordingly. There is no need to
    verify Merkle proofs or check any chainstate indexes, we have already
    confirmed all of these things. All we need to do is update the 'currentState'
    fields of each affected leaf in the leaf transformations hashamp.

40. Once all transactions have been processed, scan through the leaf
    transformations hashmap. Every single value in the hashmap should have an
    indentical 'currentState' and 'finalState'. If any value has any mismatch,
    the block is invalid.

41. Hash the first 143 elements of the `recentMinerPayouts` and compare them to
    the `futureMinerPayouts`. If the hashes don't match, the block is invalid.

42. Verify that the last element of the `recentMinerPayouts` has the correct
    payout (regular miner payout + premium fee). If not, the block is invalid.

##### Proof fill algorithm

This section gives a detailed overview of how the proofs within the leaf
transformations section of a block can be verified. Every proof builds up on top
of the previous proofs so all parts of the proof that are contained in a
previous proof can be omitted.

Quick Example. Given a merkle tree with leaves A to H and transformations for
the leaves A, B, C and D we get the following proofs before omitting duplicates.

```
inputs := []Proof{
  {A, N_B, N_CD, N_EH},    // updates A to A'
  {B, N_A', N_CD, N_EH},   // updates B to B'
  {C, N_D, N_A'B', N_EH},  // updates C to C'
  {D, N_C', N_A'B', N_EH}, // updates D to D'
}
```

After dropping all nodes that have been part of a previous proof we get the
following compressed proofs.

```
inputs := []Proof{
  {A, N_B, N_CD, N_EH}, // updates A to A'
  {B},                  // updates B to B' - 3 nodes omitted
  {C, N_D},             // updates C to C' - 2 nodes omitted
  {D},                  // updates D to D' - 3 nodes omitted
}
```

These compressed proofs are what is actually contained within the block and are
all the information required to verify the state transition between blocks
correctly. 

NOTE: Since we are looking at the leaves from left to right, a shorter proofs
indicates more common nodes with the previous proof. That's why the proof for
leaf B only contains a single element. Because it's the direct neighbour of leaf
A. We can also implicitly tell which nodes we need by looking at the proof's
leaf index and the length of the proof. e.g. the proof for `B` belongs to leaf1
and has a length of 1. Knowing that index 1 is a right-hand leaf and that the
proof has a length of 1, we can tell that it requires the hash of its immediate
neighbour `A` which is `N_A`.

Applying this knowledge we can do the following. We look at the first input. We
know that it doesn't have any gaps yet so we can verify it against the parent
block's state root `root`. This will update the `root` to `root'`.

The next input is the proof for `B` and has 3 gaps. We also know that it should
have the same length as the first proof for `A` because the merkle proofs always
have the length `log2(numLeaves)`. So we take `A'` from the previous
transformation, turn it into `N_A'` and also take `N_CD` and `N_EH` from the
previous input and add them to the compressed proof to get `{B, N_A', N_CD,
N_EH}`. This is a complete proof that we can now verify against `root'` which
gives us `root''`

Input number 3 contains the proof for `C` and got two gaps. Similar to
before we take the last known complete proof and add the missing elements from
that resulting in `{C, N_D, N_A'B', N_EH}`. Again, verify against the latest
intermediary root `root''` and receive `root'''`

We do the same thing one more time to get `D, N_C', N_A'B', N_EH` and root
`root''''`. This root should now match the new state root of the current block
contained within its header.

NOTE: Every time we verify we actually do 2 things. e.g. we prove that `A` is
part of `root` and then we compute `root'` by swapping out `A` with `A'`.

The following describes the algorithm above in pseudo code:

```go
// Initialize the currentRoot to the root of the chain state of the parent block.
var currentRoot Hash = parentBlockRoot

```go
// Initialize the currentRoot to the root of the chain state of the parent block.
var currentRoot Hash = parentBlockRoot

// Grab the first proof. This should always be a full proof without gaps.
// lastProof will always contain a full proof from leaf to root without any
// gaps. Throughout the whole algorithm.
lastProof, inputs := inputs[0], inputs[1:]

// Verify it. This returns the new root that the next proof should be verified
// against.
currentRoot := lastProof.Verify(currentRoot)

// Create a cache merkle tree. We can add leaves and nodes to this tree and it will
// compress them as we go. e.g. 2 neighbouring leafs will be compressed to a node
// at height 1. Two neighbouring nodes at height 1 to one at height 2 and so on.
// Later we can retrieve nodes for a given node's index and height.
cacheTree := NewCachedMerkleTree()

// Verify the remaining inputs.
for _, input := range inputs {
    // Push len(input)-1 elements from the last verified proof to the cache. We know
    // that these will no longer be subject to change within the merkle tree since they
    // are positioned left of the next proof.
    // NOTE: For the first element of the proof, the leaf, we push the updated value of
    // the leaf, not the original one.
    for i := 0; i < len(input)-1; i++ {
      // Push element to height i.
      cacheTree.Push(i, lastProof[i])
    }

    // Index of the leaf the proof is for.
    index := input.LeafIndex
    
    // All proofs have the same length. So we know how much data to fill them in with.
    for len(input) < len(currentProof) {
        // Get the height of the next element in the proof.
        height := len(input)
        
        // Fetch the missing element from the cache.
        node, found := cacheTree.Get(index, len(input))
        if !found {
            return errors.New("corrupt proof found")
        }
        input = append(input, node)
    }
    
  // Verify proof and update current Root.
  currentRoot = assert(currentProof.Verify(currentRoot))
}

// Verify final root against block's root.
assert(currentRoot == currentBlockRoot)
```


#### Accepting a Block as a Miner

This section assumes that the block is valid. The miner can use the full node
software to validate the block.

The miner needs to update the on-disk databases to reflect the changes in a new
block. Any leaves that changed in the chainstate need to be updated, any
in-memory cache needs to be updated, and the hybrid b-tree database needs to
have all necessary elements added to it.

1. The miner looks at the leafTransformations object to identify all leaves in
   the existing chainstate that have changed, and implements those changes in
   its chainstate database. The Merkle root cache needs to be updated as well.
   There is only one LeafTransformation in the block per chainstate leaf, and
   every already-existing leaf that changes will be represented.

2. The miner looks at the newLeafProofs in the block to add new leaves to the
   chainstate. This will be a simple append operation. The in-memory cache will
   need to be updated.

3. The miner looks at newLeafProofs in the block to add the new LeafIDs to the
   hybrind b-tree, along with their corresponding index.

That should be all that's required. It ends up being a large number of disk
IOPs and takes a while to complete (owing to the large number of random writes
in the chainstate and b-tree), but the process itself is quite simple.

#### Accepting a Reorg as a Miner

If a block is reorged out, the miner will need to update its databases to
reflect that the block got dropped. Simlar to above, the steps are pretty
simple but it may take a while due to the large number of IOPs required.

1. The miner looks at the newLeafProofs to determine which leaves no longer
   exist. The miner truncates the chainstate database to drop all of the new
   leaves. The miner also deletes all of the associated LeafIDs from the hybrid
   b-tree.

2. The miner looks at the leafTransformations to determine the prior state for
   all remaining leaves. The object contains an 'initalState' field, and the
   miner only needs to update the chainstate database to set those leaves to their
   initial state for the block.

And that's it. Nice and easy, though there are a lot of random disk writes that
means it can take a few seconds.

#### Building a Block as a Miner

This section assumes that the miner has already compiled a set of transactions
which are valid and do not conflict. The process for doing that will be
established in another section.

The miner is assumed to have access to the full previous block, as well as have
access to a chainstate database which lists out all of the leaves of the
chainstate in order, and access to a hybrid b-tree database which allows the
miner to look up the leaves in alphabetical order sorted by LeafID, as well as
look up the chainstate index for a leaf of any given ID.

The transactions will be placed into the block in the order that they should be
executed in. Every other section of the block has an alternative ordering,
meaning the miner will need to perform sorting operations.

TODO: Need to establish a canonical order for the accountUpdates. Right now
leaves are not sorted in any particular way. As far as I know, this doesn't
cause any issues but I don't want the miner to have any ability at all to
determine how a block should be structured except by changing which
transactions are in the block.

```go
type FinalState [128]byte

type newLeafTransformation struct {
	index      uint64
	finalState FinalState
}

var allLeafTransformations map[LeafID]newLeafTransformation

var allNewLeaves map[LeafID]FinalState

var allNewAccounts map[AccountID]struct{}

type NewAccountUpdate struct {
	count      uint16
	finalState FinalState
}

var allAccountUpdates map[LeafID]NewAccountUpdate
```

1. Iterate through the transactions in the order that the will be executed in
   for the block. For each transaction, identify every leaf that is impacted
   and whether the leaf existed prior to this block. That check can be
   performed by using the miner's hybrid b-tree. A cache should be used when
   checking the b-tree so that consecutive checks for the same value return the
   same result without needing to perform a disk access. Take care that the
   cache reflects the state of the disk itself, which will not be updated until
   after the entire block is built and accepted.

2. For each existing leaf that is impacted, load the leaf. First check the
   allLeafTransformations map. If the leaf is already in that map, use the
   finalState of the leaf in that map to execute the transaction. If the leaf does
   not exist in the allLeafTransformations map, try loading the leaf from the
   allNewLeaves map and using the final state from that map. If the leaf is in
   neither map, load the leaf from the chainstate database.

3. For each leaf, if the leaf already exists prior to the current block, the
   miner needs to add the leaf to the allLeafTransformations map. The final
   state of the leaf should be set to the final state after the transaction has
   been processed. If the leaf already exists in the allLeafTransformations map,
   the existing element should be overwritten.

4. If the leaf does not already exist prior to the current block it needs to be
   added to allNewLeaves. If the leaf is already in allNewLeaves (due to
   appearing earlier in the block), the finalState of the leaf should be
   updated to the leaf's state after executing the transaction. If the leaf
   corresponds to a new account, add that account to the 'allNewAccounts' map.

5. Whether the leaf was created this block or not, determine whether the leaf
   impacts an account. If the leaf does impact an account, add the leaf by its
   ID to the allAccountUpdates map. If the leaf already exists in the
   allAccountUpdates map, increment the 'count' value and update the finalState
   to reflect the new finalState. If a leaf is updated multiple times in a
   single transaction, the count needs to be incremented multiple times as well.

6. Use the length of allNewLeaves to determine the new total size of the
   chainstate. Then iterate through the allAccountUpdates map and add up all of
   the 'count' values. Use the total number of account updates and the new
   total number of leaves in the chainstate to determine the size of the Xint
   for the accountUpdates section of the block.

7. Sort the accounts in 'allNewAccounts' by their AccountID and build the
   newAccounts and newAccountShardOffsets section of the block. The number of
   shards should be the sqrt of the number of new accounts (rounded up), and
   there should be the same number of accounts in each shard, except for the last
   shard which can have fewer accounts.

8. Assign a chainstate index to each new account based on the order that it
   appears in the newAccounts field and use them to create the accountUpdates
   section of the block. The initalState of each update should reflect the
   state of the leaf before any transactions were applied. The finalState should
   reflect the state of the leaf after all transactions were applied. If the
   'count' of the update in the NewAccountUpdate object is greater than 1, the
   leaf needs to be included in the list of accountUpdates multiple times, though
   each time it appears it should be identical to previous appearances.

9. Create the new leaf proofs for the block. Go through the new leaves of the
   block in alphabetic order of their ID and identify the proving leaf in the
   chainstate that proves the leaf does not already exist in the chainstate.
   The leaf can be identified by using the hybrid b-tree. Perform a lookup on the
   ID of the new leaf, and the chainstate will return the ID and index of the
   proving leaf if the new leaf is not already in the hybrid b-tree. Update that
   leaf to point to the new leaf, and update the state of the new leaf to point to
   the previous NextLeaf of the proving leaf. Add the updated proving leaf to the
   set of allLeafTransformations.

10. Create the leafTransformations array of the block. Use the chainstate
    database (which should not have changed yet as no updates have been
    submitted to the database) to determine the initalState for each leaf. The
    chainstate database can also be used to build the required Merkle proofs.
    The transformationProofLength states a number of leaves, not a number of
    bytes.

11. Build the `recentMinerPayouts` by popping off the first payout of the last
    block's `recentMinerPayouts` and adding another payout at the end for the miner
    itself. The new first payout also becomes the `nextMinerPayout` in the header.
    Hash all elements in `recentMinerPayouts` together starting from the second one
    (index 1). This becomes the `futureMinerPayouts` field in the header.

12. Use the previous block to build the header section of the block.

13. All fields should now be created and can be put together into the final
    block. Then the header for the block can be created and the PoW can be
    completed to begin mining the block.

## Miner

Miners work a bit differently in Skynet compared to Sia since there is no p2p
network. Instead, every user keeps a list of miners that they contact directly
to send txns to.

New users can obtain a list of miners either by looking at their peers' lists or
by validating all blocks themselves and extracting the miners' infos from the
txns they use to announce themselves. e.g. a peer might be a friend to whose
list a user can subscribe using their public key.

The list of miners can be found at the following skylink.

```go
minersDataKey := hash("miner" | "list")
minersSkylink := NewResolverSkylink(pubkey, minersDataKey)

// Info containing the miners known to a user.
type minersInfo struct {
    // Entry that can be used to fetch information about the miner.
    registryEntryID crypto.Hash
    
    // Endpoint used for submitting txns to the miner.
    apiEndpoint string
}
```

### Mining

TBD

### Oak difficulty algorithm

The oak difficulty algorithm is the algorihm that determines the mining target
of a block given some information. Since Skynet Token is going to require that a
full node can verify any transition between two blocks without any more blocks,
all the information required to compute the difficulty of the next block needs
to be contained within the previous one.

#### Type/Consts overview
```go
// Types
type Target [32]byte
type Difficulty big.Int // difficulty = maxTarget / target
type BlockHeight uint64
type Timestamp uint64

// Constants
var GenesisTimestamp Timestamp = ??? // timestamp of the genesis block
var BlockFrequency int64 = 600 // time between blocks in seconds

// The block shift determines the most that the difficulty adjustment
// algorithm is allowed to shift the target block time. With a block
// frequency of 600 seconds, the min target block time is 200 seconds,
// and the max target block time is 1800 seconds.
var OakMaxBlockShift = 3

// The decay is kept at 995/1000, or a decay of about 0.5% each block.
// This puts the halflife of a block's relevance at about 1 day. This
// allows the difficulty to adjust rapidly if the hashrate is adjusting
// rapidly, while still keeping a relatively strong insulation against
// random variance.
var OakDecayNumerator big.Int = 995
var OakDecayDenominator big.Int = 1e3

// The max rise and max drop for the difficulty is kept at 0.4% per
// block, which means that in 1008 blocks the difficulty can move a
// maximum of about 55x. This is significant, and means that dramatic
// hashrate changes can be responded to quickly, while still forcing an
// attacker to do a significant amount of work in order to execute a
// difficulty raising attack, and minimizing the chance that an attacker
// can get lucky and fake a ton of work.
var OakMaxRise = big.NewRat(1004, 1e3)
var OakMaxDrop = big.NewRat(1e3, 1004)
```

##### Emergency Mode

```go
// Emergency Mode related vars. The emergency mode is enabled when the
// time since the last block has been > EmergencyModeActivationThreshold.
// In that case we replace some of the vars above with the following ones
// which have the same name but are prefixed with 'Emergency'.
//
// NOTE: we also cut the block reward in half and the
// max block size a.k.a the number of outputs.
//
var EmergencyModeActivationThreshold = time.Hour * 16
var EmergencyOakDecayNumerator big.Int = 800
var EmergencyOakDecayDenominator big.Int = 1e3
var EmergencyOakMaxRise = big.NewRat(1004, 1e3)
var EmergencyOakMaxDrop = big.NewRat(1e3, 3 * 1e3)
```

#### Updating the totals

The following snippet describes how the decayed total time and decayed total
target passed in to `childTarget` below as `parentDecayedTotalTime` and
`parentDecayedTotalTarget` are updated for each new block.

```go
newDecayedTotalTime := (previousDecayedTotalTime * OakDecayNumerator / OakDecayDenominator) +
(newTimestamp - previousTimestamp)

newDecayedTotalTarget = prevDecayedTotalTarget.MulDifficulty(big.NewRat(types.OakDecayNum, types.OakDecayDenom))
                        .AddDifficulties(targetOfNewBlock)
```

#### Algorithm

```go
// childTarget computes the target that the next block to mine should fulfill given
// the following parameters.
//
// parentDecayedTotalTime - the total decayed time that has passed since the genesis
// block in relation to the parent block's timestamp.
//
// parentDecayedTotalTarget - the decayed total target of all blocks up until and
// including the parent block.
//
// parentHeight - the blockheight of the parent.
//
// parentTimeStamp - the timestamp of the parent block.
func childTarget(parentDecayedTotalTime int64, parentDecayedTotalTarget Target, parentHeight BlockHeight,
                 parentTimestamp Timestamp) Target {
    // Make sure parentDecayedTotalTime is at least 1 second to avoid divisions by 0 later.
    if parentDecayedTotalTime < 1 {
        parentDecayedTotalTime = 1
    }
    
    // Compute the expected time of the parent block.
    expectedParentTime := BlockFrequency*parentHeight + GenesisTimestamp
    
    // Compute delta between the expectation and the actual timestamp.
    delta := expectedParentTime - parentTimestamp
    
    // The difficulty shift is 1/10e6th of the square.
    deltaSquare := delta*delta
    if delta < 0 {
        // Restore negative delta.
        deltaSquare *= -1
    }
    shift := deltaSquare / 10e6 // 10e3 second delta leads to 10 second shift
    
    // Apply the shift to the frequency to get the target for the next block.
    // NOTE: handle potential underflow
    targetBlockTime := BlockFrequency + shift

    // Clamp the target block time to 1/3 and 3x of the frequency.
    if targetBlockTime < int64(types.BlockFrequency)/types.OakMaxBlockShift {
        targetBlockTime = int64(types.BlockFrequency) /types.OakMaxBlockShift
    }
    if targetBlockTime > int64(types.BlockFrequency)*types.OakMaxBlockShift {
        targetBlockTime = int64(types.BlockFrequency) * types.OakMaxBlockShift
    }
    
    // If the targetBlockTime is 0 we set it to 1 second to avoid multiplying
    // with 0 later. This should only happen if the BlockFrequency is very
    // small or the shift very large. So usually only in testing scenarios.
    if targetBlockTime == 0 {
        targetBlockTime = 1
    }
    
    // Compute the visible hashrate using the total time that has passed for
    // a given parent as well as the cumulative difficulty.
    visibleHashrate := parentDecayedTotalTarget.Difficulty().Div64(uint64(parentDecayedTotalTime))
    
    // Handle divide by zero risks.
    if visibleHashrate.IsZero() {
        visibleHashrate = visibleHashrate.Add(1)
    }
    
    // Determine the new target by multiplying the visible hashrate by the
    // target block time.
    newTarget := types.RatToTarget(new(big.Rat)
      .SetFrac(types.RootDepth.Int(), visibleHashrate.Mul64(uint64(targetBlockTime)).Big()))

    // Clamp the newTarget to a 0.4% difficulty adjustment.
    maxNewTarget := currentTarget.MulDifficulty(types.OakMaxRise)
    minNewTarget := currentTarget.MulDifficulty(types.OakMaxDrop)
    if newTarget.Cmp(maxNewTarget) < 0 && parentHeight+1 != types.ASICHardforkHeight {
        newTarget = maxNewTarget
    }
    if newTarget.Cmp(minNewTarget) > 0 && parentHeight+1 != types.ASICHardforkHeight {
        // This can only possibly trigger if the BlockFrequency is less than 3
        // seconds, but during testing it is 1 second.
        newTarget = minNewTarget
    }
    return newTarget
}
```

### Persistence

A miner needs to store a large amount of information to be able to build the
proofs required for making a valid block. The miner needs to store the full set
of accounts so that they can build Merkle tree proofs, the miner needs to store
a table that allows them to figure out the account index for a particular
address, and the miner needs to have a way to identify the alphabetically
closest account to a brand new address so they can build the new account proofs.

At a scalability of 10 billion accounts, the minimum amount of data required to
store all the necessary information is roughly 320 GB. After necessary
structural overheads the true requirements are closer to 1 TB. We can't do
everything in-memory, we have to use databases. And at a target throughput of 50
tps, performance is important. Without heavy optimization, a miner could require
more than a minute to process each block due to disk I/O bottlenecks.

Because miners (or at least mining pools) are expected to be beefy, high uptime
machines, we are able to make an unusual tradeoff. The entire structure of the
databases is going to be kept in memory and built from scratch each time the
miner starts. This will require reading around 1 TB of data each time the miner
starts and performing on the order of 10 billion hash operations. This can take
up to an hour. The miners will need up to 64 GB of RAM, a 2 TB SSD rated at 1
million IOPS, and an 8-core CPU.

The miner has two separate databases. The first is just a list of accounts,
sorted by their index in the chainstate. The miner uses this database to track
the chainstate and compute merkle proofs for transactions. At startup, the miner
iterates over the entire chainstate and builds an in-memory cache of subroots.

The second database sorts the accounts alphabetically, and is useful both for
building the new account proofs and also for looking up the index of an existing
account. The database is a single file, with a pseudo-sorted list of account
hashes. They are placed into buckets where each bucket is internally sorted, and
none of the buckets have any overlap of address range. This design is inspired
by a b-tree.

The internal nodes of this database are built at startup. Because the internal
nodes are fully in memory, we use a red-black tree instead of a proper b-tree.

##### Performance Characteristics

Updating an account's state should require two disk reads and one disk write, 3
IOPs total. Amortized, adding a new account should require 1 disk read and 1
disk write per new account. If the accounts are being added to buckets within
the database that are already full, 2 disk reads and 2 disk writes can be
required.

A worst case block requires less than 300,000 IOPs to process. The average block
will be under 100,000 IOPs to process.

These IOPs are all fully parallel. All the disk operations to process a single
block can be queued simultaneously. This means that we should be able to max out
the full benchmarked capabilities of an SSD. If manufacturer claims are to be
believed, a high-end SSD should be capable of processing about 10 blocks per
second, which puts the total bootstrapping time at roughly 1.5 hours per year of
blockchain history.

If the current requirements prove to be too substantial to bootstrap quickly, we
can download a miner database from a friend and then fully verify that database
using the merkle root of a block. This brings the total IOPs required down to
roughly 1 IOP per account. We have no plans to implement this as we believe
normal Initial-Blockchain-Download (IBD) will be fast enough for mining pools,
but it's nice to know that the optimization is feasible.

#### Database 1: The Chainstate

The chainstate is a single flatfile that contains all of the data in all of the
leaves in the Merkle tree. When the leaves are updated, the file is also
updated.
At startup, the miner scans through the whole file and creates cache layers for
the Merkle tree. The cache layers cover 5 levels each, and are fully kept in
memory. If the miner reboots, the cache layers will have to be rebuilt.
The data on disk is read as the miner receives new transactions in the mempool,
but it is only updated when a new block is processed. A simple WAL is used to
write out the single transaction that will update the block, and then the block
is applied.

```
|||---------HEADER----------
|| - Version:  1 byte
|| - Reserved: 4095 bytes
||
|||----------BODY-----------
|| - Leaf1  [128]byte
|| - Leaf2  [128]byte
|| - ...
|| - LeafN  [128]byte
|||-------------------------
```

The first 4kib of the file are reserved for the header which contains at least a
version byte in the beginning for future upgrades to the persistence. The body
contains the data of the leaves (see `TokenBlance` or `Account` types) which are
padded to "PersistedLeafSize" (128) bytes.

#### Database 2: The Pseudo-B-Tree

The PBT (pseudo-b-tree) is a single file where each element is a tuple of the 32
byte ID of a leaf and the 8 byte index of the leaf within the chainstate. The
elements are clustered into buckets, where each bucket is 4096 bytes to match
the sector size of an SSD.
This means that each bucket can hold up to 102 elements. Each bucket represents
a range of elements, and every bucket covers a unique, non-overlapping range. If
a bucket fills up, it splits in half and creates two new buckets that are half
full. This part of the database works much like a b-tree.
Unlike a b-tree, all of the internal nodes are kept in memory. Because they can
be in memory instead of on disk, we use a [red-black
tree](https://en.wikipedia.org/wiki/Red%E2%80%93black_tree) in memory instead of
a proper b-tree. (this will both be easier to implement and have better
performance).
Just like the chainstate, the only updates to the PBT occur when there is a new
block being added. All other accesses remain in memory. The PBT can be used to
figure out the index of an address, and it can be used to figure out the
alphabetically closest element to a new ID, so that the miner can prove the new
ID is not already in the chainstate.

```go
// The leaves of the tree are persisted on disk. Only 100 of them will be read at a
// time from a disk sector.
Bucket struct {
  // Unique ID derived from the leaf.
  ID    [32]byte
  
  // Index of the leaf within the ChainState.
  Index uint64
}

// A leaf of the in-memory binary-search tree. Points to a bucket on disk.
TreeLeaf struct {
  // The bucket on disk. The offset can be derived from the index since every leaf's
  // size is always 128 bytes.
  BucketIndex uint64
}

// The nodes are the internal nodes of the tree. They contain thresholds for
// traversing the tree like a binary search tree.
TreeNode struct {
  LeftMaxID [32]byte // Largest ID in left branch
  RightMaxID [32]byte // Largest ID in right branch 
  
  // Node can be either red or black in a red-black-tree. The root and leaves are
  // always black.
  Red bool 
}
```
##### Example:

Let's assume a bucket can only hold 4 leafs and that we got 2 full buckets
already. Each number in the bucket describes a leaf's ID.

```
b0 = [1, 3, 4, 5]
b1 = [6, 7, 8, 9]
```

That results in the following tree in-memory. It contains the indices of the
buckets on disk in the leaves and the interior node contains 5 as the highest
leaf ID in the left branch and 9 as the highest one in the right one.

```
--------------------------------------------
Memory:
                5/9
               /   \ 
            b0Idx b1Idx 
----------------------------------------------
Disk
      b0:[1, 3, 4, 5] b1:[6, 7, 8, 9]
----------------------------------------------
```

Now let's add leaf ID 2. This should insert it into bucket `b0`. Since that
bucket is already full, we do the following:

1. Remove `b0`'s leaf from the binary tree.
2. Split `b0` into half. This results in `b0` and a new `b2`.
3. Append `b2` at the end of the file.
4. Insert two new leaves for `b0` and `b1` into the tree.

This results in the following tree:

```
--------------------------------------------
Memory:
                     
                     5/9
                    /  \
                3/5     \
               /   \     \
            b0Idx b2Idx b1Idx
----------------------------------------------
Disk
      b0:[1, 2, 3] b1:[6, 7, 8, 9] b2:[4, 5]
----------------------------------------------
```

As you can see, splitting `b0` resulted in a new bucket `b2`. On disk that
bucket sits at the end of the file. In memory, it became the neighbor of `b0` to
keep the integrity of the binary search tree intact.

---

## Wallet

The wallet's responsibility is to give the user an overview of their funds and
allow them to spend them. To achieve that it needs to be aware of what funds
belong to a user.

A fresh Sia wallet would achieve that by generating a large number of addresses
in advance as a "lookahead" and then scanning the blockchain. As it encounters
addresses from the lookahead within blocks it will adjust the lookahead and
increase its size.

Skynet has a different way of doing it since one wallet will only have one
account.

First you generate your `AccountID` deterministically. Then you go through the
blocks one-by-one and check whether the sorted `newAccounts` contain your ID
through a binary search.

Once you know your index within the account merkle tree, you can look the proof
for your account up and get the balance. From then on you continue going through
the blocks one-by-one and check the bitfield. Every time a block updates your
account, update the balance from the proof.

---

## Gateway

Unlike in Sia, the gateway doesn't make use of a traditional p2p network but
instead, users will use Skynet and SkyDB to communicate blocks between one
another. So the gateway can be thought of a collection of protocols to achieve
that.

Usually network participants will want to do the following things.

1. Subscribe to new blocks to verify (full nodes)
2. Do a bootstrapped sync using a trusted peer (light wallets)

In both cases you want to subscribe to someone's longest chain. For 1. it's
sufficient to subscribe to the longest chain of multiple untrusted miners while
for 2. you might want to choose trusted peers such as your friends. Either way
we need a protocol for an entity to publish their longest chain to Skynet so
that others can subscribe to it and get updates.

#### Publish/Subscribe Longest Chain

The longest chain is actually the chain that is backed with the most PoW. We
want miners and other participants to be able to publish that chain in form of a
list containing the headers of the blocks of that chain.

We also want subscriber to be able to gracefully handle 2 things.

1. Multiple blocks at the end of the longest chain change due to a reorg
2. Different miners/providers publish different longest chains

To do so without too much overhead, all blocks contain the block headers of the
last 100 blocks. That way a subscriber can easily tell if a reorg happened,
backtrace it to the point of the split and then continue syncing from there.

100 blocks should also provide sufficient buffer to cover all reorgs except for
maybe malicious ones. But even for a malicious one it shouldn't be possible for
an entity to control enough hashrate to outmine the remaining network for 100
blocks.



