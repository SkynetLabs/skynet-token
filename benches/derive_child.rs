//! Benchmark for `derive_child`.
//!
//! For a flamegraph of the performance:
//! sudo cargo flamegraph --bench derive_child --features testing -- --bench

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use skynet_token::consensus::{
    block::{consts, Hash, Skylink, SolvedBlockHeader},
    time,
};

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("derive_child", |b| {
        b.iter(|| {
            black_box(SolvedBlockHeader {
                header: consts::GENESIS_BLOCK.clone(),
                id: consts::GENESIS_ID.clone(),
            })
            .derive_child(
                Skylink::default(),
                Hash::default(),
                false,
                time::current_timestamp(),
            )
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
